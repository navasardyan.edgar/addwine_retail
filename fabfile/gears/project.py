from .base import Gear, print_status


class Manage(Gear):
    def __init__(self, conn, venv_path):
        self.venv_py = '{}/bin/python'.format(venv_path)
        super(Manage, self).__init__(conn)

    def collectstatic(self):
        result = self.c.run('{} manage.py collectstatic --no-input --settings=settings.production'.format(self.venv_py))
        print_status(result)

    def migrate(self):
        result = self.c.run('{} manage.py migrate --settings=settings.production'.format(self.venv_py))
        print_status(result)
