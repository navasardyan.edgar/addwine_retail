from .base import Gear, print_status
import os


class Machine(Gear):
    def make_seq_dir(self, release_folder='.'):
        result = self.c.run('ls -t {} | head -1'.format(release_folder))
        old_index = int(result.stdout.strip()) if result.stdout != '' else 0
        release_index = old_index + 1
        result = self.c.run('mkdir {}/{}'.format(release_folder, release_index))
        print_status(result)
        return str(old_index), str(release_index)

    def touch(self, filename):
        result = self.c.run('touch {}'.format(filename))
        print_status(result)

    def pwd(self):
        result = self.c.run('pwd')
        print_status(result)

    def build_path(self, *args):
        return os.path.join(self.target_dir, *args)

    def cp(self, source, dest):
        result = self.c.run('cp -R {}/. {}'.format(source, dest))
        print_status(result)

    def ln_fsn(self, link_name, target_name):
        result = self.c.run('ln -fsn {} {}'.format(target_name, link_name))
        print_status(result)

