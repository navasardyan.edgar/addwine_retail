class Gear:
    def __init__(self, connection, target_dir=None):
        self.c = connection
        self.target_dir = target_dir


def print_status(result):
    format_str = "## {:<60} {:>3}"
    status = 'OK' if result.exited == 0 else 'ERR'
    print(format_str.format(result.command, status))
    if status == "ERR":
        print(result.stderr)
