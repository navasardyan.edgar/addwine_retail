from .base import Gear, print_status


class Git(Gear):
    def pull(self):
        result = self.c.run('git pull origin master')
        print_status(result)
