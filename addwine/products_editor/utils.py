from django.conf import settings


def is_products_editor(user):
    return user.groups.filter(name__iexact=settings.PRODUCTS_EDITOR_GROUP_NAME).exists()