from django.db import models

from catalogue.models import Product


class ProductsEditorProduct(Product):

    class Meta:
        proxy = True
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

