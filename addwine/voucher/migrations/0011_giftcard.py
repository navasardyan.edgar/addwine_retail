# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-08 13:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0030_auto_20171007_1626'),
        ('voucher', '0010_auto_20171008_1438'),
    ]

    operations = [
        migrations.CreateModel(
            name='GiftCard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='This will be shown in the checkout and basket once the voucher is entered', max_length=128, verbose_name='Name')),
                ('code', models.CharField(db_index=True, help_text='Case insensitive / No spaces allowed', max_length=128, unique=True, verbose_name='Code')),
                ('spent', models.BooleanField(default=False, verbose_name='Использована')),
                ('active', models.BooleanField(default=True, verbose_name='Активна')),
                ('order', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gift_card', to='order.Order', verbose_name='Заказ')),
            ],
            options={
                'verbose_name_plural': 'подарочные карты',
                'verbose_name': 'подарочная карта',
            },
        ),
    ]
