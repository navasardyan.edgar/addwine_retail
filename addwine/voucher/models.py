from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.template.defaultfilters import lower
from django.urls import reverse
from oscar.apps.voucher import abstract_models
from django.utils.translation import ugettext_lazy as _

from marketing.models import MailCTA
from order.models import Order, settings
from seo.models import AbstractSeoConfigs


class VoucherGroup(AbstractSeoConfigs, models.Model):
    cover = models.ImageField(
        'Обложка', upload_to=settings.OSCAR_DISCOUNTS_IMAGE_FOLDER, max_length=255, blank=True, null=True)
    name = models.CharField('Название', max_length=512)
    description = models.TextField('Описание')
    rules = RichTextUploadingField('Условия участия в акции')
    mail_cta = models.ForeignKey(MailCTA, verbose_name="призыв к действию: рассылка", null=True, blank=True,
                                 on_delete=models.SET_NULL)
    code = models.CharField(_("Code"), max_length=128, db_index=True,
                            unique=True, help_text=_("Case insensitive / No"
                                                     " spaces allowed"))
    enabled = models.BooleanField(verbose_name='Активна', default=True)
    display_for_users = models.BooleanField('Отображать на сайте', default=True)

    SINGLE_USE, MULTI_USE, ONCE_PER_CUSTOMER = (
        'Single use', 'Multi-use', 'Once per customer')
    USAGE_CHOICES = (
        (SINGLE_USE, _("Can be used once by one customer")),
        (MULTI_USE, _("Can be used multiple times by multiple customers")),
        (ONCE_PER_CUSTOMER, _("Can only be used once per customer")),
    )
    usage = models.CharField(_("Usage"), max_length=128,
                             choices=USAGE_CHOICES, default=MULTI_USE)
    start_datetime = models.DateTimeField(_('Start datetime'))
    end_datetime = models.DateTimeField(_('End datetime'))

    def get_absolute_url(self):
        return reverse('voucher', args=[lower(self.code), ])

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.code = self.code.upper()
        super(VoucherGroup, self).save(*args, **kwargs)

    class Meta:
        app_label = 'voucher'
        verbose_name = 'акция'
        verbose_name_plural = 'акции'


class Voucher(abstract_models.AbstractVoucher):
    group = models.ForeignKey(VoucherGroup, on_delete=models.CASCADE, verbose_name='Акция', null=True, blank=True,
                              related_name='vouchers')
    max_applications_per_user = models.PositiveIntegerField(verbose_name='Применений на пользователя', default=1)
    enabled = models.BooleanField(verbose_name='Активно', default=True)

    def is_enabled(self):
        if not self.group:
            return self.enabled
        return self.enabled and self.group.enabled

    def is_active(self, test_datetime=None):
        return super().is_active(test_datetime) and self.is_enabled()

    def clean(self):
        return

    def rollback_discount(self, discount):
        self.total_discount -= discount
        self.save()

    def rollback_usage(self, order):
        """
        Remove records a usage of this voucher in an order.
        """
        self.applications.filter(order=order).delete()
        if self.num_orders > 0:
            self.num_orders -= 1
        self.save()

    def record_usage(self, order, user):
        """
        Records a usage of this voucher in an order.
        """
        if user and abstract_models.user_is_authenticated(user):
            self.applications.create(voucher=self, order=order, user=user)
        else:
            self.applications.create(voucher=self, order=order)
        self.num_orders += 1
        self.save()

    record_usage.alters_data = True


from oscar.apps.voucher.models import *


class GiftCard(models.Model):
    name = models.CharField(_("Name"), max_length=128,
                            help_text=_("This will be shown in the checkout"
                                        " and basket once the voucher is"
                                        " entered"))
    code = models.CharField(_("Code"), max_length=128, db_index=True,
                            unique=True, help_text=_("Case insensitive / No"
                                                     " spaces allowed"))
    description = models.TextField("Описание", blank=True, default="")

    value = models.DecimalField('Номинал', decimal_places=2, max_digits=12,)
    spent = models.BooleanField('Использована', default=False)
    active = models.BooleanField('Активна', default=True)

    def __str__(self):
        return self.code

    def is_available(self):
        return not self.spent and self.active

    class Meta:
        app_label = 'voucher'
        verbose_name = 'подарочная карта'
        verbose_name_plural = 'подарочные карты'


class VoucherUserApplication(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Клиент', related_name='voucher_applications', on_delete=models.CASCADE)
    basket = models.ForeignKey('basket.Basket', verbose_name='Корзина', related_name='vouchers_applications', null=True, on_delete=models.SET_NULL)
    voucher = models.ForeignKey(Voucher, verbose_name='Купон', related_name='user_applications', on_delete=models.CASCADE)
    created = models.DateTimeField(verbose_name='Дата применения', auto_now_add=True)

    class Meta:
        verbose_name = 'использование купона'
        verbose_name_plural = 'использования купонов'