/*
    Mark block with corresponding data attrs with class image-gallery
    Items format
    {
        src == src - source image url
        w == width - width of image
        h == height - height of image
    }
 */

function Gallery(overlay) {

    this.open = function (index, items, disableAnimation) {
        var options = {
            showHideOpacity: true
        };

        options.galleryUID = 1;
        options.index = parseInt(index, 10);

        if (isNaN(options.index))
            return;

        if (disableAnimation)
            options.showAnimationDuration = 0;

        new PhotoSwipe(overlay, PhotoSwipeUI_Default, items, options).init();
    };

}

$(function () {
    var $pswp=$(".pswp");
    var gallery=new Gallery($pswp[0]);

    $(document).on('click', '.image-gallery', function () {
        var clickedItem=this;
        var $items=$('.image-gallery');
        var items = [];
        var targetIndex=-1;
        $items.each(function (index, item) {
            var $item=$(item);
            items.push({
                src: $item.attr("data-src"),
                w: $item.attr("data-width"),
                h: $item.attr("data-height")
            });

            if(this===clickedItem)
                targetIndex=index;
        });

        gallery.open(targetIndex, items);
    });
});

