from dal import autocomplete
from django import forms
from django.contrib import admin
from django.forms.utils import ErrorList
from django.urls import reverse

from oscar.apps.order import admin as order_admin
from oscar.core.loading import get_model

from amocrm_api.utils import amocrm_sync_order
from catalogue.models import Pack
from checkout.utils import update_order
from feedback.admin import WatchedAdminMixin
from order.models import Order, OrderDiscount, ShippingAddress, ShippingContacts
from order.utils import send_client_order_receipt
from voucher.models import GiftCard, Voucher

Line = get_model('order', 'Line')


class LineInlineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(LineInlineForm, self).__init__(*args, **kwargs)
        self.fields['product'].required = True

        self.fields['selected_pack'].queryset = self.instance.product.packs.all() if self.instance.pk is not None else Pack.objects.none()

    class Meta:
        model = Line
        fields = '__all__'
        widgets = {
            'product': autocomplete.ModelSelect2(url='autocomplete_product'),
        }


class LineInline(order_admin.LineInline):
    form = LineInlineForm
    extra = 0
    min_num = 1
    fields = ['link', 'product', 'selected_pack', 'upc', 'quantity',
              'unit_price_excl_tax', 'line_price_retail', 'line_price_excl_tax', 'is_present'
              ]

    readonly_fields = ['link', 'upc', 'line_price_retail',
                       'unit_price_excl_tax', 'is_present'
                       ]

    def link(self, obj):
        return '<a target="_blank" href="' + reverse('admin:catalogue_product_change',
                                                     args=[obj.product.id]) + '">Открыть в новой вкладке</a>'

    link.short_description = 'Заголовок'
    link.allow_tags = True

    def options_summary(self, obj):
        return obj.options_summary

    options_summary.short_description = 'Опции товара'

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        return True if obj is None else obj.status == Order.STATUSES_NEW and obj.lines.count() > 1


class OrderDiscountInlineForm(forms.ModelForm):
    def clean(self):
        code = self.cleaned_data['voucher_code']

        try:
            voucher = Voucher.objects.get(code=code)
        except Voucher.DoesNotExist:
            raise forms.ValidationError("Купон с таким кодом не существует")

        self.cleaned_data['voucher_id'] = voucher.id
        return self.cleaned_data

    def save(self, commit=True):
        if not self.instance.voucher_id:
            # Fill fields with empty values, after order updating these fields will have actual values
            self.instance.voucher_id = self.cleaned_data['voucher_id']
            self.instance.offer_id = self.instance.voucher.offers.first().id
            self.instance.frequency = 0
            self.instance.amount = 0

            # Increment counters for saving correct statistic
            voucher = self.instance.voucher
            voucher.num_orders += 1
            voucher.save()

            offer = self.instance.offer
            offer.num_orders += 1
            offer.save()
        return super(OrderDiscountInlineForm, self).save(commit)

    class Meta:
        model = OrderDiscount
        fields = '__all__'


class OrderDiscountInline(admin.TabularInline):
    form = OrderDiscountInlineForm
    model = OrderDiscount
    extra = 0

    fields = ('voucher_id', 'voucher_name', 'voucher_code', 'amount')
    readonly_fields = ('voucher_id', 'voucher_name', 'amount')

    def voucher_name(self, obj):
        return obj.voucher.name

    voucher_name.short_description = 'Наименование'


class ShippingAddressAdminInline(admin.StackedInline):
    max_num = 1
    min_num = 0
    model = ShippingAddress


class ShippingContactsAdminInline(admin.StackedInline):
    max_num = 1
    min_num = 0
    model = ShippingContacts


class OrderForm(forms.ModelForm):
    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, use_required_attribute=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance,
                         use_required_attribute)
        self._initial_state = self._get_current_state()
        if not self._initial_state:
            self.fields['status'] = forms.ChoiceField(
                label='Статус сделки',
                choices=Order.ADMIN_CREATE_STATUSES
            )
        elif self._initial_state['status'] == -1:
            self.fields['status'] = forms.ChoiceField(
                label='Статус сделки',
                choices=Order.ADMIN_UPDATE_FROM_PRE_STATUSES
            )

    def _create_line_identifier(self, line):
        return "%s_%s_%s_%s" % (line.pk, line.product.pk, line.quantity, line.selected_pack_id)

    def _get_current_state(self):
        if self.instance.pk is not None:
            lines = [self._create_line_identifier(line) for line in self.instance.lines.all()]
            lines.sort()

            vouchers = [discount.voucher_id for discount in self.instance.discounts.all()]
            vouchers.sort()

            try:
                address = self.instance.shipping_address.concat_in_onestring()
            except ShippingAddress.DoesNotExist:
                address = ''

            return {
                'status': self.instance.status,
                'lines': lines,
                'vouchers': vouchers,
                'gift_card': self.instance.gift_card_id,
                'shipping_method': self.instance.shipping_method,
                'address': address
            }

        return None

    def is_status_changed(self):
        if self._initial_state is None or self.instance.pk is None:
            return True

        state = self._get_current_state()
        return state['status'] != self._initial_state['status']

    def is_lines_changed(self):
        if self._initial_state is None or self.instance.pk is None:
            return True

        state = self._get_current_state()
        return state['lines'] != self._initial_state['lines']

    def is_discounts_changed(self):
        if self._initial_state is None or self.instance.pk is None:
            return True

        state = self._get_current_state()
        return state['vouchers'] != self._initial_state['vouchers'] or state['gift_card'] != self._initial_state[
            'gift_card']

    def is_address_changed(self):
        if self._initial_state is None or self.instance.pk is None:
            return True

        state = self._get_current_state()
        return state['address'] != self._initial_state['address']

    def is_shipping_method_changed(self):
        if self._initial_state is None or self.instance.pk is None:
            return True

        state = self._get_current_state()
        return state['shipping_method'] != self._initial_state['shipping_method']

    def is_amocrmfields_changed(self):
        # TODO add correct logic. For now it's ok to trigger on every save
        # state = self._get_current_state()
        # if self._initial_state is None or self.instance.pk is None:
        #     return True
        #
        # if self.is_status_changed():
        #     return True
        #
        # return state['lines'] != self._initial_state['lines']
        return True

    class Meta:
        model = Order
        fields = '__all__'
        widgets = {
            'gift_card': autocomplete.ModelSelect2(url='voucher_extension:autocomplete_gift_card'),
        }


admin.site.unregister(Order)


@admin.register(Order)
class OrderAdmin(WatchedAdminMixin, order_admin.OrderAdmin):
    raw_id_fields = []

    form = OrderForm
    list_display = (
        'number', 'basket_total_excl_tax', 'total_retail', 'date_placed', 'shipping_date', 'shipping_charge_resolved',
        'amocrm_id', 'status', 'watched')
    list_filter = ('date_placed', 'status', 'watched')
    search_fields = ('number',)
    fieldsets = (
        ('О заказе', {
            'fields': [
                'basket_total_excl_tax',
                'basket_total_excl_tax_excl_gift_card',
                'total_before_discounts_excl_tax',
                'total_retail',
                'shipping_excl_tax',
                'shipping_charge_resolved',
                'payment_method',
                'gift_card'
            ]
        }),
        ('AmoCRM', {'fields': ('amocrm_id',)}),
        ('Доставка', {
            'fields': [
                'shipping_method',
                'shipping_date',
            ]
        }),
        ('', {
            'fields': ['date_placed', 'status', 'already_seen']
        })
    )

    readonly_fields = (
        'date_placed',
        'basket_total_excl_tax_excl_gift_card',
        'total_before_discounts_excl_tax',
        'basket_total_excl_tax',
        'total_retail',
        'shipping_excl_tax',
        'shipping_charge_resolved',
        'amocrm_id',
    )

    inlines = [ShippingContactsAdminInline, ShippingAddressAdminInline, OrderDiscountInline, LineInline]

    def basket_total_excl_tax(self, obj):
        return obj.basket_total_excl_tax

    basket_total_excl_tax.short_description = 'Всего'

    def basket_total_excl_tax_excl_gift_card(self, obj):
        return obj.basket_total_excl_tax_excl_gift_card

    basket_total_excl_tax_excl_gift_card.short_description = 'Цена без учета подарочной карты'

    def total_before_discounts_excl_tax(self, obj):
        return obj.total_before_discounts_excl_tax

    total_before_discounts_excl_tax.short_description = 'Цена без скидок'

    def number(self, obj):
        return obj.number

    number.short_description = 'Номер заказа'

    def shipping_method(self, obj):
        return obj.shipping_method.name

    shipping_method.short_description = 'Способ доставки'

    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)
        if form.instance.pk:
            # Catch changes of lines and recalculate total and etc
            deleted_products_pks = [l.product_id for l in formsets[-1].deleted_objects]
            update_order(form.instance, deleted_products_pks, {
                'lines': form.is_lines_changed(),
                'discounts': form.is_discounts_changed(),
                'address': form.is_address_changed(),
                'shipping_method': form.is_shipping_method_changed()
            })

        if form.is_status_changed():
            instance = form.instance
            if instance.status == Order.SEND_RECEIPT_STATUS:
                send_client_order_receipt(instance)

        # Update moved here from post_save because we need sync after related models or status were changed
        if form.instance.pk:
            if ((form.is_amocrmfields_changed() or form.instance.amocrm_id == -1) and
                    form.instance.status not in Order.DO_NOT_SYNC_STATUSES):
                amocrm_sync_order(form.instance.pk)

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    class Media:
        js = ('order/js/order-admin.js#1', )
