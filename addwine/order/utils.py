from django.forms import model_to_dict

from customer.models import CommunicationEventType
from feedback.utils import send_mail
from order.models import ShippingAddress

CLIENT_ORDER_RECEIPT = 'CLIENT_ORDER_RECEIPT'


def send_client_order_receipt(order):
    if not order.shipping_contacts.email:
        return

    shipping_date = order.shipping_date
    if shipping_date:
        shipping_date = shipping_date.strftime('%d.%m.%Y')

    ctx = {
        'id': order.id,
        'total': order.basket_total_excl_tax,   # prev_val 'total': order.total_excl_tax,
        'payment_method': order.payment_method.name,
        'lines': [model_to_dict(line) for line in order.lines.all()],
        'date_placed': order.date_placed,

        'shipping': order.shipping_excl_tax,
        'shipping_method': {
            'name': order.shipping_method.name,
            'description': order.shipping_method.description
        },
        'shipping_charge_resolved': order.shipping_charge_resolved,
        'shipping_date': shipping_date,

        'shipping_contacts': model_to_dict(order.shipping_contacts),
    }
    ctx['shipping_contacts']['phone_number'] = ctx['shipping_contacts']['phone_number'].as_international

    try:
        ctx['shipping_address'] = model_to_dict(order.shipping_address)
    except ShippingAddress.DoesNotExist:
        pass

    send_mail(CommunicationEventType.objects.get(code=CLIENT_ORDER_RECEIPT), ctx, [order.shipping_contacts.email])


def mail_to_file(ctx):
    comm_event_obj = CommunicationEventType.objects.get(code=CLIENT_ORDER_RECEIPT)
    message = comm_event_obj.get_messages(ctx=ctx)

    with open('email.html', 'w') as f:
        f.write(message['body'])

