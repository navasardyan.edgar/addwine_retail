from django.conf import settings

GET_SITE_CONFIGS_TEMPLATE_TAG_NAME = getattr(settings,
    'GET_SITE_CONFIGS_TEMPLATE_TAG_NAME', 'get_configs')

# The cache that should be used, e.g. 'default'. Refers to Django CACHES setting.
# Set to None to disable caching.
SITE_CONFIGS_CACHE = None

SITE_CONFIGS_CACHE_TIMEOUT = 60 * 5

SITE_CONFIGS_CACHE_PREFIX = 'site_configs'
