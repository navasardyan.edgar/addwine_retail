import logging

from .base import *

DEBUG = False
SECRET_KEY = "zV5D42vLIMsfGZtEEzkKzz4mUXWyBxXyHg7f2tDVeyzavQsLPapEt9S6xxwN23Ep"
TEST_RUNNER = "django.test.runner.DiscoverRunner"
TEST_MODE_FOR_PHONE = True

# CACHES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache", "LOCATION": ""
    }
}

# DATABASES
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'addwine_test_db'
    }
}

if os.environ.get('TESTS_WITHOUT_MIGRATIONS', False):
    MIGRATION_MODULES = {
        app.split('.')[-1]: None for app in INSTALLED_APPS
    }

# CELERY
CELERY_ALWAYS_EAGER = True
CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
BROKER_BACKEND = 'redis'
CELERY_BROKER_URL = 'redis://'

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES[0]["OPTIONS"]["debug"] = DEBUG  # noqa F405
TEMPLATES[0]["OPTIONS"]["loaders"] = [  # noqa F405
    (
        "django.template.loaders.cached.Loader",
        [
            "django.template.loaders.filesystem.Loader",
            "django.template.loaders.app_directories.Loader",
        ],
    )
]

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"
# https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = "localhost"
# https://docs.djangoproject.com/en/dev/ref/settings/#email-port
EMAIL_PORT = 1025


YANDEX_MARKET_ENABLED = False
AMOCRM_SYNC_ENABLED = True
UNISENDER_SYNC_ENABLED = True
MOI_SKLAD_SYNC_ENABLED = True

TELEGRAM_BOT_SET_WEBHOOK = False
LOGGING['handlers'] = {
    'mail_admins': {
        'level': 'ERROR',
        'filters': ['require_debug_false'],
        'class': 'logging.StreamHandler',
    },
    'telegram_bot': {
        'level': 'WARNING',
        'class': 'logging.StreamHandler',
        'formatter': 'default',
    },
    'amocrm': {
        'level': 'INFO',
        'class': 'logging.StreamHandler',
        'formatter': 'default',
    },
    'unisender': {
        'level': 'INFO',
        'class': 'logging.StreamHandler',
        'formatter': 'default',
    },
    'celery': {
        'level': 'DEBUG',
        'class': 'logging.StreamHandler',
        'formatter': 'default',
    },
    'project': {
        'level': 'DEBUG',
        'class': 'logging.StreamHandler',
        'formatter': 'default',
    },
    'errors': {
        'level': 'DEBUG',
        'class': 'logging.StreamHandler',
        'formatter': 'default'
    },
    'doc_import': {
        'level': 'INFO',
        'class': 'logging.StreamHandler',
        'formatter': 'default',
    }
}
logging.disable(logging.CRITICAL)
