from django.conf.urls import url, include
from api.routes import router


urlpatterns = [
    url(r'', include(router.urls))
]

