from rest_framework.routers import DefaultRouter

from api.views import ProductViewSet


router = DefaultRouter()
router.register('products', ProductViewSet, base_name='products')
