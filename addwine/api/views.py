from rest_framework.decorators import action
from rest_framework import status, viewsets
from rest_framework.response import Response


from catalogue.api.serializers import PackSerializer
from catalogue.models import Product


class ProductViewSet(viewsets.GenericViewSet):
    # lookup_field = "ticker"
    queryset = Product.objects.all()

    @action(detail=True, methods=['get'])
    def packs(self, request, *args, **kwargs):
        product = self.get_object()
        qs = product.packs.all()
        serializer = PackSerializer(qs, many=True)
        return Response(serializer.data)
