from django import forms
from phonenumber_field.formfields import PhoneNumberField

from catalogue.models import Product
from shipping.models import OrderAndItemCharges


class ShippingForm(forms.Form):
    shipping_method = forms.ModelChoiceField(OrderAndItemCharges.objects.all())

    shipping_date = forms.DateField()
    address_onestring = forms.CharField(max_length=1024, required=False)
    address_region = forms.CharField(max_length=1024, required=False)
    address_city = forms.CharField(max_length=1024, required=False)
    address_street = forms.CharField(max_length=1024, required=False)
    address_house = forms.CharField(max_length=6, required=False)
    address_flat = forms.CharField(required=False)
    address_block = forms.CharField(required=False)
    address_notes = forms.CharField(max_length=2048, required=False)

    conditional_require_fields = ['shipping_date', 'address_onestring', 'address_region', 'address_city',
                                  'address_street', 'address_house',]

    def clean(self):
        shipping_method = self.cleaned_data['shipping_method']

        if shipping_method.require_address:
            self.fields_required(self.conditional_require_fields)
        else:
            for field in self.conditional_require_fields:
                self.cleaned_data[field] = None

        return self.cleaned_data

    def fields_required(self, fields):
        for field in fields:
            if not self.cleaned_data.get(field, ''):
                msg = forms.ValidationError("This field is required.")
                self.add_error(field, msg)


class ContactsForm(forms.Form):
    name = forms.CharField(max_length=255)
    email = forms.EmailField(max_length=255, required=False)
    phone_number = PhoneNumberField()
    notes = forms.CharField(max_length=2048, required=False)


class FastCheckoutForm(forms.Form):
    first_name = forms.CharField(max_length=255)
    product = forms.ModelChoiceField(Product.objects.all())
    phone_number = PhoneNumberField()
    quantity = forms.IntegerField(min_value=1, required=False)
    pack = forms.IntegerField(min_value=1, required=False)


class BasketFastCheckoutForm(forms.Form):
    first_name = forms.CharField(max_length=255)
    phone_number = PhoneNumberField()
