from ckeditor.fields import RichTextField
from django.db import models
from treebeard.mp_tree import MP_Node


class FAQCategory(MP_Node):
    title = models.CharField(verbose_name='название', max_length=1024)
    _full_name_separator = ' > '

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        """
        Returns a string representation of the category and it's ancestors,
        e.g. 'Books > Non-fiction > Essential programming'.

        It's rarely used in Oscar's codebase, but used to be stored as a
        CharField and is hence kept for backwards compatibility. It's also
        sufficiently useful to keep around.
        """
        names = [category.title for category in self.get_ancestors_and_self()]
        return self._full_name_separator.join(names)

    def get_ancestors_and_self(self):
        """
        Gets ancestors and includes itself. Use treebeard's get_ancestors
        if you don't want to include the category itself. It's a separate
        function as it's commonly used in templates.
        """
        return list(self.get_ancestors()) + [self]

    def get_descendants_and_self(self):
        """
        Gets descendants and includes itself. Use treebeard's get_descendants
        if you don't want to include the category itself. It's a separate
        function as it's commonly used in templates.
        """
        return list(self.get_descendants()) + [self]

    def has_children(self):
        return self.get_num_children() > 0

    def get_num_children(self):
        return self.get_children().count()

    class Meta:
        verbose_name = 'категория FAQ'
        verbose_name_plural = 'категории FAQ'


class FAQQuestion(models.Model):
    category = models.ForeignKey(FAQCategory, verbose_name='категория', on_delete=models.CASCADE,
                                 related_name='questions')
    question = models.CharField(verbose_name="вопрос", max_length=2048)
    answer = RichTextField(verbose_name="ответ")
    created = models.DateTimeField("создано", auto_now_add=True)
    order_field = models.PositiveIntegerField(verbose_name='нажать и тащить', default=0, blank=False, null=False)

    def __str__(self):
        return self.question

    class Meta:
        verbose_name = "вопрос"
        verbose_name_plural = "FAQ"
        ordering = ('order_field',)
