from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin
from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory

from faq import models


@admin.register(models.FAQCategory)
class FAQCategoryAdmin(TreeAdmin):
    form = movenodeform_factory(models.FAQCategory)
    fields = ('title','_position', '_ref_node_id',)
    list_display = ('title',)
    search_fields = ('title',)


@admin.register(models.FAQQuestion)
class FAQQuestionAdmin(SortableAdminMixin, admin.ModelAdmin):
    fields = ('category', 'question', 'answer')
    list_display = ('question', 'answer', 'category', 'created')
    search_fields = ('question',)
    list_filter = ('category', 'created',)
