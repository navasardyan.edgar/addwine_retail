# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-01 11:41
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0002_auto_20170826_1848'),
    ]

    operations = [
        migrations.CreateModel(
            name='FAQCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=1024, verbose_name='название')),
                ('order_field', models.PositiveIntegerField(default=0, verbose_name='нажать и тащить')),
            ],
            options={
                'verbose_name': 'категория FAQ',
                'ordering': ('order_field',),
                'verbose_name_plural': 'категории FAQ',
            },
        ),
        migrations.AlterModelOptions(
            name='faqquestion',
            options={'ordering': ('order_field',), 'verbose_name': 'вопрос', 'verbose_name_plural': 'FAQ'},
        ),
        migrations.AlterField(
            model_name='faqquestion',
            name='answer',
            field=ckeditor.fields.RichTextField(verbose_name='ответ'),
        ),
        migrations.AlterField(
            model_name='faqquestion',
            name='created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='создано'),
        ),
        migrations.AlterField(
            model_name='faqquestion',
            name='order_field',
            field=models.PositiveIntegerField(default=0, verbose_name='нажать и тащить'),
        ),
        migrations.AlterField(
            model_name='faqquestion',
            name='question',
            field=models.CharField(max_length=2048, verbose_name='вопрос'),
        ),
        migrations.AddField(
            model_name='faqquestion',
            name='category',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='faq.FAQCategory', verbose_name='категория'),
            preserve_default=False,
        ),
    ]
