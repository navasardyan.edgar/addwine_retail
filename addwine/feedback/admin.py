import itertools
from django.contrib import admin
from feedback.models import BackCall, SliderCTARequest, Question


class WatchedAdminMixin(object):
    def get_readonly_fields(self, request, obj=None):
        return list(super().get_readonly_fields(request, obj)) + ['already_seen', ]

    def already_seen(self, object):
        if object.id:
            if not object.watched:
                object.watched = True
                object.save()

        return object.watched

    already_seen.short_description = "Просмотрено"
    already_seen.boolean = True


class ReadOnlyAdminMixin(object):
    def __init__(self, model):
        self.readonly_fields = [field.name for field in model._meta.fields]
        self.readonly_model = model

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return request.user.is_staff

    class Media:
        css = {
            'all': ('feedback/admin/css/read-only-view.css',)
        }


@admin.register(BackCall)
class BackCallAdmin(WatchedAdminMixin, ReadOnlyAdminMixin, admin.ModelAdmin):
    def __init__(self, model, admin_site):
        ReadOnlyAdminMixin.__init__(self, model)
        admin.ModelAdmin.__init__(self, model, admin_site)

    fields = ('first_name', 'last_name', 'phone_number', 'amocrm_id', 'already_seen')
    list_display = ('phone_number', 'first_name', 'last_name', 'amocrm_id', 'created', 'watched')
    search_fields = ('contact', 'first_name', 'last_name')
    list_filter = ('created', 'watched',)


@admin.register(SliderCTARequest)
class SliderCTARequestAdmin(WatchedAdminMixin, ReadOnlyAdminMixin, admin.ModelAdmin):
    def __init__(self, model, admin_site):
        ReadOnlyAdminMixin.__init__(self, model)
        admin.ModelAdmin.__init__(self, model, admin_site)

    fields = ('slide', 'first_name', 'last_name', 'contact', 'already_seen')
    list_display = ('slide', 'first_name', 'last_name', 'contact', 'created', 'watched')
    search_fields = ('contact', 'first_name', 'last_name')
    list_filter = ('slide', 'created', 'watched',)


@admin.register(Question)
class QuestionAdmin(WatchedAdminMixin, ReadOnlyAdminMixin, admin.ModelAdmin):
    def __init__(self, model, admin_site):
        ReadOnlyAdminMixin.__init__(self, model)
        admin.ModelAdmin.__init__(self, model, admin_site)

    fields = ('first_name', 'last_name', 'email', 'phone_number', 'text', 'amocrm_id', 'already_seen')
    list_display = ('email', 'phone_number','first_name', 'last_name', 'amocrm_id', 'created', 'watched')
    search_fields = ('first_name', 'last_name', 'email', 'phone_number')
    list_filter = ('created', 'watched')
