from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core import urlresolvers
from django.db import models

from customer.models import Contacts
from marketing.models import SliderCTA


class WatchedMixinModel(models.Model):
    watched = models.BooleanField('Просмотрено', default=False)

    class Meta:
        abstract = True


class BaseFeedbackModel(models.Model):
    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return ''.join([
            'https://',
            Site.objects.get_current().domain,
            urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        ])

    created = models.DateTimeField("Создано", auto_now_add=True)

    def __str__(self):
        return self._meta.verbose_name

    class Meta:
        abstract = True


class BackCall(WatchedMixinModel, Contacts, BaseFeedbackModel):
    class Meta:
        verbose_name = "обратный звонок"
        verbose_name_plural = "обратный звонок"


class SliderCTARequest(WatchedMixinModel, Contacts, BaseFeedbackModel):
    slide = models.ForeignKey(SliderCTA, verbose_name='Слайд')
    contact = models.CharField("Контакт", max_length=255)

    def as_dict(self):
        data = super().as_dict()
        data['slide'] = self.slide.as_dict()
        return data

    class Meta:
        verbose_name = "запись на тест драйв"
        verbose_name_plural = "записи на тест драйв"


class Question(WatchedMixinModel, Contacts, BaseFeedbackModel):

    text = models.TextField("Вопрос")

    class Meta:
        verbose_name = "вопрос"
        verbose_name_plural = "вопросы"
