from django import forms
from django.core.exceptions import ValidationError

from customer.forms import ContactsForm
from feedback.models import BackCall, SliderCTARequest, Question

from web.mixins import RecaptchaFormMixin


class BackCallForm(forms.ModelForm):
    name = forms.CharField(label='Фамилия Имя Отчество', max_length=512, required=True)

    def __init__(self, *args, **kwargs):
        super(BackCallForm, self).__init__(*args, **kwargs)
        self.fields['phone_number'].required = True

    class Meta:
        model = BackCall
        exclude = ('amocrm_id', 'watched', 'created',)


class SliderCTARequestForm(forms.ModelForm):
    name = forms.CharField(label='Фамилия Имя Отчество', max_length=512, required=False)
    contact = forms.CharField(max_length=255)

    def clean_contact(self):
        contact = self.cleaned_data['contact']
        contacts_form = ContactsForm({'email': contact, 'phone_number': contact})
        contacts_form.is_valid()

        email = contacts_form.cleaned_data.get('email')
        phone_number = contacts_form.cleaned_data.get('phone_number')
        if not email and not phone_number:
            raise ValidationError(
                'Invalid value for field contact: %(value)s must be phone_number or email',
                params={'value': contact},
            )

        self.data = dict(self.data)
        if email:
            self.changed_data.append('email')
            self.data['email'] = contact
            self.cleaned_data['email'] = contact
            return email
        if phone_number:
            self.changed_data.append('phone_number')
            self.data['phone_number'] = contact
            self.cleaned_data['phone_number'] = contact
            return phone_number

    class Meta:
        model = SliderCTARequest
        exclude = ('watched', 'created',)


class QuestionForm(RecaptchaFormMixin, forms.ModelForm):
    name = forms.CharField(label='Фамилия Имя Отчество', max_length=512, required=False)

    def __init__(self, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['phone_number'].required = False

    class Meta:
        model = Question
        exclude = ('amocrm_id', 'watched', 'created')
