from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core import mail
import logging

from customer.models import CommunicationEventType
from feedback.settings import FEEDBACK_TARGET

from common.utils import get_admin_settings

logger = get_task_logger(__name__)

TASKS_CONFIGS_SEND_MESSAGE_WITH_TEMPLATE = {
    'name': "feedback.send_message_with_template",
    'ignore_result': True
}
TASKS_CONFIGS_SEND_MESSAGE_WITH_TEMPLATE.update(settings.FEEDBACK_TASK_CONFIG)


@shared_task(**TASKS_CONFIGS_SEND_MESSAGE_WITH_TEMPLATE)
def send_message_with_template(comm_event_obj_id, ctx=None, recipients=None):
    try:
        comm_event_obj = CommunicationEventType.objects.get(id=comm_event_obj_id)
    except CommunicationEventType.DoesNotExist:
        logger.error("Built-in email template with id \'%s\' does not exists when task was started" % (comm_event_obj_id, ))
        return
    admin_settings = get_admin_settings()
    message = comm_event_obj.get_messages(ctx=ctx)
    if recipients is None:
        recipients = [getattr(admin_settings, 'email', FEEDBACK_TARGET)]
    mail.send_mail(message['subject'], message['body'], settings.FEEDBACK_FROM, recipients,
                   html_message=message['body'])
