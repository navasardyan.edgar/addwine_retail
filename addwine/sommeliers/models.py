from django.conf import settings
from django.contrib.sites.models import Site
from django.core import urlresolvers
from django.db import models
from django.db.models.signals import pre_save, post_init
from django.forms import model_to_dict
from django.utils import timezone

from amocrm_api.models import AbstractAmoCRMFields
from catalogue.models import Product, ContentType
from customer.models import User, Contacts
from marketing.models import SommelierCTA


def is_sommelier(user):
    return user.groups.filter(name=settings.SOMMELIER_GROUP_NAME).exists()


class SommelierTask(AbstractAmoCRMFields, models.Model):
    cta = models.ForeignKey(SommelierCTA, verbose_name='информация о запросе')
    product = models.ForeignKey(Product, verbose_name='продукт', blank=True, null=True)
    contacts = models.ForeignKey(Contacts, verbose_name='контакты клиента', related_name='sommelier_request')
    sommelier = models.ForeignKey(User, verbose_name='сомелье', related_name='sommelier_tasks', blank=True, null=True)
    is_free = models.BooleanField('свободно', default=False)
    is_complete = models.BooleanField('выполнено', default=False)
    submited_date = models.DateTimeField(verbose_name='дата принятия', blank=True, null=True)
    created_date = models.DateTimeField(verbose_name='дата создания', auto_now_add=True)

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return ''.join([
            'https://',
            Site.objects.get_current().domain,
            urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        ])

    def as_dict(self):
        data = model_to_dict(self)
        data['created_date'] = self.created_date.strftime("%H:%M  %d.%m.%Y")
        data['contacts'] = self.contacts.as_dict()
        data['cta'] = self.cta.as_dict()
        if self.sommelier:
            data['sommelier'] = self.sommelier.as_dict()
        if self.product:
            data['product'] = self.product.as_dict()
        data['admin_url'] = self.get_admin_url()
        return data

    @staticmethod
    def pre_save(sender, **kwargs):
        instance = kwargs.get('instance')
        instance.is_free = instance.sommelier is None

        instance.contacts.amocrm_id = instance.amocrm_id
        instance.contacts.save()

        if not instance.is_free and (instance.prev_is_free or instance.pk is None):
            instance.submited_date = timezone.now()

    @staticmethod
    def remember_prev_is_free(sender, **kwargs):
        instance = kwargs.get('instance')
        instance.prev_is_free = instance.is_free

    def __str__(self):
        return 'Вопрос от %s' % self.contacts.name

    class Meta:
        verbose_name = 'задача для сомелье'
        verbose_name_plural = 'задачи для сомелье'


pre_save.connect(SommelierTask.pre_save, sender=SommelierTask)
post_init.connect(SommelierTask.remember_prev_is_free, sender=SommelierTask)


class SommelierTaskSubmited(SommelierTask):

    def as_dict(self):
        data = super().as_dict()
        data['admin_url'] = ''.join([
            'https://',
            Site.objects.get_current().domain,
            urlresolvers.reverse("admin:sommeliers_sommeliertasksubmited_change", args=(self.id,))
        ])
        return data

    class Meta:
        verbose_name = 'задача для сомелье'
        verbose_name_plural = 'задачи для сомелье'
        proxy = True
