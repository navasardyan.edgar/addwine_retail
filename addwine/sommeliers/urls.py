from django.conf.urls import url

from common.views import get_admin_autocomplete_view
from customer.models import User

urlpatterns = [
    url(r'catalogue/autocomplete/sommelier/sommeliers',
        get_admin_autocomplete_view(User,'username',
                                    additional_filter={'groups__name__iexact': 'Сомелье'}).as_view()
        , name='autocomplete_sommelier'),
]