from amocrm_api.api.base import Client, prepare_field_value
from time import time


class OrderLeadsApi(Client):
    PIPELINE_ID = 592711

    RETAIL_FIELD_ID = 64256
    SITE_ID_FIELD_ID = 207525
    ADMIN_URL_FIELD_ID = 206897

    ADDRESS_FIELD_ID = 221255  # Адрес получателя
    PHONE_FIELD_ID = 221251  # Телефон получателя
    FIO_FIELD_ID = 221253  # ФИО получателя
    TRADE_DATE_FIELD_ID = 60602  # Дата сделки

    STORE_FIELD_ID = 222705  # Склад
    STORE_FIELD_DEFAULT_VALUE = 457975
    ORGANIZATION_FIELD_ID = 222703  # Организация
    ORGANIZATION_FIELD_DEFAULT_VALUE = 457973

    KWARGS_FIELD_MAP = {
        'price_retail': RETAIL_FIELD_ID,
        'site_id': SITE_ID_FIELD_ID,
        'admin_url': ADMIN_URL_FIELD_ID,

        'address': ADDRESS_FIELD_ID,
        'phone': PHONE_FIELD_ID,
        'fio': FIO_FIELD_ID,
        'date_delivery': TRADE_DATE_FIELD_ID,

        'organization': ORGANIZATION_FIELD_ID,
        'store': STORE_FIELD_ID,
    }

    STATUSES_NEW = 14811802
    STATUSES_SUBMITTED = 14811805
    STATUSES_DELIVER = 17597569
    STATUSES_DELIVERED = 14823253
    STATUSES_CLOSED = 142
    STATUSES_REJECTED = 143
    STATUSES_NEW_PHONE = 19035355
    STATUSES_CHANGED = 18900187
    STATUSES_PICKUP = 19001272
    STATUSES_PICKUP_COMPLETE = 19035604

    STATUS_PIPELINE = [
        STATUSES_NEW, STATUSES_SUBMITTED, STATUSES_DELIVER, STATUSES_DELIVERED,
        STATUSES_CLOSED, STATUSES_REJECTED, STATUSES_NEW_PHONE, STATUSES_CHANGED,
        STATUSES_PICKUP, STATUSES_PICKUP_COMPLETE
    ]

    ENDPOINT = 'leads'

    def add(self, name, status, price, **kwargs):
        resp = self.send(
            'POST', self.TYPE_SET, self.OPERATION_ADD,
            [
                {
                    'name': name,
                    'pipeline_id': self.PIPELINE_ID,
                    'status_id': status,
                    'price': price,
                    'tags': [self.ADDWINE_TAG_ID],
                    'custom_fields': self._get_custom_fields(**kwargs)
                }
            ]
        )
        return resp

    def update(self, id, name, status, price, **kwargs):
        resp = self.send(
            'POST', self.TYPE_SET, self.OPERATION_UPDATE,
            [
                {
                    'id': id,
                    'last_modified': time(),
                    'name': name,
                    'pipeline_id': self.PIPELINE_ID,
                    'status_id': status,
                    'price': price,
                    'custom_fields': self._get_custom_fields(**kwargs)
                }
            ]
        )
        return resp

    def _get_custom_fields(self, **kwargs):
        fields = kwargs
        fields['organization'] = self.ORGANIZATION_FIELD_DEFAULT_VALUE
        fields['store'] = self.STORE_FIELD_DEFAULT_VALUE
        return [
            {
                'id': self.KWARGS_FIELD_MAP[key],
                'values': [
                    {'value': prepare_field_value(value)}
                ]
            } for key, value in fields.items()
        ]


class SiteInstanceLeadsApi(Client):
    PIPELINE_ID = 928567
    ADMIN_URL_FIELD_ID = 206897

    ENDPOINT = 'leads'

    def add(self, name, admin_url):
        resp = self.send('POST', self.TYPE_SET, self.OPERATION_ADD,
                         [
                             {
                                 'pipeline_id': self.PIPELINE_ID,
                                 'name': name,
                                 'price': 0,
                                 'tags': [self.ADDWINE_TAG_ID],
                                 'custom_fields': [
                                     {
                                         'id': self.ADMIN_URL_FIELD_ID,
                                         "values": [
                                             {'value': admin_url}
                                         ]
                                     },
                                 ]
                             }
                         ]
                         )

        return resp

    def update(self, id, name, admin_url):
        resp = self.send('POST', self.TYPE_SET, self.OPERATION_UPDATE,
                         [
                             {
                                 'id': id,
                                 'pipeline_id': self.PIPELINE_ID,
                                 'last_modified': time(),
                                 'name': name,
                                 'price': 0,
                                 'custom_fields': [
                                     {
                                         'id': self.ADMIN_URL_FIELD_ID,
                                         "values": [
                                             {'value': admin_url}
                                         ]
                                     },
                                 ]
                             }
                         ]
                         )

        return resp


class BackCallLeadsApi(SiteInstanceLeadsApi):
    PIPELINE_ID = 928567


class SommelierTaskLeadsApi(SiteInstanceLeadsApi):
    PIPELINE_ID = 928570


class FAQQuestionLeadsApi(SiteInstanceLeadsApi):
    PIPELINE_ID = 928576


class SubscriptionForDiscountLeadsApi(SiteInstanceLeadsApi):
    PIPELINE_ID = 959827


class ProductAlertLeadsApi(SiteInstanceLeadsApi):
    PIPELINE_ID = 1067560


class BirthdayLeadsApi(Client):
    PIPELINE_ID = 1110007

    PHONE_NUMBER_FIELD_ID = 60502
    EMAIL_FIELD_ID = 60504
    BIRTHDAY_FIELD_ID = 63374

    ENDPOINT = 'leads'

    @classmethod
    def format_date(cls, date):
        if date:
            return date.strftime('%Y-%m-%d %H:%M:%S')
        else:
            return ''

    def add(self, id, name, phones, emails, birthday, address, linked_leads_id=None):
        contact_data = {
             'pipeline_id': self.PIPELINE_ID,
             'name': name,
             'price': 0,
             'contacts_id': id,
             'tags': [self.ADDWINE_TAG_ID],
         }
        if linked_leads_id:
            contact_data['linked_leads_id'] = linked_leads_id
        resp = self.send('POST', self.TYPE_SET, self.OPERATION_ADD, [contact_data])

        return resp

    def update(self, id, name, phones, emails, birthday, address, linked_leads_id=None):
        resp = self.add(id, name, phones, emails, birthday, address, linked_leads_id)
        return resp

