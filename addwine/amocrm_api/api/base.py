from datetime import date

import simplejson as json
import logging
import requests
from django.conf import settings

logger = logging.getLogger(__name__)


def format_date(date_value):
    return date_value.strftime('%Y-%m-%d %H:%M:%S') if date_value else ''


def prepare_field_value(value):
    if type(value) == date:
        value = format_date(value)
    return value


class Client(object):
    URL = 'https://' + settings.AMOCRM_HOST_NAME + '.amocrm.ru/private/api/'
    ENDPOINTS_URL = URL + 'v2/json/'
    AUTH_URL = URL + "auth.php?type=json"

    # Every child must override that
    ENDPOINT = ''

    TYPE_SET = 'set'
    TYPE_LINKS = 'links'
    TYPE_LIST = 'list'

    OPERATION_ADD = 'add'
    OPERATION_UPDATE = 'update'

    ADDWINE_TAG_ID = 229379  # Magic value, get from amocrm site

    def wrap_data(self, endpoint, operation, data):
        # it is simple json, it necessary for correct dumps of decimal filds
        return json.dumps({
            'request': {
                endpoint: {
                        operation: data
                }
            }
        }, use_decimal=True)

    def extract_data(self, endpoint, operation, data):
        return data['response'][endpoint][operation]

    def auth(self):
        session = requests.session()
        try:
            resp = session.post(self.AUTH_URL, {
                'USER_LOGIN': settings.AMOCRM_USER_LOGIN,
                'USER_HASH': settings.AMOCRM_API_KEY
            })

            if resp.status_code != 200:
                logger.error('Authorization failed with status %s. Response %s' % (resp.status_code, resp))
                return None

            resp = resp.json()
        except requests.exceptions.RequestException as e:
            logger.exception('Authorization failed with error: \n %s' % e)
            return None

        if not resp['response']['auth']:
            logger.error('Authorization failed, check credentials data. Response: %s' % resp)
            return None

        return session

    def make_url(self, op_type):
        return self.ENDPOINTS_URL+self.ENDPOINT+'/'+op_type+'/'

    def send(self, method, op_type, operation, data):
        session = self.auth()

        if session is None:
            return None

        url = self.make_url(op_type)
        try:
            if method == 'POST':
                resp = session.post(url, self.wrap_data(self.ENDPOINT, operation, data))
            else:
                resp = session.get(url, params=data)
            if resp.status_code != 200:
                logger.error('Request failed with status %s. Response %s' % (resp.status_code, resp.text))
                return None
            resp = resp.json()
        except requests.exceptions.RequestException as e:
            logger.exception(e)
            return None

        try:
            resp = self.extract_data(self.ENDPOINT, operation, resp)
        except KeyError as e:
            logger.exception('Got unrecognized response: ' + str(resp))
            return None

        if resp:
            try:
                if 'error' in resp[0]:
                    logger.error('AmoCRM API error %s' % (resp))
                    return None
            except (IndexError, KeyError):
                return resp

        return resp
