import datetime
from django.conf import settings
import logging
import requests
from amocrm_api.api.base import Client, format_date
from time import time

logger = logging.getLogger(__name__)


class ContactsApi(Client):
    PHONE_NUMBER_FIELD_ID = 60502
    EMAIL_FIELD_ID = 60504
    BIRTHDAY_FIELD_ID = 63374
    ADDRESS_FIELD_ID = 222711  # Фактический адрес

    ENDPOINT = 'contacts'

    BLANK_NAME_LITERAL = 'Имя неизвестно'

    @classmethod
    def parse_date(cls, date_str):
        return datetime.datetime.strptime(date_str, '%d.%m.%Y')

    def get_contacts_leeds_id(self, amocrm_id):
        session = self.auth()
        if session is None:
            return None
        url = 'https://' + settings.AMOCRM_HOST_NAME + '.amocrm.ru/api/v2/contacts/'
        params = {
            'id': amocrm_id
        }

        try:
            resp = session.get(url, params=params)
            if resp.status_code != 200:
                logger.error('Request failed with status %s. Response %s' % (resp.status_code, resp.text))
                return None
            resp = resp.json()
        except requests.exceptions.RequestException as e:
            logger.exception(e)
            return None

        try:
            resp_leeds = resp['_embedded']['items'][0]['leads']
            resp_leeds_ids = resp_leeds.get('id', [])
        except KeyError as e:
            logger.exception('Got unrecognized response: ' + str(resp))
            return None

        return resp_leeds_ids

    def add(self, name, phones, emails, birthday, address, linked_leads_id=None):
        if not name:
            name = self.BLANK_NAME_LITERAL

        contact_data = {
                    'name': name,
                    'custom_fields': [
                        {
                            'id': self.PHONE_NUMBER_FIELD_ID,
                            "values": [
                                 {'value': phone, 'enum': 'MOB'} for phone in phones
                            ]
                        },
                        {
                            'id': self.EMAIL_FIELD_ID,
                            "values": [
                                 {'value': email, 'enum': 'WORK'} for email in emails
                            ]
                        },
                        {
                            'id': self.BIRTHDAY_FIELD_ID,
                            "values": [
                                 {'value': format_date(birthday)}
                            ]
                        },
                        {
                            'id': self.ADDRESS_FIELD_ID,
                            'values': [{'value': address}]
                        },
                    ]
                }
        if linked_leads_id:
          contact_data['linked_leads_id'] = linked_leads_id
        resp = self.send('POST', self.TYPE_SET, self.OPERATION_ADD, [contact_data])

        return resp

    def update(self, id, name, phones, emails, birthday, address, linked_leads_id=None):
        if not name:
            name = self.BLANK_NAME_LITERAL

        contact_data = {
                    'id': id,
                    'last_modified': time(),
                    'name': name,
                    'custom_fields': [
                        {
                             'id': self.PHONE_NUMBER_FIELD_ID,
                             "values": [
                                 {'value': phone, 'enum': 'MOB'} for phone in phones
                            ]
                        },
                        {
                            'id': self.EMAIL_FIELD_ID,
                            "values": [
                                 {'value': email, 'enum': 'WORK'} for email in emails
                            ]
                        },
                        {
                            'id': self.BIRTHDAY_FIELD_ID,
                            "values": [
                                 {'value': format_date(birthday)}
                            ]
                        },
                        {
                            'id': self.ADDRESS_FIELD_ID,
                            'values': [{'value': address}]
                        },
                    ]
                }
        if linked_leads_id:
            contact_data['linked_leads_id'] = linked_leads_id
        resp = self.send('POST', self.TYPE_SET, self.OPERATION_UPDATE, [contact_data])
        return resp