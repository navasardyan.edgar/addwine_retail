import os, json
import requests_mock
from django.test.testcases import TestCase
from .fabric import UserFactory

from amocrm_api.tasks import BirthdayLeadApiTaskRequest, BirthdayLeadsApi


def response_from_file(filename):
    with open(os.path.join(os.getcwd(), 'amocrm_api', 'test', 'mock_responses', filename), 'r', encoding='utf8') as f:
        data = json.load(f)
    return data


class TestBirthdayLeadApiTaskRequest(TestCase):
    def setUp(self):
        self.user = UserFactory.build()
        self.user.amocrm_id = 1099181
        self.user.save()

    @requests_mock.Mocker()
    def test_success_lead_create(self, m):
        b_task = BirthdayLeadApiTaskRequest()

        mock_auth_url = 'https://justaddwine.amocrm.ru/private/api/auth.php?type=json'
        m.post(mock_auth_url,
               status_code=200,
               headers={'content-type': 'application/json'},
               json={
                   'response': {
                       'auth': 'ok_mock_XD'
                   }
               }
               )
        mock_get_contact_url = 'https://justaddwine.amocrm.ru/api/v2/contacts/?id={}'.format(self.user.amocrm_id)
        m.get(mock_get_contact_url,
              status_code=200,
              headers={'content-type': 'application/json'},
              json=response_from_file('get_contact_id_1099181.json'),
              )

        mock_post_leads_set_url = 'https://justaddwine.amocrm.ru/private/api/v2/json/leads/set/'
        m.post(mock_post_leads_set_url,
               status_code=200,
               headers={'content-type': 'application/json'},
               json=response_from_file('leads_set.json'),
               )

        mock_post_contacts_set_url = 'https://justaddwine.amocrm.ru/private/api/v2/json/contacts/set/'
        m.post(mock_post_contacts_set_url,
               status_code=200,
               headers={'content-type': 'application/json'},
               json=response_from_file('contact_set.json'),
               )
        resp = b_task.update(BirthdayLeadsApi(), self.user, self.user.amocrm_id)
        self.assertEqual(resp, [{'id': 894321, 'request_id': 0}])
