import json
import logging

import re
from collections import defaultdict

from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from phonenumber_field.phonenumber import PhoneNumber

from amocrm_api.api.contacts import ContactsApi
from amocrm_api.api.leads import OrderLeadsApi
from customer.models import User, Email, Phone
from order.models import Order
from order.utils import send_client_order_receipt

logger = logging.getLogger(__name__)


@csrf_exempt
@require_http_methods(["POST"])
def webhook(request):
    data = preprocess_dict(request.POST.dict())
    if 'leads' in data:
        process_leads(data['leads'])
    elif 'contacts' in data:
        process_contacts(data['contacts'])
    return HttpResponse(status=200)


def parse_key(key):
    action_index = key.index('[')
    action = key[:action_index]

    other_keys_str = key[action_index:]
    pattern = '\[(\w+)\]'
    other_keys = []
    for match in re.findall(pattern, other_keys_str):
        try:
            match = int(match)
        except ValueError:
            pass
        other_keys.append(match)
    return [action] + other_keys


def put_if_empty(data, keys, value):
    inner_dict = data
    for key in keys[:-1]:
        if key not in inner_dict:
            inner_dict[key] = {}
        inner_dict = inner_dict.get(key)
    inner_dict[keys[-1]] = value


def check_is_num_sequence(sequence, start_index):
    num_sequence = []
    for key in sequence:
        if isinstance(key, int):
            num_sequence.append(int(key))
        else:
            return False

    index = start_index
    num_sequence.sort()
    for val in num_sequence:
        if val != index:
            return False
        index += 1

    return True


def num_dicts_to_arr(data):
    if not isinstance(data, dict):
        return data

    keys = data.keys()
    if check_is_num_sequence(keys, 0):
        data = list(data.values())

    for key in keys:
        data[key] = num_dicts_to_arr(data[key])

    return data


# Because AmoCRM return data in very strange format, we must preprocess it
def preprocess_dict(data):
    result_dict = {}
    for key, value in data.items():
        keys = parse_key(key)
        put_if_empty(result_dict, keys, value)

    result_dict = num_dicts_to_arr(result_dict)
    return result_dict


def get_order(id):
    try:
        return Order.objects.get(amocrm_id=id)
    except Order.DoesNotExist:
        return None


def process_leads(data):
    if 'status' in data:
        for lead_data in data['status']:
            if int(lead_data['pipeline_id']) != OrderLeadsApi.PIPELINE_ID:
                return

            order = get_order(lead_data['id'])
            if not order:
                return

            status_id = int(lead_data['status_id'])
            changed = True
            try:
                status = OrderLeadsApi.STATUS_PIPELINE.index(status_id)
                if order.status != status:
                    order.status = status
                else:
                    changed = False
            except ValueError:
                logger.error('Invalid status id %s. Please sync statuses from amocrm with site.' % status_id)
                changed = False

            if changed:
                if order.status == Order.SEND_RECEIPT_STATUS:
                    send_client_order_receipt(order)
                order.save()
    if 'delete' in data:
        for lead_data in data['delete']:
            order = get_order(lead_data['id'])
            if not order:
                return

            order.amocrm_id = -1
            order.save()


def get_user(id):
    try:
        return User.objects.get(amocrm_id=id)
    except User.DoesNotExist:
        return None


def process_email_field(user, emails):
    if not emails:
        return

    user_emails = [user.email] + list(user.additional_emails.values_list('email', flat=True))
    new_emails = [email for email in emails if email['value'] not in user_emails]
    if not new_emails:
        return

    if not user.email:
        user.email = new_emails.pop(0)['value']
        user.save()

    for email in new_emails:
        Email.objects.create(
            user=user,
            email=email['value']
        )


def process_phone_field(user, phones):
    if not phones:
        return

    user_phones = [user.phone_number] + list(user.additional_phone_numbers.values_list('phone_number', flat=True))
    phones = [str(PhoneNumber.from_string(phone['value'])) for phone in phones]

    new_phones = [phone for phone in phones if phone not in user_phones]
    if not new_phones:
        return

    if not user.phone_number:
        user.phone_number = PhoneNumber.from_string(new_phones.pop(0))
        user.save()

    for phone_number in new_phones:
        Phone.objects.create(
            user=user,
            phone_number=PhoneNumber.from_string(phone_number)
        )


def process_contacts(data):
    if 'update' in data:
        for contact_data in data['update']:
            user = get_user(contact_data['id'])
            if not user:
                return

            custom_fields = contact_data['custom_fields']
            for field in custom_fields:
                field_id = int(field['id'])
                if field_id == ContactsApi.PHONE_NUMBER_FIELD_ID:
                    process_phone_field(user, field['values'])
                elif field_id == ContactsApi.EMAIL_FIELD_ID:
                    process_email_field(user, field['values'])
                elif field_id == ContactsApi.BIRTHDAY_FIELD_ID:
                    if field['values']:
                        user.birthday = ContactsApi.parse_date(field['values'][0]['value'])
                        user.save()

    if 'delete' in data:
        for contact_data in data['delete']:
            user = get_user(contact_data['id'])
            if not user:
                return

            user.amocrm_id = -1
            user.save()
