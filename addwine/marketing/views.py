from django.conf import settings
from django.forms import model_to_dict
from django.shortcuts import render

from amocrm_api.utils import amocrm_sync_sommelier_cta
from common.views import CatchFormView
from customer.forms import ContactsForm
from customer.models import User, Contacts
from feedback.views import send_mail
from marketing.forms import MailCTAForm, SommelierCTAForm
from marketing.models import SommelierCard
from sommeliers.models import SommelierTask
from sommelier_bot.bot_methods import notify_about_task


class MailCTAFormView(CatchFormView):
    form_class = MailCTAForm
    form_name = 'mail_cta_request'

    def form_valid(self, form):

        cta = form.cleaned_data['cta']
        cta.counter += 1
        cta.save()

        email = form.cleaned_data['email']
        contacts = Contacts(email=email)
        contacts.save()

        send_mail(cta.email, contacts.as_dict(), [email,])
        return super().form_valid(form)


class SommelierCTAFormView(CatchFormView):
    form_class = SommelierCTAForm
    form_name = 'sommelier_cta_request'

    def form_valid(self, form):
        cta = form.cleaned_data['cta']
        cta.counter += 1
        cta.save()

        contact = form.cleaned_data['contact']
        contacts = Contacts()
        setattr(contacts, form.contact_type, contact)
        contacts.save()

        if form.contact_type == 'phone_number':
            sommelier_task = SommelierTask.objects.create(
                cta=cta,
                contacts=contacts,
                sommelier=form.cleaned_data['cta'].sommelier,
                product=form.cleaned_data.get("product")
            )
            amocrm_sync_sommelier_cta(sommelier_task.id)
            # notify_about_task(sommelier_task) # Disabled because Telegram is not available
        else:
            send_mail(cta.email, contacts.as_dict(), [contact,])

        return super().form_valid(form)


class SommelierCardsMixin(object):
    def get_context_data(self, **kwargs):
        context = super(SommelierCardsMixin, self).get_context_data(**kwargs)
        count = settings.SOMMELIER_DISPLAY_COUNT
        context['sommeliers'] = SommelierCard.objects.filter(enabled=True)[:count]
        return context
