import html

from ckeditor.fields import RichTextField
from django.db import models
from django.forms import model_to_dict
from django.template.defaultfilters import striptags

from customer.models import CommunicationEventType, User


class SliderCTA(models.Model):
    title = models.CharField(verbose_name='заголовок слайда', max_length=1024)
    text = models.CharField(verbose_name='текст слайда (email)', max_length=1024, null=True, default='')
    btn_text = models.CharField(verbose_name='текст кнопки', max_length=255)
    img = models.ImageField(verbose_name='изображение')
    href = models.URLField(verbose_name='ссылка', null=True, blank=True)
    order_field = models.PositiveIntegerField(verbose_name="Нажать и тащить", default=0)
    enabled = models.BooleanField(verbose_name='Отображать на сайта', default=True)

    def __str__(self):
        return self.title

    def as_dict(self):
        return model_to_dict(self, fields=('title', 'btn_text'))

    class Meta:
        ordering = ('order_field',)
        verbose_name = 'слайд'
        verbose_name_plural = 'слайды'


class SEOTextCategory(models.Model):
    title = models.CharField('название', max_length=1024)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'категория SEO текста'
        verbose_name_plural = 'категории SEO текстов'


class SEOText(models.Model):
    inner_category = models.ForeignKey(SEOTextCategory, verbose_name='внутренняя категория', null=True, blank=True)
    title = models.CharField('заголовок', max_length=1024)
    text = RichTextField('текст')
    admin_comment = models.TextField(verbose_name='Комментарий', default='', blank=True)
    order_field = models.PositiveIntegerField(verbose_name='нажать и тащить', default=0)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order_field', )
        verbose_name = 'SEO текст'
        verbose_name_plural = 'SEO тексты'


class SommelierCTA(models.Model):
    title = models.CharField(verbose_name="заголовок", max_length=1024)
    text = models.TextField(verbose_name="текст", max_length=2048)
    email = models.ForeignKey(CommunicationEventType, verbose_name="email шаблон")
    sommelier = models.ForeignKey(User, verbose_name='сомелье',
                                  limit_choices_to={'groups__name__iexact': 'Сомелье'}, null=True, blank=True)
    counter = models.PositiveIntegerField(verbose_name='Количество использований', default=0)
    created = models.DateTimeField(verbose_name="дата создания", auto_now_add=True, blank=True)

    def __str__(self):
        return self.title

    def as_dict(self):
        data = model_to_dict(self)
        data['text'] = html.unescape(striptags(data['text']))
        data['sommelier'] = self.sommelier.as_dict() if self.sommelier else None
        return data

    class Meta:
        verbose_name = "сомелье: призыв к действию"
        verbose_name_plural = "сомелье: призывы к действию"


class SommelierCard(models.Model):
    photo = models.ImageField(verbose_name='Фото')
    name = models.CharField(verbose_name="Имя", max_length=255)
    post = models.TextField(verbose_name="должность", max_length=1024)
    description = models.TextField(verbose_name="Описание", max_length=2048)
    enabled = models.BooleanField(verbose_name='Отображать на сайте', default=True, blank=True)
    order_field = models.PositiveIntegerField(verbose_name='нажать и тащить', default=0)
    networks_facebook = models.URLField(verbose_name="ссылка на Facebook", default='', blank=True)
    networks_instagram = models.URLField(verbose_name="ссылка на Instagram", default='', blank=True)
    networks_vk = models.URLField(verbose_name="ссылка на Vk", default='', blank=True)
    networks_youtube = models.URLField(verbose_name="ссылка на Youtube", default='', blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('order_field', )
        verbose_name = "сомелье: карточки"
        verbose_name_plural = "сомелье: карточки"


class MailCTA(models.Model):
    title = models.CharField(verbose_name="заголовок", max_length=1024)
    email = models.ForeignKey(CommunicationEventType, verbose_name="email шаблон")
    counter = models.PositiveIntegerField(verbose_name='Количество использований', default=0)
    created = models.DateTimeField(verbose_name="дата создания", auto_now_add=True, blank=True)

    def as_dict(self):
        return model_to_dict(self)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "почта: призыв к действию"
        verbose_name_plural = "почта: призывы к действию"
