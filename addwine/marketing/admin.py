from adminsortable2.admin import SortableAdminMixin
from ckeditor.widgets import CKEditorWidget
from dal import autocomplete
from django import forms
from django.contrib import admin
from django.db import models

from marketing.models import SommelierCTA, MailCTA, SliderCTA, SEOText, SEOTextCategory, SommelierCard


@admin.register(SEOTextCategory)
class SEOTextCategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)


class SEOTextForm(forms.ModelForm):
    class Meta:
        model = SEOText
        fields = '__all__'
        widgets = {
            'inner_category': autocomplete.ModelSelect2(url='autocomplete_seo_text_category'),
        }


@admin.register(SEOText)
class SEOTextAdmin(SortableAdminMixin, admin.ModelAdmin):
    form = SEOTextForm
    list_display = ('title', 'admin_comment', 'inner_category')
    list_filter = ('inner_category',)


@admin.register(SliderCTA)
class SliderCTAAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('title', 'enabled')
    list_filter = ('enabled',)
    list_editable = ('enabled',)


@admin.register(SommelierCard)
class SommelierCardAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('name', 'post', 'enabled')
    list_editable = ('enabled', )


class SommelierCTAForm(forms.ModelForm):
    class Meta:
        model = SommelierCTA
        exclude = ('counter', 'created')
        widgets = {
            'sommelier': autocomplete.ModelSelect2(url='autocomplete_sommelier'),
            'email': autocomplete.ModelSelect2(url='customer_extension:autocomplete_email_templates'),
        }


@admin.register(SommelierCTA)
class SommelierCTAAdmin(admin.ModelAdmin):
    form = SommelierCTAForm
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()},
    }

    list_display = ('title', 'counter', 'created')


class MailCTAForm(forms.ModelForm):
    class Meta:
        model = MailCTA
        fields = '__all__'
        widgets = {
            'email': autocomplete.ModelSelect2(url='customer_extension:autocomplete_email_templates'),
        }


@admin.register(MailCTA)
class MailCTAAdmin(admin.ModelAdmin):
    form = MailCTAForm
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()},
    }

    fields = ('title', 'email')
    list_display = ('title', 'counter', 'created')
