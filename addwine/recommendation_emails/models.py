from django.db import models
from django.urls import reverse
from django.utils import timezone

from order.models import Order
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from marketing.models import SliderCTA
from news.models import Article
from ckeditor.fields import RichTextField
from datetime import timedelta
import uuid


class RecommendationEmailsSettings(models.Model):
    main_image = models.ImageField(upload_to='emails_images/', null=True, verbose_name='Основное изображение')
    subject = models.CharField(max_length=255, verbose_name='Тема письма')
    greeting = models.CharField(max_length=255, verbose_name='Шаблон приветсвия')
    text = RichTextField(
        'Текст письма', blank=True, null=True,
        help_text="HTML template")
    slide = models.ForeignKey(SliderCTA, verbose_name='Слайд', null=True, blank=True)
    article_one = models.ForeignKey(Article, verbose_name='Первая статья', related_name='article1', null=True, blank=True)
    article_two = models.ForeignKey(Article, verbose_name='Вторая статья', related_name='article2', null=True, blank=True)
    date_delta = models.PositiveSmallIntegerField('Дней до отправки письма', default=6)

    @staticmethod
    def get_instance():
        if RecommendationEmailsSettings.objects.count() == 0:
            RecommendationEmailsSettings.objects.create(
                subject='Тема пиcьма',
                greeting='Здравствуйте, {{firstname}} {{lastname}}!',
                text='Введите текст',
                slide=SliderCTA.objects.first(),
                article_one=Article.objects.first(),
                article_two=Article.objects.first()
            )
        return RecommendationEmailsSettings.objects.first()

    def __str__(self):
        return 'Настройки рекомендательных писем'

    class Meta:
        verbose_name = 'настройки рекомендательных писем'
        verbose_name_plural = 'настройки рекомендательных писем'


class RecommendationEmails(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    order = models.ForeignKey(Order, verbose_name='Заказ')
    send_date = models.DateField(verbose_name='Дата планируемой отправки письма')
    will_send = models.BooleanField('Будет отправлено', default=True)
    sent = models.BooleanField('Отправлено?', default=False)
    sent_datetime = models.DateTimeField(verbose_name='Дата и время фактической отправки',
                                         blank=True, null=True)
    rendered_html = models.TextField('HTML', blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.sent_datetime:
            self.sent = True

        super(RecommendationEmails, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('show-recommendation-email', kwargs={'email_uuid': self.uuid})

    @property
    def email_address(self):
        return self.order.shipping_contacts.email

    def __str__(self):
        return '{} for order_id: {}'.format(
            self.id,
            self.order_id
        )

    @classmethod
    def create_for_order(cls, order):
        if settings.SEND_RECOMMENDATION_EMAILS and order.status == Order.STATUSES_CLOSED:
            if not RecommendationEmails.objects.filter(order=order).exists():
                send_date = timezone.now() + timedelta(days=RecommendationEmailsSettings.get_instance().date_delta)
                RecommendationEmails.objects.create(
                    order=order,
                    send_date=send_date,
                )

    class Meta:
        verbose_name = 'Рекомендательное письмо'
        verbose_name_plural = 'Рекомендательные письма'


@receiver(post_save, sender=Order)
def create_recommendation_email(sender, instance, **kwargs):
    RecommendationEmails.create_for_order(order=instance)
