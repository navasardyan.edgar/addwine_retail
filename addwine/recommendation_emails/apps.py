from django.apps import AppConfig


class RecommendationEmailsConfig(AppConfig):
    name = 'recommendation_emails'
