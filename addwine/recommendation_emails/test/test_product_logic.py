import factory

from django.test import TestCase
from order.mommy_recipes import create_order, create_line
from ..utils import recommendation_logic
from model_mommy import mommy
from ..models import RecommendationEmails, RecommendationEmailsSettings
from ..tasks import send_recommendation_emails, send_recommendation_email
from django.utils import timezone
from datetime import timedelta
from django.core import mail
from django.db.models.signals import post_save, pre_save
from common.models import SiteConfiguration


class TestRecommendationEmailsTask(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestRecommendationEmailsTask, cls).setUpClass()
        cls.order = create_order(lines_count=2)

    def test_send_today_planned_mail(self):
        RecommendationEmails.objects.create(
            order=self.order,
            send_date=timezone.now().date()
        )
        send_recommendation_emails()
        self.assertEqual(len(mail.outbox), 1)
        self.assertTrue(len(mail.outbox[0].body) > 40000)

    def test_send_previous_day_planned_mail(self):
        RecommendationEmails.objects.create(
            order=self.order,
            send_date=timezone.now().date()-timedelta(days=1)
        )
        send_recommendation_emails()
        self.assertEqual(len(mail.outbox), 1)
        self.assertTrue(len(mail.outbox[0].body) > 40000)

    def test_send_today_and_previous_days_email(self):
        email_count = 3
        RecommendationEmails.objects.bulk_create([
            RecommendationEmails(
                order=self.order,
                send_date=timezone.now().date()-timedelta(days=i)
            )
            for i in range(email_count)
        ])
        send_recommendation_emails()
        self.assertEqual(len(mail.outbox), 3)


class TestRecommendationEmails(TestCase):
    @factory.django.mute_signals(pre_save, post_save)
    def test_products_logic(self):
        # make tests for different Lines count in Order
        for lines_count in range(1, 11):
            # Create OrderLine
            order = create_order()
            lines_in_order = []
            for k in range(lines_count):
                line = create_line(order)
                lines_in_order.append(line)

            with self.subTest('lines_count {}'.format(lines_count+1)):
                result_products = recommendation_logic(lines_in_order)
                self.assertEqual(len(result_products), 9)

    def test_exclude_duplicates(self):
        # create order with two lines
        order = create_order()
        line1 = create_line(order)
        line2 = create_line(order)
        # create same recommendation product with max price for both lines
        duplicated_product = mommy.make_recipe('catalogue.rcp_product')
        mommy.make_recipe('partner.rcp_stock_record', product=duplicated_product, price_excl_tax=30000)
        mommy.make_recipe('catalogue.rcp_product_recommendation',
                          primary=line1.product, recommendation=duplicated_product, ranking=0)
        mommy.make_recipe('catalogue.rcp_product_recommendation',
                          primary=line2.product, recommendation=duplicated_product, ranking=1)
        result_products = recommendation_logic([line1, line2])
        self.assertEqual(len(result_products), 9)
        self.assertEqual(result_products.count(duplicated_product), 1)


    def test_emails(self):
        # setup
        order = create_order(lines_count=2)
        # trigger email creations
        order.status = order.__class__.STATUSES_CLOSED
        order.save()

        # with self.subTest('Email success creation'):
        #     email = RecommendationEmails.objects.get(order=order)
        #     self.assertEqual(email.send_date,
        #                      timezone.now().date()+timedelta(days=RecommendationEmailsSettings.get_instance().date_delta))

        with self.subTest('Email send'):
            email = RecommendationEmails.objects.get(order=order)
            send_recommendation_email(email.pk)
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].recipients(), [order.shipping_contacts.email, ])
            self.assertTrue(len(mail.outbox[0].body) > 40000)
