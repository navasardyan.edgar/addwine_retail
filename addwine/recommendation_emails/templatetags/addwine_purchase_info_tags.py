from django import template
from partner.strategy import Selector
from oscar.core.compat import assignment_tag

register = template.Library()


@assignment_tag(register)
def purchase_info_for_product(request, product):
    strategy = request.strategy if request else Selector().strategy()

    if product.is_parent:
        return strategy.fetch_for_parent(product)

    return strategy.fetch_for_product(product)
