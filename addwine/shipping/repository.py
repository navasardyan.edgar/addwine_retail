from oscar.apps.shipping import repository

from shipping.models import OrderAndItemCharges
from . import methods as shipping_methods


class Repository(repository.Repository):
    def get_available_shipping_methods(
            self, basket, user=None, shipping_addr=None,
            request=None, **kwargs):
        methods = []
        db_methods = OrderAndItemCharges.objects.all()
        methods.extend(db_methods)
        return methods
