from django.apps import AppConfig


class ShippingConfig(AppConfig):
    name = 'shipping'
    verbose_name = 'Доставка'
