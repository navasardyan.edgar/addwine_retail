from cacheops.templatetags.cacheops import register


@register.filter
def subtract(value, arg):
    return int(value) - int(arg)


@register.filter
def adjust_line_for_taxes_and_discounts(obj):
    if obj.has_range_discount or obj.has_discount:
        if obj.is_tax_known:
            obj.line_price_incl_tax_incl_discounts
        else:
            obj.line_price_excl_tax_incl_discounts
    else:
        if obj.is_tax_known:
            obj.line_price_incl_tax
        else:
            obj.line_price_excl_tax


@register.filter
def adjust_line_for_taxes(obj):
    if obj.is_tax_known:
        return obj.line_price_incl_tax
    else:
        return obj.line_price_excl_tax


@register.filter
def adjust_for_taxes_and_discounts(obj):
    if obj.has_range_discount:
        if obj.is_tax_known:
            obj.incl_range_discount_incl_tax
        else:
            obj.incl_range_discount_excl_tax
    else:
        if obj.is_tax_known:
            return obj.incl_tax
        else:
            return obj.excl_tax

@register.filter
def adjust_for_taxes(obj):
    if obj.is_tax_known:
        return obj.incl_tax
    else:
        return obj.excl_tax
