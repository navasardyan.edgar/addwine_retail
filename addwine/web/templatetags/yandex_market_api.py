import bleach
import cacheops
from cacheops.templatetags.cacheops import register
from django.conf import settings


@register.filter
def yandex_api_process_text(text):
    text = bleach.clean(text, tags=['p', 'ul', 'li', 'p', 'br'], strip=True)
    return text