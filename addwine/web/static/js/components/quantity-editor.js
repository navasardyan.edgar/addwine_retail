Vue.component('quantity-editor', {
    delimiters: ['[[', ']]'],
    props: ['productId', 'quantity'],
    data() {
        return {
            localQuantity: this.quantity
        }
    },
    methods: {
        onChangeQuantity(delta) {
            if (delta === -1 && this.localQuantity === 1) {
                return
            }
            this.localQuantity += delta;
            this.$emit('change', this.localQuantity)
        }
    },
    template: `<div class="item__count">
        <div class="minus" @click="onChangeQuantity(-1)">
            <img src="/static/img/icons/16px/Minus.svg" alt="" class="img-svg">
        </div>
        <input type="text" name="quantity" v-model="localQuantity">
        <div class="plus" @click="onChangeQuantity(1)">
            <img src="/static/img/icons/16px/Plus.svg" alt="" class="img-svg">
        </div>
    </div>`
})