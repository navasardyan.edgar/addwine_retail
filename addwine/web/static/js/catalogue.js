$(function () {
    var $filterForms=$('.filter-form');
    var $filterControls=$filterForms.find('input.filter-control');

    var $resultsCount=$('.founded-goods span');
    var $productsListUpdatablePart=$('.updatable-container');
    var $loadMoreLoading=$('#productsLoadMoreLoading');

    var initialState=JSON.parse($('#initialFiltersState').val());
    var currentState=initialState;
    currentState.page=1;
    window.history.replaceState(initialState, '', window.location);

    var $displayTypeButtons=$('.sort-imgs a');
    var $onlyAvailableButton=$('.available-button');

    var $rangeSliders=$filterControls.filter(".range-slider");

    $rangeSliders.ionRangeSlider({
        onChange: function (obj) {
            var $this = obj.input,
                value = $this.prop("value").split(";");
            var $form = $this.closest("form");
            $form = $('form[name="'+$form.attr('name')+'"]');
            $form.find('.range-min').val(value[0]);
            $form.find('.range-max').val(value[1]);
        },
        onFinish: function () {
            updateCatalogue();
        }
    });
    $('.range-min,.range-max').on("input", function () {
        var $this=$(this);
        var $form = $this.closest("form");
        var slider=$form.find(".range-slider").data('ionRangeSlider');
        slider.update({
            from: parseInt($form.find('.range-min').val()),
            to: parseInt($form.find('.range-max').val())
        })
    });
    // $(".select_join").selectBoxIt({
    //     downArrowIcon: "fa fa-angle-down",
    //     hideCurrent: true
    // });

    $filterForms.click(function (ev) {
        if($(this).hasClass("disabled")) {
            ev.preventDefault();
            ev.stopImmediatePropagation();
        }
    });


    // Collect data from all filters and creating filter string
    var makeFilterQueryString=function () {
        let query='';
        $filterForms.filter('.options.desktop').each(function () {
            let $this=$(this);
            let $options=$this.find("input.filter-control:checked");
            let name=$this.attr("name");
            let filterPattern='selected_facets='+name + "_exact:";
            if($options.length>0) {
                $options.each(function () {
                    query += filterPattern+$(this).val()+"&";
                });
            }
        });
        $filterForms.filter('.range.desktop').each(function () {
            let $this=$(this);
            let name=$this.attr("name");
            let filterPattern='ranges='+name;
            query+=filterPattern+"_min:"+$this.find(".range-min").val()+"&";
            query+=filterPattern+"_max:"+$this.find(".range-max").val()+"&";
        });

        if(query.length>0)
            query=query.substr(0, query.length-1);
        return encodeURI(query);
    };

    var getPageParamsString=function (reset) {
        let url='?';
        if(!reset) {
            url+=makeFilterQueryString();
        }
        url+='&sort_by='+encodeURI($('.sort-block select').val());
        url+="&display="+$displayTypeButtons.filter(".active").attr('data-type');
        if($onlyAvailableButton.prop("checked")) {
            url += "&filters=available:True";
        }
        console.log('url', url);
        return url;
    };


    var updateFacets=function (facetData, resetChecked) {
        //Reset all checkboxes
        var $checkboxes=$filterForms.find('input[type="checkbox"]');
        $checkboxes.not(':checked').prop('disabled', true);
        if(resetChecked)
            $checkboxes.prop('checked', false);

        Object.keys(facetData).forEach(function(key) {
            var data=facetData[key];

            if(!data.results)
                return;

            var $form=$filterForms.filter('*[name="'+key+'"]');
            $form.removeClass("disabled");
            var $variantsCheckboxes=$form.find('input[type="checkbox"]');
            if(data.results.length === 0)
                $form.addClass("disabled");
            data.results.forEach(function (variantData) {
                var $variant=$variantsCheckboxes.filter('*[value="'+variantData.name+'"]');
                $variant.prop("disabled", variantData.disabled);
                $variant.prop("checked", variantData.selected);
            })
        });
    };

    var updateRanges=function (rangesData) {
         Object.keys(rangesData).forEach(function (rangeName){
            var $form=$('form[name="'+rangeName+'"]');
            var rangeData=rangesData[rangeName];
            var $rangeMin=$form.find(".range-min");
            var $rangeMax=$form.find(".range-max");
            $rangeMin.val(rangeData.query_min?rangeData.query_min:rangeData.min);
            $rangeMax.val(rangeData.query_max?rangeData.query_max:rangeData.max);
            $rangeMax.trigger('input');
        });
    };

    let toggleLoading=function () {
        let $productsLoadingOverlay = $(".catalog-box.loading > .loading__overlay")
        $productsLoadingOverlay.toggle();
    };

    let saveInHistory=function (path, data) {
        currentState=data;
        window.history.pushState(data, '', path);
    };
    window.addEventListener('popstate', function(e) {
        currentState=e.state;
        if(currentState)
            updateCatalogueData(currentState, true);
    });

    let updateCatalogueData=function (data, resetCheckboxes) {
        updateFacets(data.facet_data, resetCheckboxes);
        updateRanges(data.range_data);

        $resultsCount.html(data.count);

        $productsListUpdatablePart.html(data.products_html);

        let searchQuery=window.location.search;
        if(searchQuery.length>0)
            searchQuery=searchQuery.substr(1, searchQuery.length);
        let params=parseQueryString(searchQuery);
        let $orderBySelect=$('.sort-block select');

        if(params.sort_by)
            $orderBySelect.val(params.sort_by);
        else
            $orderBySelect[0].selectedIndex=0;
        // $orderBySelect.selectBoxIt().data("selectBoxIt").refresh();

        let displayType=params.display?params.display:'list';
        let $displayTypesIcons=$(".sort-imgs a");
        $displayTypesIcons.removeClass("active");
        $displayTypesIcons.filter('*[data-type="'+displayType+'"]').addClass("active");

        if(params.filters && params.filters.indexOf('available:True') !== -1)
            $onlyAvailableButton.addClass("active");
        else
            $onlyAvailableButton.removeClass("active");

        if(data.num_pages > 1) {
            currentState.page=1;
        }

    };

    var updateCatalogue=function (reset=false, params) {

        if(typeof params === 'undefined') {
            params = getPageParamsString(reset);
        }
        toggleLoading();
        let path=location.pathname+params;
        sendRequest(path, function (success, data) {

            if(success) {
                toggleLoading();
                saveInHistory(path, data);
                updateCatalogueData(data);
            }
        });
    };

    // Sync checkboxes on mobile and desktop filters

    $filterControls.filter('*[type="checkbox"]').on('change', function () {
        var $this=$(this);
        var id=$this.attr('id');
        var targetId;

        if(id.endsWith('Mobile')) {
            targetId = id.replace('Mobile', '');
        }else
            targetId=id+"Mobile";


        if($this.attr("type")==='checkbox') {

            $('#'+targetId).prop("checked", $this.prop("checked"));
        }
        updateCatalogue();
    });

    $onlyAvailableButton.click(function () {
       // $('#'+targetId).prop("checked", $this.prop("checked"));
       updateCatalogue();
    });

    // On click change location to same url but without filters
    $('.reset-filters-btn').click(function () {
         updateCatalogue();
    });

    // On click displayType update page with new display type and current filters
    $displayTypeButtons.click(function () {
        $displayTypeButtons.removeClass('active');
        $(this).addClass('active');
        updateCatalogue();
    });

    // On change sort type update page
    $('.sort-block select').on("change", function () {
        updateCatalogue();
    });


    $(".headers-section .block").click(function () {
        var $blockWrap=$(this).closest(".block-wrap");
        var active=false;
        if($blockWrap.hasClass("active"))
            active=true;
        $(".block-wrap").removeClass("active");
        if(!active)
            $blockWrap.addClass("active");
    });

    $(".headers-section .drop-block__menu").click(function () {
        $(".block-wrap").removeClass("active");
    });

});