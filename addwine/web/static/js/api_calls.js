async function addProductToBasket(productId, quantity) {
    const url = `/basket/add/${productId}/?render_quick_basket=True`
    const params = new URLSearchParams();
    params.append('quantity', quantity)
    return await axios.post(url, params, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-CSRFToken': getCookie('csrftoken'),
        }
    });
}