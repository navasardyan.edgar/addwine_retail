$(function () {
    var requestAlertModal=$("#requestAlert");
   $(document).on("click", "a.product-alert", function () {
       var $this=$(this);
       requestAlertModal.find("input[name='product']").val($this.attr("data-product-id"));
       requestAlertModal.find(".img-block img").attr("src", $this.attr("data-product-img"));
       requestAlertModal.find(".name").html($this.attr("data-product-title"));
       requestAlertModal.find(".price .value").html($this.attr("data-product-price"));
   });
});