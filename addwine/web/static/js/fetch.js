function getCookie(cname) {
    let name = cname + '=';
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return '';
}

function setCookie(name, value, days = null) {
    if (value === '' || value === null) {
        document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    } else {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }
}

const GENERAL_ERROR_MESSAGE = 'Упс...Что-то пошло не так. Мы уже работаем, чтобы исправить это';
function handle400(error) {
    const response = error.response;
    let options = {
        message: error.response.data.title
    }
    if (error.response.data.hasOwnProperty('detail')) {
        options.detail = error.response.data.detail;
    }
    reportInfo(options)
}

function handle403(error) {
    let options = {}
    if (error.response.data && error.response.data.hasOwnProperty('title')) {
        options['message'] = error.response.data.title;
    } else {
        options['message'] = 'Недостаточно прав';
    }
    if (error.response.data.hasOwnProperty('detail')) {
        options.detail = error.response.data.detail;
    }
    reportInfo(options)
}

function handle404(error) {
    const response = error.response;
    let options = {
        message: GENERAL_ERROR_MESSAGE
    }
    options.detail = `Вызван несуществующий ресурс ${error.response.config.url}`;
    reportInfo(options)
}

function handle500(error) {
    reportError(GENERAL_ERROR_MESSAGE);
}

axios.interceptors.response.use(
    (response) => {
        return response.data;
    },
    (error) => {
        if (error.response) {
            if (error.response.status === 400) {
                handle400(error)
            } else if (error.response.status === 403) {
                handle403(error)
            } else if (error.response.status === 404) {
                handle404(error)
            }
            else if (error.response.status === 500) {
                handle500(error)
            }
        } else if (error.request) {
            console.log('asd')
        } else {
            // No network, api is not accessible
        }
        return Promise.reject(error);
    }
);

const authorizedGetConfig = (options = {}) => {
    return {
        ...options,
        // baseURL: BASE_URL,
        transformRequest: [
            (data) => {
                let ret = '';
                for (let it in data) {
                    ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
                }
                return ret;
            }
        ],
        transformResponse: [
            (data) => {
                return data
            }
        ],
        headers: {
            'X-CSRFToken': getCookie('csrftoken'),
            'Content-Type': 'application/json'
        },
        responseType: options.responseType || 'json'
    }
}
const authorizedPutConfig = (options = {}) => {
    return {
        ...options,
        // baseURL: environment.baseUrl,
        transformRequest: [
            (data) => {
                return JSON.stringify(data);
            }
        ],
        transformResponse: [
            (data) => {
                return data
            }
        ],
        headers: {
            'X-CSRFToken': getCookie('csrftoken'),
            'Content-Type': options.contentType || 'application/json'
        },
        responseType: options.responseType || 'json'
    }
};
const authorizedPostConfig = (options = {}) => {
    return {
        // baseURL: environment.baseUrl,
        headers: {
            'X-CSRFToken': getCookie('csrftoken'),
            'Content-Type': options.contentType || 'application/json'
        },
        responseType: options.responseType || 'json'
    }
};

let unauthorizedPostConfig = {
    transformRequest: [
        (data) => {
            return JSON.stringify(data);
        }
    ],
    transformResponse: [
        (data) => {
            return data
        }
    ],
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-CSRFToken': getCookie('csrftoken'),
    },
    responseType: 'json'
};

let unauthorizedGetConfig = {
    // baseURL: environment.baseUrl,
    method: 'get',
    withCredentials: false,
    headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': getCookie('csrftoken'),
    },
    responseType: 'json'
};

const get = (url, options = {}) => {
    const configs = authorizedGetConfig(options);
    return axios.get(url, configs)
};

const post = (url, data, options = {}) => {
    const configs = authorizedPostConfig(options)
    return axios.post(url, data, configs)
};

const put = (url, data, options = {}) => {
    const configs = authorizedPutConfig(options)
    return axios.put(url, data, configs)
};

const remove = (url) => {
    const configs = authorizedPostConfig()
    return axios.delete(url, configs)
};

const unauthorizedGet = (url, options = {}) => {
    let configs = { ...unauthorizedGetConfig }
    configs = {
        ...configs,
        headers: { ...configs.headers, ...options.headers }
    }
    return axios.get(url, configs)
};

const unauthorizedPost = (url, data) => {
    return axios.post(url, data, unauthorizedPostConfig)
};