$(function() {
        new Vue({
            delimiters: ['[[', ']]'],
            el: '#vue-wrapper-for-catalogue',
            data() {
                return {
                    isListViewActive: true
                }
            },
            mounted() {

            },
            methods: {
                onToggleViewType() {
                    this.isListViewActive = !this.isListViewActive;
                }
            }
        })
    })