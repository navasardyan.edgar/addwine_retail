$(function () {
    let $modal=$("#abstract-purchase-modal");

    $(document).on("click", "button.open-abstract-purchase-modal", function () {
        let $this=$(this);
        $modal.find("h2.modal-title").html($this.attr("data-modal-title"));
        $modal.find("h3.modal-subtitle").html($this.attr("data-modal-subtitle"));
        $modal.find("input[name='product']").val($this.attr("data-product-id"));
        $modal.find("input[name='product']").val($this.attr("data-product-id"));
        $modal.find("input[name='pack']").val($this.attr("data-pack"));
        $modal.find("#fastbuyPhone").attr('placeholder', $this.attr("data-placeholder"));
        const quantity = $this.attr("data-quantity");
        if (quantity) {
            $modal.find("input[name='quantity']").val(quantity);
            $modal.find("input[name='quantity']").closest('.item__count').show();
        } else {
            $modal.find("input[name='quantity']").closest('.item__count').hide();
        }
        $modal.find(".modal-order__item__img").attr("src", $this.attr("data-product-img"));
        $modal.find(".modal-order__item__name a").html($this.attr("data-product-title"));
        $modal.find(".search-item__price span.price").html($this.attr("data-product-price"));

        let $form = $modal.find("form")
        $form.data('yandex-goal', $this.attr("data-yandex-goal"))
        $form.attr('action', $this.attr("data-action"));
   });
});