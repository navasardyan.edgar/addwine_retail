import requests

from math import ceil

from django.conf import settings
from django import forms

from catalogue.models import Category
from news.models import Article
from offer.models import RangeDiscount
from voucher.models import VoucherGroup


class MenuViewMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = {
            'catalogue': {},
            'footer': {}
        }
        categories = Category.objects.filter(depth=1, is_active=True)
        context['menu']['catalogue']['categories'] = categories

        context['menu']['footer']['discounts'] = list(RangeDiscount.objects.filter(display_for_users=True,
                                                                                   hidden_in_admin=False).order_by('-date_created')[
                                                      :settings.BOTTOM_DISCOUNTS_COUNT]) + \
                                                 list(VoucherGroup.objects.filter(display_for_users=True).order_by('-pk')[
                                                      :settings.BOTTOM_DISCOUNTS_COUNT])
        context['menu']['footer']['discounts'] = context['menu']['footer']['discounts'][
                                                 :settings.BOTTOM_DISCOUNTS_COUNT]
        context['menu']['footer']['articles'] = Article.objects.all().order_by('-created')[:settings.BOTTOM_ARTICLES_COUNT]

        return context


class RecaptchaFormMixin:
    def clean(self):
        cleaned_data = super().clean()
        recaptcha_response = self.data.get('g-recaptcha-response')
        data = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        result = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data).json()

        if not result['success']:
            raise forms.ValidationError('Captcha code is invalid', code='invalid')
        return cleaned_data
