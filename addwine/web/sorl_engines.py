from PIL import Image, ImageColor
from sorl.thumbnail.engines.pil_engine import Engine


class ConverterEngine(Engine):
    def create(self, image, geometry, options):
        thumb = super(Engine, self).create(image, geometry, options)
        if thumb.mode == 'RGBA' and options.get('format', 'JPEG') == 'JPEG':
            background = options.get('background', '#ffffff')
            try:
                background = Image.new('RGB', thumb.size, ImageColor.getcolor(background, 'RGB'))
                background.paste(thumb, mask=thumb.split()[3])  # 3 is the alpha of an RGBA image.
                return background
            except Exception as e:
                return thumb
        return thumb
