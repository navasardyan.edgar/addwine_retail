from django.core.exceptions import ValidationError
from oscar.apps.basket.forms import *
from oscar.apps.basket.forms import BasketVoucherForm as BaseBasketVoucherForm
from phonenumber_field.formfields import PhoneNumberField


class BasketVoucherForm(BaseBasketVoucherForm):
    email = forms.EmailField(required=False)
    phone = PhoneNumberField(required=False)

    def clean(self):
        if not self.cleaned_data.get('email') and not self.cleaned_data.get('phone'):
            raise ValidationError('Email and phone fields are empty. Atleast one field must be provided.')
        return super().clean()

