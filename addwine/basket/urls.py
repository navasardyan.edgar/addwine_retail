from django.conf.urls import url

from basket.views import LineDeleteView, LineUpdateView

urlpatterns = [
    url(r'lines/(?P<pk>\d+)/delete/$', LineDeleteView.as_view(), name='line_delete'),
    url(r'lines/(?P<pk>\d+)/update/$', LineUpdateView.as_view(), name='line_update'),
]
