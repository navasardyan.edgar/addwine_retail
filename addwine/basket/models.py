from decimal import Decimal as D
from django.db import models
from oscar.apps.basket.abstract_models import AbstractBasket, AbstractLine

from catalogue.models import Product, Pack
from partner.prices import PriceWithRangeDiscount
from voucher.models import GiftCard


class Basket(AbstractBasket):
    gift_card = models.OneToOneField(GiftCard, verbose_name='Подарочная карта', blank=True, null=True,
                                     on_delete=models.SET_NULL, related_name='basket')

    def is_shipping_required(self):
        return True

    @property
    def has_gift_card(self):
        return self.gift_card is not None

    @property
    def total_excl_tax(self):
        total = super().total_excl_tax
        if self.gift_card and self.gift_card.is_available():
            total -= self.gift_card.value
        return max(0, total)

    @property
    def total_incl_tax(self):
        total = super().total_incl_tax
        if self.gift_card and self.gift_card.is_available():
            total -= self.gift_card.value
        return max(0, total)

    @property
    def total_excl_tax_excl_gift_card(self):
        return super().total_excl_tax

    @property
    def total_incl_tax_excl_gift_card(self):
        return super().total_incl_tax

    @property
    def total_retail(self):
        return self._get_total('line_price_retail')

    def get_forbidden_for_delivery_products(self):
        return [line.product for line in self.all_lines() if line.product.forbidden_for_delivery]

    def contains_forbidden_for_delivery_products(self):
        return len(self.get_forbidden_for_delivery_products()) > 0

    def contains_only_forbidden_for_delivery_products(self):
        return len(self.get_forbidden_for_delivery_products()) == self.num_lines

    def add_product(self, product, quantity=1, options=None):
        line, created = super().add_product(product, quantity, options)
        stock_info = self.strategy.fetch_for_product(product)
        line.price_retail = stock_info.price.retail
        if created and options:
            option_value = [option for option in options if option['option'].code == Product.PACK_OPTION_CODE]
            if len(option_value) > 0:
                option_value = option_value[0]['value']
                try:
                    option_value = int(option_value)
                except ValueError:
                    option_value = None
                if option_value is not None:
                    try:
                        pack = product.packs.get(id=option_value)
                        line.price_excl_tax = pack.price
                        line.price_retail = pack.price_retail
                    except Pack.DoesNotExist:
                        pass
        line.save()

        return line, created

    def add_presents(self, exclude_ids=None, clear=True):
        if clear:
            self._clear_present_lines()
        line_present_qs = self.all_lines().select_related('product__present').exclude(product__present=None)
        if exclude_ids:
            line_present_qs = line_present_qs.exclude(product_id__in=exclude_ids)
        present_list = ((line.product.present, line.quantity) for line in line_present_qs)
        for (present, quantity) in present_list:
            if present:
                self.add_present_product(present, quantity)

    def add_present_product(self, product, quantity=1, options=None):
        if options is None:
            options = []
        if not self.id:
            self.save()

        # Ensure that all lines are the same currency
        stock_info = self.strategy.fetch_for_product(product)
        price_retail = stock_info.price.retail
        stock_info = stock_info._replace(price=PriceWithRangeDiscount(0, 0, price_retail, product, tax=D('0.00')))
        stock_info.stockrecord.price_retail = price_retail
        stock_info.stockrecord.price_excl_tax = 0

        line_ref = '%s_%s' % (product.id, 'present')

        # Determine price to store (if one exists).  It is only stored for
        # audit and sometimes caching.
        defaults = {
            'quantity': quantity,
            'price_excl_tax': stock_info.price.excl_tax,
            'price_currency': stock_info.price.currency,
        }

        line, created = self.lines.get_or_create(
            line_reference=line_ref,
            product=product,
            stockrecord=stock_info.stockrecord,
            defaults=defaults,
            is_present=True)
        if created:
            for option_dict in options:
                line.attributes.create(option=option_dict['option'],
                                       value=option_dict['value'])
        else:
            line.quantity = max(0, line.quantity + quantity)
            line.save()
        self.reset_offer_applications()

        # Returning the line is useful when overriding this method.
        return line, created

    def _clear_present_lines(self):
        self.all_lines().filter(is_present=True).delete()


class Line(AbstractLine):
    is_present = models.BooleanField('Подарок', default=False)
    price_retail = models.DecimalField('Себестоимость', decimal_places=2, max_digits=12, null=True)

    @property
    def has_present(self):
        return bool(self.product.present)

    @property
    def purchase_info(self):
        purchase_info = super(Line, self).purchase_info
        if self.is_present:
            purchase_info = purchase_info._replace(price=PriceWithRangeDiscount(0, 0, purchase_info.price.retail,
                                                                                self.product, tax=D('0.00')))
            return purchase_info
        try:
            pack_option = self.attributes.get(option__code=Product.PACK_OPTION_CODE)
        except LineAttribute.DoesNotExist:
            return purchase_info
        if pack_option.value:
            try:
                pack = self.product.packs.get(id=pack_option.value)
            except Pack.DoesNotExist:
                return purchase_info
            purchase_info.price.excl_tax = pack.price
            purchase_info.price.retail = pack.price_retail
        return purchase_info

    def discount(self, discount_value, affected_quantity, incl_tax=True, offer=None):
        self._applied_offer = offer
        return super().discount(discount_value, affected_quantity, incl_tax)

    @property
    def applied_offer(self):
        return self._applied_offer

    def clear_discount(self):
        self._applied_offer = None
        super().clear_discount()

    @property
    def has_range_discount(self):
        return self.purchase_info.price.has_range_discount

    @property
    def unit_price_excl_tax_incl_range_discount(self):
        return self.purchase_info.price.incl_range_discount_excl_tax

    @property
    def unit_price_incl_tax_incl_range_discount(self):
        return self.purchase_info.price.incl_range_discount_incl_tax

    @property
    def unit_price_retail(self):
        return self.purchase_info.price.retail

    @property
    def line_price_excl_tax_incl_range_discount(self):
        if self.unit_price_excl_tax_incl_range_discount is not None:
            return self.quantity * self.unit_price_excl_tax_incl_range_discount

    @property
    def line_price_incl_tax_incl_range_discount(self):
        if self.unit_price_incl_tax_incl_range_discount is not None:
            return self.quantity * self.unit_price_incl_tax_incl_range_discount

    @property
    def line_price_excl_tax_incl_discounts(self):
        price_excl_tax = self.line_price_excl_tax_incl_range_discount
        if self._discount_excl_tax and price_excl_tax is not None:
            return price_excl_tax - self._discount_excl_tax
        if self._discount_incl_tax and self.line_price_incl_tax is not None:
            # This is a tricky situation.  We know the discount as calculated
            # against tax inclusive prices but we need to guess how much of the
            # discount applies to tax-exclusive prices.  We do this by
            # assuming a linear tax and scaling down the original discount.
            return price_excl_tax - self._tax_ratio * self._discount_incl_tax
        return price_excl_tax

    @property
    def line_price_incl_tax_incl_discounts(self):
        # We use whichever discount value is set.  If the discount value was
        # calculated against the tax-exclusive prices, then the line price
        # including tax
        price_incl_tax = self.line_price_incl_tax_incl_range_discount
        if price_incl_tax is not None:
            return price_incl_tax - self.discount_value

    @property
    def line_price_retail(self):
        return self.quantity * self.unit_price_retail


    @property
    def selected_pack(self):
        try:
            pack_id = self.attributes.get(option__code=Product.PACK_OPTION_CODE).value
            return Pack.objects.get(id=pack_id)
        except (LineAttribute.DoesNotExist, Pack.DoesNotExist, ValueError):
            return None

    @property
    def options_summary(self):
        options_str = ''
        for attr in self.attributes.all():
            value = attr.value
            if attr.option.code == Product.PACK_OPTION_CODE and value:
                try:
                    value = self.product.packs.get(pk=value)
                except Pack.DoesNotExist:
                    continue
            if value:
                options_str += '%s: %s,' % (attr.option.name, value)
        if len(options_str) > 0:
            options_str = options_str[:-1]
        return options_str


from oscar.apps.basket.models import *
