$(function() {

    let $basket = $('.bag-section')
    let $priceField=$("#js-basket-total");
    let $count=$basket.find("#counter");

    let isEmpty=false;
    let listeners={};

    $basket.find(".bag-snippet").click(function (ev) {
        if($(window).width()<768 && !isEmpty)
            return;

        ev.preventDefault();
        ev.stopImmediatePropagation();
        if(!isEmpty)
            $basket.toggleClass("active");
    });

    $(document).on('click', '.add-to-basket-form .submit', function(e) {
        e.preventDefault();
        let $form = $(e.target).closest('form');
        let productId = $form.closest('.js-row-container').data('pk');
        addProductToCart(productId, $form);
    });

    $(document).on('click', '.update-basket-form .submit', function(e) {
        e.preventDefault();
        let $form=$(e.target).closest('form');
        let productId = $form.data('pk');
        addProductToCart(productId, $form);
    });

    // Количество товаров
    $(document).on('click', '.plus', function (e) {
        e.preventDefault()
        e.stopPropagation()
        _changeQuantityByDelta($(this), + 1)
    });
    $(document).on('click', '.minus', function (e) {
        e.preventDefault()
        e.stopPropagation()
        _changeQuantityByDelta($(this), - 1)
    });

    $(document).on('change', '.update-basket-form input[name=quantity]', function(e) {
        e.preventDefault()
        e.stopPropagation()
        let $form=$(this).closest('form');
        let productId = $form.data('pk');
        _updateProductInBaskets(productId, $form);
        return false;
    });

    $(document).on('click', '.delete-from-basket-form .submit', function(e) {
        e.preventDefault();
        let $form=$(e.target).closest('form');
        let productId = $form.data('pk');
        deleteProductFromCart(productId, $form);
    });

    let _changeQuantityByDelta = function($this, delta) {
        let $input = $this.parent().find('input.js-quantity');
        let newValue = parseInt($input.val()) + delta;
        newValue = newValue < 1 ? 1 : newValue;
        $input.val(newValue);
        $input.change();
    }

    let _findProductRow = function(productId) {
        return $('.js-row-container[data-pk=' + productId + ']');
    }

    let _declareProductAddedToBasket = function(productId) {
        let $row = _findProductRow(productId);
        $row.find('form .submit').addClass('active').text('в корзине');
    }

    let _declareProductRemovedFromBasket = function(productId) {
        let $row = _findProductRow(productId);
        $row.find('form .submit').removeClass('active').text('в корзину');
    }

    let _toggleProductLoader = function(productId) {
        let $row = _findProductRow(productId)
        let $loader = $row.find('.loading__overlay')
        $loader.toggle();
    }

    let addProductToCart = function(productId, $form){
        _toggleProductLoader(productId);
        _toggleBasketLoader();
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            type: 'POST',
            success: function (data) {
                _toggleProductLoader(productId);
                _updateBaskets(data);
                _declareProductAddedToBasket(productId);
            },
            error: function(data) {
                _toggleProductLoader(productId);
                _toggleBasketLoader();

            },
        });
    }

    let _updateProductInBaskets = function(productId, $form) {
        _toggleProductLoader(productId);

        _toggleBasketLoader();
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            type: 'POST',
            success: function (data) {
                _toggleProductLoader(productId);
                _toggleBasketLoader();
                _updateBaskets(data);
            },
            error: function(data) {

            },
        });
    }

    let deleteProductFromCart = function(productId, $form) {
        _toggleProductLoader(productId);
        _toggleBasketLoader();

        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            type: 'POST',
            success: function (data) {
                _toggleProductLoader(productId);
                _toggleBasketLoader();
                _updateBaskets(data)
                _declareProductRemovedFromBasket(productId);
            },
            error: function(data) {
                _toggleProductLoader(productId);
                _toggleBasketLoader();

            },
        });
    }
})

let $basket = $('.bag-section')
let $priceField=$("#js-basket-total");
let $count=$basket.find("#counter");

let _toggleBasketLoader=function(){
    let $loadingOverlay=$(".bag-section .loading__overlay");
    $loadingOverlay.toggle()
}

let _updateBaskets = function(data) {
    let $smallBasketUpdatablePart = $('#small-basket-updatable-wrapper');
    let $largeBasketUpdatablePart = $('#large-basket-updatable-wrapper');

    if ($smallBasketUpdatablePart.length > 0) {
        _toggleBasketLoader();
        $smallBasketUpdatablePart.html(data.html);
        $count.html($smallBasketUpdatablePart.find(".product-row").length);
    } else {
        $largeBasketUpdatablePart.html(data.html);
        $priceField.html(data.total)
    }
};