from ratelimit import RateLimitException

from search.utils import update_index
from django.contrib import admin
from .models import QueryString


class IndexUpdateAdminMixin(object):
    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        try:
            update_index()
        except RateLimitException:
            pass


class IndexUpdateRelatedAdminMixin(object):
    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)
        try:
            update_index()
        except RateLimitException:
            pass


@admin.register(QueryString)
class RecommendationEmails(admin.ModelAdmin):
    fields = ('text', 'count')
    list_display = ('text', 'count')