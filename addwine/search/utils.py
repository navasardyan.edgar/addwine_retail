from ratelimit import limits

from .tasks import update_index as update_index_task


TEN_MINUTES = 600


@limits(calls=1, period=TEN_MINUTES)
def update_index():
    update_index_task.apply_async(countdown=1)
