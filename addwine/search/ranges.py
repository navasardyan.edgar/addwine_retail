from collections import OrderedDict

from django.conf import settings

from catalogue.models import ProductAttribute


def get_ranges():
    search_ranges = list(settings.SEARCH_RANGES.items())
    search_ranges.extend([
        (attribute.code, {'name': attribute.name, 'field': attribute.code}) for attribute in
        ProductAttribute.objects.filter(
            type__in=[ProductAttribute.INTEGER, ProductAttribute.FLOAT], facets_on=True
        )
    ])

    return OrderedDict(search_ranges)


def apply_aggregation(sqs):
    for range in get_ranges().values():
        sqs = sqs.add_aggregation(range['field'], 'min')
        sqs = sqs.add_aggregation(range['field'], 'max')
    return sqs


class RangeMunger(object):
    def __init__(self, ranges, intervals):
        self.ranges = ranges
        self.intervals = intervals

    def range_data(self):
        data = OrderedDict()
        display_ranges = get_ranges()
        for key, display_range in display_ranges.items():
            self.munge_range(key, display_range, self.ranges.get(key, {}), data)
        return data

    def munge_range(self, key, display_range, query_interval, cleaned_data):
        range_interval = self.intervals.get(key) or {'min': 0, 'max': 0}
        range_interval_min = range_interval.get('min')
        range_interval_max = range_interval.get('max')

        cleaned_data[key] = {
            'name': display_range['name'],
            'min': query_interval.get('min', range_interval_min),
            'max': query_interval.get('max', range_interval_max)
        }

        if query_interval.get('min'):
            cleaned_data[key].update({
                'query_min': query_interval['min']
            })

        if query_interval.get('max'):
            cleaned_data[key].update({
                'query_max': query_interval['max']
            })
