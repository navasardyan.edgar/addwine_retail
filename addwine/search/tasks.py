from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.utils.timezone import now
from haystack.management.commands import update_index as update_index_command

logger = get_task_logger(__name__)

TASKS_CONFIGS_UPDATE_INDEX = {
    'name': "search.update_index",
    'ignore_result': True,
    'retry': False
}

TASKS_CONFIGS_UPDATE_INDEX.update(settings.HAYSTACK_TASK_CONFIG)


@shared_task(**TASKS_CONFIGS_UPDATE_INDEX)
def update_index():
    start_time = now()
    try:
        update_index_command.Command().handle(using=['default'], interactive=False)
    except Exception as e:
        logger.exception(e)
    interval = now() - start_time
    logger.info('Index was updated. Work-time %s' % (interval.microseconds/1000))

