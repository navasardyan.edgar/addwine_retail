from urllib.parse import quote
from sorl.thumbnail import get_thumbnail
from django.db.models import Q
from django.db.utils import ProgrammingError, OperationalError
from haystack import indexes
from oscar.apps.analytics.models import ProductRecord

from oscar.core.loading import get_class, get_model

# Load default strategy (without a user/request)
from catalogue.models import ProductAttribute, Category
from offer.models import Range

Selector = get_class('partner.strategy', 'Selector')


def patch_product_index(cls):
    int_or_float_type_range = [ProductAttribute.INTEGER, ProductAttribute.FLOAT]
    custom_fields = {}
    try:
        for attr in ProductAttribute.objects.filter(type__in=[
            ProductAttribute.INTEGER, ProductAttribute.FLOAT, ProductAttribute.OPTION, ProductAttribute.MULTI_OPTION]):
            if attr.type in int_or_float_type_range:
                field = indexes.FloatField(null=True, )
            else:
                if attr.type == ProductAttribute.MULTI_OPTION:
                    field = indexes.MultiValueField(null=True, faceted=True)
                else:
                    field = indexes.CharField(null=True, faceted=True)
                field.set_instance_name(attr.code)

                # Attach facet field to cls
                shadow_facet_name = attr.code + "_exact"
                shadow_facet_field = field.facet_class(facet_for=attr.code)
                shadow_facet_field.set_instance_name(shadow_facet_name)
                custom_fields[shadow_facet_name] = shadow_facet_field
                setattr(cls, shadow_facet_name, shadow_facet_field)
            custom_fields[attr.code] = field
            setattr(cls, attr.code, field)
        setattr(cls, 'custom_fields', custom_fields)
    except (ProgrammingError, OperationalError):
        # catch errors to migrate empty db
        pass
    return cls


@patch_product_index
class ProductIndex(indexes.SearchIndex, indexes.Indexable):

    # Fields for faceting
    category = indexes.MultiValueField(null=True, faceted=True)
    product_range = indexes.MultiValueField(null=True, faceted=True)
    product_class = indexes.CharField(null=True, faceted=True)
    price = indexes.FloatField(null=True)
    old_price = indexes.FloatField(null=True)
    brand = indexes.CharField(null=True, faceted=True)
    product_id = indexes.CharField(null=True)
    available = indexes.BooleanField()
    active = indexes.BooleanField()

    text = indexes.EdgeNgramField(document=True)

    url = indexes.CharField(model_attr='get_absolute_url')
    upc = indexes.CharField(model_attr='upc')
    title = indexes.EdgeNgramField()
    thumbnail_url = indexes.CharField()

    relevancy_rating = indexes.FloatField()
    rating = indexes.FloatField(model_attr='rating')
    date_created = indexes.DateTimeField(model_attr='date_created')
    date_updated = indexes.DateTimeField(model_attr='date_updated')

    _strategy = None

    def __init__(self):
        try:
            self.fields.update(ProductIndex.custom_fields)
        except AttributeError:
            pass
        super().__init__()

    def get_model(self):
        return get_model('catalogue', 'Product')

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(is_active=True).select_related('product_class')\
            .prefetch_related('categories',).order_by('-date_updated')

    def read_queryset(self, using=None):
        return self.get_model().objects.filter(is_active=True).base_queryset()

    @staticmethod
    def prepare_text(obj):
        return obj.title

    @staticmethod
    def prepare_title(obj):
        return ' '.join((
            obj.title, obj.upc, obj.search_key_words
        ))

    def prepare_thumbnail_url(self, obj):
        image = getattr(obj.primary_image(), 'original', None)
        if image:
            im = get_thumbnail(image, '100x100', crop='center', quality=99)
            return im.url
        return ""

    def prepare_product_id(self, obj):
        return str(obj.id)

    def prepare_rating(self, obj):
        return obj.rating

    def prepare_product_class(self, obj):
        return obj.get_product_class().name

    def prepare_relevancy_rating(self, obj):
        try:
            return obj.stats.score
        except ProductRecord.DoesNotExist:
            return 0

    def prepare_brand(self, obj):
        return obj.brand.title

    def prepare_category(self, obj):
        categories = list(obj.categories.all())
        slugs = [category.full_slug for category in categories]
        if len(categories) > 0:
            all_slugs = []
            for slug in slugs:
                parents = slug.split(Category._slug_separator)
                all_slugs.append(slug)
                for i in range(1, len(parents)):
                    all_slugs.append(Category._slug_separator.join(parents[:i]))
            return all_slugs

    def prepare_product_range(self, obj):
        ranges = Range.objects.filter(
            Q(includes_all_products=True)|
            Q(included_products__id=obj.id)|
            Q(classes__id=obj.product_class.id)|
            Q(included_categories__in=obj.categories.all()) |
            Q(included_brands__id=obj.brand.id)
        ).exclude(excluded_products__id=obj.id)

        return [range.slug for range in ranges]

    # Pricing and stock is tricky as it can vary per customer.  However, the
    # most common case is for customers to see the same prices and stock levels
    # and so we implement that case here.

    def get_strategy(self):
        if not self._strategy:
            self._strategy = Selector().strategy()
        return self._strategy

    def prepare_available(self, obj):
        return obj.is_available

    def prepare_active(self, obj):
        return obj.is_active

    def prepare_price(self, obj):
        strategy = self.get_strategy()
        result = None
        if obj.is_parent:
            result = strategy.fetch_for_parent(obj)
        elif obj.has_stockrecords:
            result = strategy.fetch_for_product(obj)

        if result:
            if result.price.has_range_discount:
                if result.price.is_tax_known:
                    return result.price.incl_range_discount_incl_tax
                return result.price.excl_range_discount_incl_tax
            else:
                if result.price.is_tax_known:
                    return result.price.incl_tax
                return result.price.excl_tax

    def prepare_old_price(self, obj):
        strategy = self.get_strategy()
        result = None
        if obj.is_parent:
            result = strategy.fetch_for_parent(obj)
        elif obj.has_stockrecords:
            result = strategy.fetch_for_product(obj)

        if result:
            if result.price.is_tax_known:
                return result.price.incl_tax
            return result.price.excl_tax

    def _process_attribute_option_value(self, attribute_value):
        try:
            iter(attribute_value)
            return [str(value) for value in attribute_value]
        except (TypeError, ValueError):
            pass

        return str(attribute_value)

    def prepare(self, obj):
        prepared_data = super(ProductIndex, self).prepare(obj)

        int_or_float_type_range = [ProductAttribute.INTEGER, ProductAttribute.FLOAT]
        for attribute_value in obj.attribute_values.filter(attribute__facets_on=True, attribute__type__in=[
            ProductAttribute.INTEGER, ProductAttribute.FLOAT, ProductAttribute.OPTION, ProductAttribute.MULTI_OPTION
        ]).select_related("attribute"):
            if attribute_value.value is None:
                continue
            if attribute_value.attribute.type in int_or_float_type_range:
                prepared_data[attribute_value.attribute.code] = float(attribute_value.value)
            else:
                prepared_data[attribute_value.attribute.code] = self._process_attribute_option_value(
                    attribute_value.value)

        return prepared_data

    def get_updated_field(self):
        """
        Used to specify the field used to determine if an object has been
        updated

        Can be used to filter the query set when updating the index
        """
        return 'date_updated'
