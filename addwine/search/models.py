from django.db import models


class QueryString(models.Model):
    text = models.CharField('Запрос', max_length=120)
    count = models.PositiveSmallIntegerField('Кол-во', default=1)

    class Meta:
        verbose_name_plural = 'Поисковые запросы'
        verbose_name = 'Поисковый запрос'
        ordering = ['-count']
