from django.utils import six
from drf_haystack.serializers import HighlighterMixin, HaystackSerializer
from drf_haystack.viewsets import HaystackViewSet
from drf_haystack.filters import HaystackHighlightFilter, HaystackAutocompleteFilter

from catalogue.models import Product
from search.search_indexes import ProductIndex
from rest_framework import response
from .models import QueryString
from django.db.models import F
from rest_framework.decorators import api_view
from django.views.generic import TemplateView
from haystack.utils import Highlighter
from django.utils.html import strip_tags
from catalogue.views import SommelierCardsMixin, PageViewMixin
from django.views.decorators.csrf import csrf_exempt
from haystack.query import SearchQuerySet
from rest_framework.response import Response


class HighlighterNoDots(Highlighter):
    def highlight(self, text_block):
        self.text_block = strip_tags(text_block)
        highlight_locations = self.find_highlightable_words()
        return self.render_html(highlight_locations, 0, len(self.text_block))


class FilteringHighlighterMixin(HighlighterMixin):
    highlighter_filter_field = None

    def to_representation(self, instance):
        ret = super(HighlighterMixin, self).to_representation(instance)
        terms = " ".join(six.itervalues(self.context["request"].GET))
        if terms:
            highlighter = self.get_highlighter()(terms, **{
                "html_tag": self.highlighter_html_tag,
                "css_class": self.highlighter_css_class,
                "max_length": self.highlighter_max_length
            })
            document_field = self.get_document_field(instance)
            if highlighter and document_field:
                hl = highlighter.highlight(getattr(instance, self.highlighter_filter_field or document_field))
                if hl and 'span' in hl:
                    ret["highlighted"] = highlighter.highlight(getattr(instance, self.highlighter_field or document_field))

        hl = ret.get('highlighted', None)
        return ret if hl else None


class ProductSearchSerializer(FilteringHighlighterMixin, HaystackSerializer):
    highlighter_field = 'text'
    highlighter_filter_field = 'title'
    highlighter_max_length = 300
    highlighter_class = HighlighterNoDots

    class Meta:
        index_classes = [ProductIndex]
        fields = [
            "product_id", "text", "title", "upc", "url", "price",
            "old_price", "thumbnail_url", "rating", "available"
        ]
        field_aliases = {
            "q": "title"
        }


class FilteredHaystackViewSet(HaystackViewSet):
    def get_queryset(self, index_models=[]):
        q = super().get_queryset(index_models)
        return q.filter(available=True).order_by('-rating')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        serializer = self.get_serializer(queryset, many=True)
        data = [v for v in serializer.data if v]
        return Response(data)


class ProductSearchView(FilteredHaystackViewSet):
    index_models = [Product, ]
    serializer_class = ProductSearchSerializer
    filter_backends = [HaystackAutocompleteFilter, HaystackHighlightFilter]


@csrf_exempt
@api_view(['POST', ])
def save_quertystring(request):
    try:
        data = request.data
        q = data['text']

        obj = QueryString.objects.get(text=q)
        obj.count = F('count')+1
        obj.save()
        return response.Response(200)
    except QueryString.DoesNotExist:
        QueryString.objects.create(text=q)
        return response.Response(200)
    except (KeyError, ValueError):
        return response.Response(400)


class SearchResultView(SommelierCardsMixin, PageViewMixin, TemplateView):
    context_object_name = "products"
    template_name = 'web/catalogue/search.html'
    MAX_COUNT = 100

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        products = SearchQuerySet()\
            .filter(title=kwargs.get('query'))\
            .order_by('-rating')
        highlighter = HighlighterNoDots(kwargs.get('query'))
        ctx['products_count'] = len(products)
        products = products[:self.MAX_COUNT]

        ids = [p.product_id for p in products if 'span' in highlighter.highlight(p.title)]
        if products:
            ctx['products'] = Product.objects.filter(pk__in=ids)
        else:
            ctx['products'] = None
        ctx['query_text'] = kwargs.get('query')
        return ctx

