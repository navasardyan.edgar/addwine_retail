import operator

from datetime import timedelta

from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core import exceptions, urlresolvers
from decimal import Decimal as D, Decimal
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save, pre_save
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from oscar.core.loading import get_model
from oscar.models import fields
from oscar.apps.offer import utils

from oscar.apps.offer import abstract_models
from treebeard.mp_tree import MP_Node, MP_NodeManager

from customer.models import Contacts
from marketing.models import MailCTA, SommelierCTA, SEOText
from offer.utils import update_discounts
from seo.models import AbstractSeoConfigs
from django.dispatch import receiver


class RangeGroupManager(MP_NodeManager):
    def get_queryset(self):
        return super(RangeGroupManager, self).get_queryset().prefetch_related('ranges')


class RangeGroup(MP_Node):
    title = models.CharField(verbose_name='название', max_length=1024)
    _full_name_separator = ' > '

    objects = RangeGroupManager()

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        """
        Returns a string representation of the category and it's ancestors,
        e.g. 'Books > Non-fiction > Essential programming'.

        It's rarely used in Oscar's codebase, but used to be stored as a
        CharField and is hence kept for backwards compatibility. It's also
        sufficiently useful to keep around.
        """
        names = [category.title for category in self.get_ancestors_and_self()]
        return self._full_name_separator.join(names)

    def get_ancestors_and_self(self):
        """
        Gets ancestors and includes itself. Use treebeard's get_ancestors
        if you don't want to include the category itself. It's a separate
        function as it's commonly used in templates.
        """
        return list(self.get_ancestors()) + [self]

    def get_descendants_and_self(self):
        """
        Gets descendants and includes itself. Use treebeard's get_descendants
        if you don't want to include the category itself. It's a separate
        function as it's commonly used in templates.
        """
        return list(self.get_descendants()) + [self]

    def has_children(self):
        return self.get_num_children() > 0

    def get_num_children(self):
        return self.get_children().count()

    class Meta:
        verbose_name = 'группа ассортиментов'
        verbose_name_plural = 'группы ассортиментов'


class RangeDiscount(AbstractSeoConfigs, models.Model):
    cover = models.ImageField(
        'Обложка', upload_to=settings.OSCAR_DISCOUNTS_IMAGE_FOLDER, max_length=255, blank=True, null=True)
    name = models.CharField(
        _("Name"), max_length=128, unique=True, )
    code = models.SlugField(_("Code"), max_length=128, unique=True)
    rules = RichTextUploadingField('Условия участия в акции')
    mail_cta = models.ForeignKey(MailCTA, verbose_name="призыв к действию: рассылка", null=True, blank=True,
                                 on_delete=models.SET_NULL)
    display_for_users = models.BooleanField('Отображать на сайте', default=True)

    range = models.ForeignKey(
        'offer.Range',
        related_name='discounts',
        on_delete=models.CASCADE,
        verbose_name=_("Range"))

    # Benefit types
    PERCENTAGE, FIXED = ("Percentage", "Absolute",)
    TYPE_CHOICES = (
        (PERCENTAGE, _("Discount is a percentage off of the product's value")),
        (FIXED, _("Discount is a fixed amount off of the product's value")),
    )
    type = models.CharField(
        _("Type"), max_length=128, choices=TYPE_CHOICES, blank=True)

    # The value to use with the designated type
    value = fields.PositiveDecimalField(
        _("Value"), decimal_places=2, max_digits=12, null=True, blank=True)

    # Some complicated situations require offers to be applied in a set order.
    priority = models.IntegerField(
        _("Priority"), default=0,
        help_text=_("The highest priority offers are applied first"))

    # AVAILABILITY

    end_datetime = models.DateTimeField(
        _("End date"),
        help_text=_("Offers are active until the end date. "
                    "Leave this empty if the offer has no expiry date."))

    _was_available = models.BooleanField(default=False)

    # TRACKING
    # These fields are used to enforce the limits set by the
    # max_* fields above.

    total_discount = models.DecimalField(
        _("Total Discount"), decimal_places=2, max_digits=12,
        default=D('0.00'))

    num_orders = models.PositiveIntegerField(
        'Куплено товаров', default=0)

    hidden_in_admin = models.BooleanField('Служебная акция', default=False)
    active = models.BooleanField('Активно', default=True)
    date_created = models.DateTimeField(_("Date Created"), auto_now_add=True)

    def as_dict(self):
        value = self.value
        if self.type == self.PERCENTAGE:
            value = round(self.value, 0)
        return {
            'name': self.name,
            'code': self.code,
            'rules': self.rules,
            'type': self.type,
            'value': value,
            'symbol': '%' if self.type == self.PERCENTAGE else 'руб',
            'end_datetime': self.end_datetime.strftime('%d.%m.%Y')
        }

    def __str__(self):
        return self.name

    def __update_information_about_products(self):
        update_discounts(self.id)

    def is_available(self, test_date=None, update_if_state_changed=True):
        update_if_state_changed = test_date is None or update_if_state_changed
        if test_date is None:
            test_date = now()

        if not self.active:
            return False

        if test_date > self.end_datetime:
            if self._was_available and update_if_state_changed:
                self._was_available = False
                self.save()
                self.__update_information_about_products()
            return False

        if not self._was_available and update_if_state_changed:
            self._was_available = True
            self.save()
            self.__update_information_about_products()
        return True

    def calculate_discount(self, product, price):
        discount = D("0.00")
        if not product.is_discountable or not self.is_available():
            return discount
        if self.type == self.PERCENTAGE:
            discount = self.value / Decimal("100.00") * price
        elif self.type == self.FIXED:
            discount = self.value
        if discount > price:
            return price
        return discount

    @staticmethod
    def post_save(sender, **kwargs):
        instance = kwargs.get('instance')
        instance._was_available = instance.is_available()

    def get_absolute_url(self):
        return reverse('discount', args=[self.code, ])

    class Meta:
        app_label = 'offer'
        ordering = ['priority', ]
        verbose_name = 'скидка'
        verbose_name_plural = 'скидки'


post_save.connect(RangeDiscount.post_save, sender=RangeDiscount)


class ConditionalOffer(abstract_models.AbstractConditionalOffer):
    def apply_benefit(self, basket):
        return self.benefit.proxy().apply(
            basket, self.condition.proxy(), self)

    def rollback_usage(self, discount):
        self.total_discount -= discount['discount']
        self.num_applications = max(self.num_applications - discount['freq'], 0)
        if self.num_orders > 0:
            self.num_orders -= 1
        self.save()

    @property
    def remaining_num_of_applications(self):
        if self.max_global_applications:
            return self.max_global_applications - self.num_applications
        else:
            return None

    @property
    def remaining_available_discount(self):
        if self.max_discount:
            return self.max_discount - self.total_discount
        else:
            return None


class Benefit(abstract_models.AbstractBenefit):
    one_product_application = models.BooleanField(verbose_name='Применять только к 1 продукту', default=False,
                                                  blank=True)

    def get_applicable_lines(self, offer, basket, range=None):
        """
                Return the basket lines that are available to be discounted

                :basket: The basket
                :range: The range of products to use for filtering.  The fixed-price
                        benefit ignores its range and uses the condition range
                """
        if range is None:
            range = self.range
        line_tuples = []
        for line in basket.all_lines():
            product = line.product

            if (not range.contains(product) or
                    not self.can_apply_benefit(line)):
                continue

            price = utils.unit_price(offer, line)
            if not price:
                # Avoid zero price products
                continue
            line_tuples.append((price, line))

        # Select product with highest price
        if self.one_product_application and line_tuples:
            line_tuples.sort(key=lambda tup: tup[0], reverse=True)
            line_tuples = [line_tuples[0]]

        # We sort lines to be cheapest first to ensure consistent applications
        return sorted(line_tuples, key=operator.itemgetter(0))

    def can_apply_benefit(self, line):
        return super().can_apply_benefit(line) and not line.has_range_discount

    def proxy(self):
        from offer.benefits import BestPercentageDiscountBenefit, BestAbsoluteDiscountBenefit
        from oscar.apps.offer import benefits

        klassmap = {
            self.PERCENTAGE: BestPercentageDiscountBenefit,
            self.FIXED: BestAbsoluteDiscountBenefit,
            self.MULTIBUY: benefits.MultibuyDiscountBenefit,
            self.FIXED_PRICE: benefits.FixedPriceBenefit,
            self.SHIPPING_ABSOLUTE: benefits.ShippingAbsoluteDiscountBenefit,
            self.SHIPPING_FIXED_PRICE: benefits.ShippingFixedPriceBenefit,
            self.SHIPPING_PERCENTAGE: benefits.ShippingPercentageDiscountBenefit
        }
        # Short-circuit logic if current class is already a proxy class.
        if self.__class__ in klassmap.values():
            return self

        field_dict = dict(self.__dict__)
        for field in list(field_dict.keys()):
            if field.startswith('_'):
                del field_dict[field]

        if self.proxy_class:
            klass = utils.load_proxy(self.proxy_class)
            # Short-circuit again.
            if self.__class__ == klass:
                return self
            return klass(**field_dict)

        if self.type in klassmap:
            return klassmap[self.type](**field_dict)
        raise RuntimeError("Unrecognised benefit type (%s)" % self.type)


class Condition(abstract_models.AbstractCondition):
    def can_apply_condition(self, line):
        return super().can_apply_condition(line) and not line.has_range_discount


class RangeInnerCategory(models.Model):
    title = models.CharField('название', max_length=1024)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'внутренняя категория ассортиментов'
        verbose_name_plural = 'внутренняя категории ассортиментов'


class Range(AbstractSeoConfigs, abstract_models.AbstractRange):
    inner_category = models.ForeignKey(RangeInnerCategory, verbose_name='внутренняя категория', null=True, blank=True)
    group = models.ForeignKey(RangeGroup, verbose_name='Группа ассортиментов',
                              blank=True, null=True, related_name='ranges', limit_choices_to={'depth': 2})
    slug = models.SlugField(
        _("Slug"), max_length=128, unique=True)
    description = None
    included_products = models.ManyToManyField(
        'catalogue.Product', related_name='includes', blank=True,
        verbose_name=_("Included Products"))
    admin_comment = models.TextField(verbose_name='Комментарий', default='', blank=True)

    mail_cta = models.ForeignKey(MailCTA, verbose_name="призыв к действию: рассылка", null=True, blank=True,
                                 on_delete=models.SET_NULL)
    sommelier_cta = models.ForeignKey(SommelierCTA, verbose_name="призыв к действию: сомелье", null=True, blank=True,
                                      on_delete=models.SET_NULL, related_name='attached_ranges')
    rules_guarantee = RichTextField('гарантия', default='', blank=True)
    order_field = models.PositiveIntegerField(verbose_name='Нажать и тащить', default=0, blank=True)
    hidden_in_admin = models.BooleanField('Служебная акция', default=False)

    included_brands = models.ManyToManyField(
        'catalogue.Brand', related_name='includes', blank=True,
        verbose_name='Включенные бренды')

    __brand_ids = None

    def contains_product(self, product):
        contains = super(Range, self).contains_product(product)
        if not contains:
            contains = product.brand in self.included_brands.all()
        return contains

    def add_product(self, product, display_order=None):
        self.included_products.add(product)

    def remove_product(self, product):
        self.included_products.remove(product)

    def _included_product_ids(self):
        return self.included_products.values_list("id", flat=True)

    def _brand_ids(self):
        if self.__brand_ids is None:
            self.__brand_ids = list(self.included_brands.values_list('pk', flat=True))
        return self.__brand_ids

    def invalidate_cached_ids(self):
        super(Range, self).invalidate_cached_ids()
        self.__brand_ids = None

    def all_products(self):
        """
        Return a queryset containing all the products in the range

        This includes included_products plus the products contained in the
        included classes and categories, minus the products in
        excluded_products.
        """
        if self.proxy:
            return self.proxy.all_products()

        Product = get_model("catalogue", "Product")
        if self.includes_all_products:
            # Filter out child products
            return Product.browsable.all()

        return Product.objects.filter(
            Q(id__in=self._included_product_ids()) |
            Q(product_class_id__in=self._class_ids()) |
            Q(productcategory__category_id__in=self._category_ids()) |
            Q(brand_id__in=self._brand_ids())
        ).exclude(id__in=self._excluded_product_ids()).distinct()

    @staticmethod
    def post_save(sender, **kwargs):
        instance = kwargs.get('instance')
        for discount in instance.discounts.all():
            update_discounts(discount.id)

    class Meta:
        ordering = ['order_field']
        verbose_name = 'ассортимент'
        verbose_name_plural = 'ассортименты'


post_save.connect(Range.post_save, sender=Range)


class RangeSEOText(models.Model):
    category = models.ForeignKey(Range, verbose_name='ассортимент',
                                 on_delete=models.CASCADE, related_name='seo_texts')
    seo_text = models.ForeignKey(SEOText, verbose_name='SEO текст', related_name='ranges')

    def __str__(self):
        return 'SEO текст для ассортимента %s' % self.category.name

    class Meta:
        verbose_name = 'SEO текст'
        verbose_name_plural = 'SEO тексты'


from oscar.apps.offer.models import *


class SubscriptionForDiscount(Contacts):
    EMAIL_TEMPLATE_CODE = 'SUBSCRIPTION_FOR_DISCOUNT'

    product = models.ForeignKey(
        'catalogue.Product',
        verbose_name='Товар',
        related_name='subscription_for_discount',
        on_delete=models.CASCADE)

    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return ''.join([
            'https://',
            Site.objects.get_current().domain,
            urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        ])

    def as_dict(self):
        data = super().as_dict()
        data['product'] = self.product.as_dict()
        data['discount'] = self.product.discount.as_dict()
        data['date_created'] = self.date_created.strftime('%d.%m.%Y')
        return data

    def __str__(self):
        return 'подписка на скидку %s' % self.product

    class Meta:
        verbose_name = 'подписка на скидку'
        verbose_name_plural = 'подписки на скидку'


class RangeOrder(models.Model):
    range = models.ForeignKey(Range, null=False, blank=False)
    product = models.ForeignKey('catalogue.Product', null=False, blank=False)
    manual_order_id = models.PositiveIntegerField(verbose_name="Нажать и тащить", null=False, blank=False)

    class Meta(object):
        ordering = ('manual_order_id', )
        unique_together = ("range", "product")


@receiver(post_save, sender=Range)
def range_order_update(instance, **kwargs):
    products = instance.all_products().defer('id')
    RangeOrder.objects.exclude(product__in=products).filter(range=instance).delete()
    current_products = [r.product.pk for r in RangeOrder.objects.filter(range=instance).select_related('product')]

    new_products = products.exclude(id__in=current_products)

    RangeOrder.objects.bulk_create([
        RangeOrder(range=instance, product=product, manual_order_id=i+1)
        for i, product in enumerate(new_products)
    ])
