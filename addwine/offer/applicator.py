from oscar.apps.offer import results
from oscar.apps.offer.applicator import Applicator as BaseApplicator


class Applicator(BaseApplicator):
    def apply_offers(self, basket, offers):
        for line in basket.all_lines():
            line.clear_discount()
        super().apply_offers(basket, offers)
        applied_offers = set()
        discount_sum = {}
        for line in basket.all_lines():
            if line.applied_offer:
                if not discount_sum.get(line.applied_offer.id):
                    discount_sum[line.applied_offer.id] = 0
                discount_sum[line.applied_offer.id] += line.discount_value
            applied_offers.add(line.applied_offer)
        applied_offers -= {None}

        real_offer_applications = results.OfferApplications()
        for application in basket.offer_applications:
            if application['offer'] in applied_offers:
                real_offer_applications.add(application['offer'],
                                            results.BasketDiscount(discount_sum.get(application['offer'].id, 0)))
        basket.offer_applications = real_offer_applications
