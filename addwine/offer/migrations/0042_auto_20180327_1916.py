# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-03-27 16:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offer', '0041_auto_20180327_0014'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rangediscount',
            name='name',
            field=models.CharField(max_length=128, verbose_name='Name'),
        ),
    ]
