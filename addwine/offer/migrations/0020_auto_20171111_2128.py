# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-11 18:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offer', '0019_remove_rangediscount_indexed_availability'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='range',
            options={'ordering': ['order_field'], 'verbose_name': 'ассортимент', 'verbose_name_plural': 'ассортименты'},
        ),
        migrations.AddField(
            model_name='range',
            name='order_field',
            field=models.PositiveIntegerField(blank=True, default=0, verbose_name='Нажать и тащить'),
        ),
    ]
