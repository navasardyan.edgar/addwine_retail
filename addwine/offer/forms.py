from django import forms
from django.core.exceptions import ValidationError

from offer.models import SubscriptionForDiscount


class SubscriptionForDiscountForm(forms.ModelForm):

    name = forms.CharField(label='Фамилия Имя Отчество', max_length=512, required=False)

    def __init__(self, *args, **kwargs):
        super(SubscriptionForDiscountForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['phone_number'].required = False

    def clean(self):
        cleaned_data = super(SubscriptionForDiscountForm, self).clean()

        email = cleaned_data['email']
        product = cleaned_data['product']

        try:
            SubscriptionForDiscount.objects.get(product=product, email=email)
            raise ValidationError(
                    'Invalid value for fields email: %(email). Value must be unique',
                    params={'email': email},
                )
        except SubscriptionForDiscount.DoesNotExist:
            pass

        return cleaned_data

    class Meta:
        model = SubscriptionForDiscount
        exclude = ('amocrm_id', 'date_created', )