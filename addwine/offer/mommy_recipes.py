from model_mommy import mommy
from model_mommy.recipe import Recipe
from .models import Range

mommy.generators.add('oscar.models.fields.slugfield.SlugField', lambda: 'slug')
mommy.generators.add('ckeditor_uploader.fields.RichTextUploadingField', lambda: '<html></html>')

rcp_range = Recipe(
    Range,
)
