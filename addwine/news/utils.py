import json

from django.conf import settings


def is_available_for_vote(request, article):
    voted_articles = request.COOKIES.get(settings.NEWS_RATING_COOKIE_KEY, '[]')
    try:
        voted_articles = json.loads(voted_articles)
    except ValueError as e:
        voted_articles = []
    if article.id not in voted_articles:
        return True, voted_articles
    return False, voted_articles