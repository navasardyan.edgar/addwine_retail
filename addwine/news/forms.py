from django import forms

from news.models import Comment, Article


class CommentForm(forms.ModelForm):

    name = forms.CharField(label='Фамилия Имя Отчество', max_length=512)

    class Meta:
        model = Comment
        fields = ('article', 'email', 'name', 'text')


class RateForm(forms.Form):
    article = forms.ModelChoiceField(queryset=Article.objects.all())
    rate = forms.IntegerField(min_value=1, max_value=5)
