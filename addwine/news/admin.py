#!/usr/bin/env python
# -*- coding: utf-8 -*-
from adminsortable2.admin import SortableInlineAdminMixin, SortableAdminMixin
from dal import autocomplete
from django import forms
from django.contrib import admin

from news import models
from news.models import cut_text, Comment, CategorySEOText


class CategorySEOTextInlineForm(forms.ModelForm):
    class Meta:
        model = CategorySEOText
        fields = '__all__'
        widgets = {
            'seo_text': autocomplete.ModelSelect2(url='autocomplete_seo_text'),
        }


class CategorySEOTextInline(admin.StackedInline):
    model = CategorySEOText
    form = CategorySEOTextInlineForm
    min_num = 0
    extra = 1


class CategoryAdminForm(forms.ModelForm):
    class Meta:
        model = models.Category
        fields = '__all__'
        widgets = {
            'mail_cta': autocomplete.ModelSelect2(url='autocomplete_mail_cta'),
        }


@admin.register(models.Category)
class CategoryAdmin(SortableAdminMixin, admin.ModelAdmin):
    form = CategoryAdminForm
    fields = ['title', 'slug', 'mail_cta']
    list_display = ('title',)
    prepopulated_fields = {"slug": ("title",)}
    inlines = [CategorySEOTextInline]


class CommentAdminInline(admin.StackedInline):
    model = Comment
    readonly_fields = (
        'phone_number', 'auto_filled_phone_number',
        'email', 'auto_filled_email',
        'first_name', 'last_name', 'auto_filled_name',
        'created'
    )
    fields = (
        'phone_number', 'auto_filled_phone_number',
        'email', 'auto_filled_email',
        'first_name', 'last_name', 'auto_filled_name',
        'text', 'created'
    )

    def has_add_permission(self, request):
        return False


@admin.register(models.Article)
class ArticleAdmin(admin.ModelAdmin):
    list_per_page = 9

    def snippet(self, object):
        return cut_text(object.text, 40)

    snippet.short_description = "Текст"

    fieldsets = (
        (None, {
            'fields': ['category', 'title', 'slug', 'cover', 'text', 'created', 'snippet_length', 'rating', 'votes']
        }),
        ('SEO', {
            'fields': (
                'seo_title', 'seo_description', 'seo_keywords',
                'seo_og_title', 'seo_og_description', 'seo_og_image',
            ),
        }),
    )
    list_display = ('title', 'category', 'created')

    readonly_fields = ('votes',)
    prepopulated_fields = {"slug": ("title",)}

    def rating(self, obj):
        return obj.rating

    rating.short_description = 'Рейтинг'

    inlines = [CommentAdminInline, ]
