from django.conf.urls import url

from news.views import AddCommentView, RateView

urlpatterns = [
    url(r'^add_comment$', AddCommentView.as_view(), name='add_comment'),
    url(r'^rate$', RateView.as_view(), name='rate'),
]