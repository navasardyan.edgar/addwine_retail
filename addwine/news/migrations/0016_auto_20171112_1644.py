# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-12 13:44
from __future__ import unicode_literals

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0015_auto_20171018_0136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(verbose_name='текст'),
        ),
    ]
