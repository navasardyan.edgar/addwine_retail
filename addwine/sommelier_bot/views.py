import json

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from telegram import Update
import logging

logger = logging.getLogger(__name__)


@csrf_exempt
@require_http_methods(["POST"])
def webhook(request):
    from sommelier_bot.bot import TELEGRAM_BOT
    try:
        json_body = json.loads(request.body.decode('utf-8'))
    except Exception as e:
        logger.exception('Can not parse json message from Telegram. Message: %s' % (request.body,))
        return HttpResponse(status=500)
    update = Update.de_json(json_body, TELEGRAM_BOT)
    TELEGRAM_BOT.dispatcher.process_update(update)
    return HttpResponse(status=200)
