from django.db import models
from django.template import engines


class AbstractTelegramUserFields(models.Model):
    telegram_username = models.CharField('Имя пользователя', max_length=255, unique=True, blank=True, null=True)
    telegram_chat_id = models.CharField('ID чата', max_length=255, blank=True, null=True)

    class Meta:
        abstract = True


class TelegramMessageTemplate(models.Model):
    name = models.CharField('Название', max_length=255)
    code = models.CharField('Код', max_length=255)
    text = models.TextField('Текст шаблона')

    def __str__(self):
        return self.name

    def get_message(self, ctx=None):
        if ctx is None:
            ctx = {}

        template = engines['django'].from_string(self.text)
        return template.render(ctx)

    class Meta:
        verbose_name = 'Шаблон Telegram сообщения'
        verbose_name_plural = 'Шаблоны Telegram сообщений'


class TelegramMessageTemplateTestVariable(models.Model):
    template = models.ForeignKey(TelegramMessageTemplate, verbose_name='Шаблон', related_name='test_variables')
    code = models.CharField('Код', max_length=255)
    value = models.CharField('Значение', max_length=2048, default='', blank=True)
    description = models.TextField('Описание')

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = 'Переменная шаблона'
        verbose_name_plural = 'Переменные шаблона'