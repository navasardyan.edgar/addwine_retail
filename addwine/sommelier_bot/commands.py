from customer.models import User
from sommelier_bot.bot_methods import send_message
from sommeliers.models import is_sommelier
from sommelier_bot import utils

import logging

logger = logging.getLogger(__name__)


def start(bot, update):
    username = update.message.from_user.username
    chat_id = update.message.chat.id
    sommelier = None
    template_code = utils.UNRECOGNIZED_USER
    if username:
        try:
            sommelier = User.objects.get(telegram_username=username)
        except User.DoesNotExist:
            logger.warning('Unrecognized user with username %s trying to get access to sommelier bot' % (username,))

    if sommelier and (is_sommelier(sommelier) or sommelier.is_superuser):
        template_code = utils.START
        sommelier.telegram_chat_id = chat_id
        sommelier.save()

    template = utils.get_template(template_code)
    if template is None:
        logger.error('Built-in telegram message template with code \'%s\' does not exists' % (template_code,))
        return
    send_message(chat_id, template.get_message())
