from sommelier_bot.models import TelegramMessageTemplate

START = 'START'
UNRECOGNIZED_USER = 'UNRECOGNIZED_USER'
MESSAGE_TO_CHAT = 'MESSAGE_TO_CHAT'
MESSAGE_TO_PERSON = 'MESSAGE_TO_PERSON'

import logging

logger = logging.getLogger(__name__)


def get_template(code):
    try:
        return TelegramMessageTemplate.objects.get(code=code)
    except TelegramMessageTemplate.DoesNotExist:
        return None
