from django.apps import AppConfig

class SommelierBotConfig(AppConfig):
    name = 'sommelier_bot'

    def ready(self):
        from .bot import setup_bot
        setup_bot()