import telegram
from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings

from common.utils import get_admin_settings
from sommelier_bot import utils
from sommeliers.models import SommelierTask, SommelierTaskSubmited

logger = get_task_logger(__name__)

TASKS_CONFIGS_NOTIFY_ABOUT_TASK = {
    'name': "sommelier_bot.notify_about_task",
    'ignore_result': True,
    'retry': False
}
TASKS_CONFIGS_NOTIFY_ABOUT_TASK.update(settings.TELEGRAM_BOT_TASK_CONFIG_NON_API_TASK)

TASKS_CONFIGS_SEND_MESSAGE = {
    'name': "sommelier_bot.send_message",
    'autoretry_for': (telegram.TelegramError,),
    'retry_backoff': True,
    'retry_kwargs': {'max_retries': 5},
    'ignore_result': True
}
TASKS_CONFIGS_SEND_MESSAGE.update(settings.TELEGRAM_BOT_TASK_CONFIG_API_TASK)


@shared_task(**TASKS_CONFIGS_NOTIFY_ABOUT_TASK)
def notify_about_task(sommelier_task_id):
    try:
        sommelier_task = SommelierTask.objects.get(id=sommelier_task_id)
    except SommelierTask.DoesNotExist:
        logger.error(
            'Task with id \'%s\' does not exist when task was started. Message was not sent' % (sommelier_task_id,))
        return
    if sommelier_task.sommelier:
        sommelier_task = SommelierTaskSubmited.objects.get(id=sommelier_task_id)
        template_code = utils.MESSAGE_TO_PERSON
        chat_id = sommelier_task.sommelier.telegram_chat_id

        if not chat_id:
            logger.error(
                'Chat id was not set for user with id \'%s\'. Message was not sent' % (sommelier_task.sommelier.id,))
            return
    else:
        template_code = utils.MESSAGE_TO_CHAT
        chat_id = get_admin_settings().telegram_channel_id

        if not chat_id:
            logger.error('Chat id of common telegram channel was not set. Message was not sent')
            return

    template = utils.get_template(template_code)
    if template is None:
        logger.error('Built-in telegram message template with code \'%s\' does not exist. Message was not sent' % (template_code,))
        return
    data = sommelier_task.as_dict()
    return send_message.delay(chat_id, template.get_message(data))


@shared_task(**TASKS_CONFIGS_SEND_MESSAGE)
def send_message(chat_id, message):
    from sommelier_bot.bot import TELEGRAM_BOT
    return TELEGRAM_BOT.send_message(chat_id, message)
