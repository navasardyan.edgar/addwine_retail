from distutils.core import setup


requirements = [
        'django-ckeditor==5.2.2',
        'django==1.11.6',
        'django-oscar==1.5',
        'pillow==4.2.1',
        'django-widget-tweaks==1.4.1',
        'django-compressor==2.1.1',
        'pycountry==17.5.14',
        'psycopg2',
        'django-admin-tools==0.8.1',
        'django-admin-sortable2==0.6.15',
        'django-autocomplete-light==3.2.9',
        'requests==2.17.3',
        'django-el-pagination==3.1.0',
        'django-sortedm2m==1.5.0',
        'celery==4.1.0',
        'django-celery-email==2.0.0',
        'python-telegram-bot==8.0',
        'shapely',
        'redis',
        'django-cacheops',
        'django-haystack==2.6.0',
        'elasticsearch>=2.0.0,<3.0.0',
        'bleach',
        'simplejson',
        'xlrd'
]

if __name__ == '__setup__':
    setup(
        name='addwine',
        version='0.1',
        url='http://addwine.ru',
        license='',
        author='Ilya Shmelev',
        author_email='netral23@yandex.ru',
        description='',
        requires=requirements
    )
