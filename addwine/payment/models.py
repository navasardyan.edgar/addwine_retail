from django.db import models
from django.forms import model_to_dict

from oscar.apps.payment.models import *


class PaymentMethod(models.Model):
    name = models.CharField('название', max_length=1024)
    enabled = models.BooleanField('отображать на сайте', default=True)
    logo = models.ImageField('логотип')

    def as_dict(self):
        data = model_to_dict(self)
        data['logo'] = self.logo.url if self.logo else None
        return data

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'способ оплаты'
        verbose_name_plural = 'способы оплаты'
