from django.conf import settings
from django.conf.urls import url

from catalogue.models import Product, Category, ProductClass
from common.views import robots, get_admin_autocomplete_view
from django.contrib.sitemaps.views import sitemap

from customer.models import User, CommunicationEventType
from customer.views import ProductAlertAddView, BirthdaySaveView

urlpatterns = [
    url(r'autocomplete/customer/users', get_admin_autocomplete_view(User, 'username').as_view()
        , name='autocomplete_user'),
    url(r'autocomplete/customer/email_templates', get_admin_autocomplete_view(CommunicationEventType, 'name').as_view()
        , name='autocomplete_email_templates'),
    url(r'product_alert/add$', ProductAlertAddView.as_view(), name='product-alert-add'),
    url(r'birthday/save$', BirthdaySaveView.as_view(), name='birthday-save'),
]
