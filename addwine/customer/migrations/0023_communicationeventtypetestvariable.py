# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-11 11:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0022_auto_20171011_0328'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommunicationEventTypeTestVariable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=255, verbose_name='Код')),
                ('value', models.CharField(default='', max_length=2048, verbose_name='Значение')),
                ('description', models.TextField(verbose_name='Описание')),
                ('template', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='customer.CommunicationEventType', verbose_name='Шаблон')),
            ],
            options={
                'verbose_name': 'Переменная шаблона',
                'verbose_name_plural': 'Переменные шаблона',
            },
        ),
    ]
