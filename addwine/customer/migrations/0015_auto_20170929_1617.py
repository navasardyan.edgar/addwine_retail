# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-29 13:17
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0014_auto_20170929_1602'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='contacts',
            unique_together=set([]),
        ),
    ]
