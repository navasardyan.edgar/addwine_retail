# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-29 11:06
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0010_auto_20170927_1206'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productalert',
            name='date_cancelled',
        ),
        migrations.RemoveField(
            model_name='productalert',
            name='date_closed',
        ),
        migrations.RemoveField(
            model_name='productalert',
            name='date_confirmed',
        ),
        migrations.RemoveField(
            model_name='productalert',
            name='key',
        ),
        migrations.RemoveField(
            model_name='productalert',
            name='status',
        ),
        migrations.AddField(
            model_name='productalert',
            name='phone_number',
            field=phonenumber_field.modelfields.PhoneNumberField(db_index=True, default='', max_length=128, verbose_name='Номер телефона'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='productalert',
            name='email',
            field=models.EmailField(db_index=True, max_length=254, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='productalert',
            name='user',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, related_name='alerts', to=settings.AUTH_USER_MODEL, verbose_name='User'),
            preserve_default=False,
        ),
    ]
