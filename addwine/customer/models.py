from django.contrib.auth.validators import UnicodeUsernameValidator, ASCIIUsernameValidator
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core import urlresolvers
from django.db.models.signals import pre_save
from django.forms import model_to_dict
from django.utils.translation import ugettext_lazy as _
from oscar.apps.customer.abstract_models import *
from phonenumber_field.modelfields import PhoneNumberField

from amocrm_api.models import AbstractAmoCRMFields
from amocrm_api.utils import amocrm_sync_user
from sommelier_bot.models import AbstractTelegramUserFields
from unisender_api.utils import unisender_sync_user


class CommunicationEventType(AbstractCommunicationEventType):
    class Meta:
        verbose_name = 'Шаблон письма'
        verbose_name_plural = 'Шаблоны писем'


class CommunicationEventTypeTestVariable(models.Model):
    template = models.ForeignKey(CommunicationEventType, verbose_name='Шаблон', related_name='test_variables')
    code = models.CharField('Код', max_length=255)
    value = models.CharField('Значение', max_length=2048, default='', blank=True)
    description = models.TextField('Описание')

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = 'Переменная шаблона'
        verbose_name_plural = 'Переменные шаблона'


class Contacts(AbstractAmoCRMFields, models.Model):
    phone_number = PhoneNumberField(_("Phone number"), blank=True, null=True)
    auto_filled_phone_number = models.BooleanField(verbose_name='Автозаполнен номер телефона', default=False)
    email = models.EmailField(verbose_name='Email', max_length=255, default="", blank=True, null=True)
    auto_filled_email = models.BooleanField(verbose_name='Автозаполнен email', default=False)
    first_name = models.CharField('Имя', max_length=255, default="", blank=True, null=True)
    last_name = models.CharField('Фамилия', max_length=255, default="", blank=True, null=True)
    auto_filled_name = models.BooleanField(verbose_name='Автозаполнено имя', default=False)

    recognized_user = models.ForeignKey('customer.User', on_delete=models.SET_NULL, blank=True, null=True, related_name='contacts')

    @property
    def name(self):
        full_name = ''
        if self.last_name:
            full_name += self.last_name
            if self.first_name:
                full_name += ' '
        if self.first_name:
            full_name += self.first_name
        return full_name

    @name.setter
    def name(self, value):
        parts = value.strip().split(" ")
        count_of_parts = len(parts)
        if count_of_parts == 0:
            return

        if count_of_parts > 1:
            self.last_name = parts[0]
            self.first_name = parts[1]
        else:
            self.first_name = parts[0]

    def as_dict(self):
        data = model_to_dict(self)
        data['name'] = self.name
        data['phone_number'] = self.phone_number.as_international if self.phone_number else ''
        return data

    def recognize_user(self):
        users = []
        if self.email:
            users = User.objects.filter(email=self.email)
            if not len(users):
                users = User.objects.filter(additional_emails__email=self.email)

        if len(users) > 0:
            return users[0]

        if self.phone_number:
            users = User.objects.filter(phone_number=self.phone_number)
            if not len(users):
                users = User.objects.filter(additional_phone_numbers__phone_number=self.phone_number)

        if len(users) > 0:
            return users[0]

        return None

    def auto_fill(self):
        user = self.recognize_user()
        if user:
            if not self.last_name and user.last_name:
                self.last_name = user.last_name
                self.auto_filled_name = True

            if not self.first_name and user.first_name:
                self.first_name = user.first_name
                self.auto_filled_name = True

            if not self.phone_number and user.phone_number:
                self.phone_number = user.phone_number
                self.auto_filled_phone_number = True

            if not self.email and user.email:
                self.email = user.email
                self.auto_filled_email = True
            self.recognized_user = user

    def before_save(self):
        self.auto_fill()

    def after_save(self):
        user = self.recognize_user()

        changed = False
        if user:
            if not user.first_name and not self.auto_filled_name:
                changed = True
                user.first_name = self.first_name or ''
            if not user.last_name and not self.auto_filled_name:
                changed = True
                user.last_name = self.last_name or ''
            if not self.auto_filled_phone_number and self.phone_number:
                if not user.phone_number:
                    changed = True
                    user.phone_number = self.phone_number
                elif self.phone_number != user.phone_number:
                    phone, created = Phone.objects.get_or_create(
                        user=user,
                        phone_number=self.phone_number
                    )
                    changed = created
            if not self.auto_filled_email and self.email:
                if not user.email:
                    changed = True
                    user.email = self.email
                elif self.email != user.email:
                    email, created = Email.objects.get_or_create(
                        user=user,
                        email=self.email
                    )
                    changed = created
            user.save()
        else:
            user = User.objects.create(
                first_name=self.first_name or '',
                last_name=self.last_name or '',
                phone_number=self.phone_number,
                email=self.email,
                password=''
            )
            changed = True
        if changed or user.amocrm_id == -1:
            amocrm_sync_user(user.pk)
        if changed:
            unisender_sync_user(user.pk)

    def save(self, **kwargs):
        # We used it because pre_save and post_save not rising from children to parent,
        # also it has no effect if register it for parent
        self.before_save()
        super(Contacts, self).save(**kwargs)
        self.after_save()

    def __str__(self):
        return self.name if self.name else 'Неизвестный контакт'

    class Meta:
        verbose_name = 'контакты'
        verbose_name_plural = 'контакты'


class ProductAlert(Contacts):
    EMAIL_TEMPLATE_CODE = 'PRODUCT_ALERT'
    AMOCRM_SYNC_METHOD_NAME = 'amocrm_sync_product_alert'
    ACTIVE = 'active'

    product = models.ForeignKey(
        'catalogue.Product',
        verbose_name='Товар',
        on_delete=models.CASCADE)

    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)
    count = models.IntegerField(verbose_name='Количество', default=1)

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return ''.join([
            'https://',
            Site.objects.get_current().domain,
            urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        ])

    def as_dict(self):
        data = super().as_dict()
        data['product'] = self.product.as_dict()
        data['date_created'] = self.date_created.strftime('%d.%m.%Y')
        return data

    def __str__(self):
        return 'подписка на товар %s' % self.product

    class Meta:
        app_label = 'customer'
        verbose_name = _('Product alert')
        verbose_name_plural = _('Product alerts')


class Email(models.Model):
    user = models.ForeignKey('customer.User', verbose_name='Пользователь', related_name='additional_emails')
    email = models.EmailField(verbose_name='Электронная почта')

    class Meta:
        verbose_name = 'электронная почта'
        verbose_name_plural = 'электронные почты'


class Phone(models.Model):
    user = models.ForeignKey('customer.User', verbose_name='Пользователь', related_name='additional_phone_numbers')
    phone_number = PhoneNumberField(verbose_name='Телефон')

    class Meta:
        verbose_name = 'номер телефона'
        verbose_name_plural = 'номера телефонов'


from oscar.apps.customer.models import *


class User(AbstractAmoCRMFields, AbstractTelegramUserFields, AbstractUser):
    username_validator = UnicodeUsernameValidator() if six.PY3 else ASCIIUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    email = models.EmailField(_('email address'), blank=True, null=True)
    phone_number = PhoneNumberField(_("Phone number"), blank=True, null=True)
    birthday = models.DateField('День рождения', blank=True, null=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    @property
    def address(self):
        last_order = self.orders.last()
        if last_order:
            return last_order.address

    @staticmethod
    def pre_save(sender, **kwargs):
        instance = kwargs.get('instance')
        if not instance.username:
            if instance.email:
                instance.username = instance.email
            else:
                instance.username = instance.phone_number

    def get_all_phone_numbers(self):
        phones = list(self.additional_phone_numbers.all().values_list('phone_number', flat=True))
        if self.phone_number:
            phones = [str(self.phone_number)] + phones
        return phones

    def get_all_emails(self):
        return [self.email] + list(self.additional_emails.all().values_list('email', flat=True))

    def as_dict(self):
        data = model_to_dict(self)
        data['name'] = self.get_full_name()
        data['phone_number'] = self.phone_number.as_international if self.phone_number else ''
        return data

    def _migrate_alerts_to_user(self):
        pass

    class Meta:
        unique_together = ('email', 'phone_number')
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


pre_save.connect(User.pre_save, sender=User)
