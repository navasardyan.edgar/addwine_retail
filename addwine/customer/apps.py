from django.apps import AppConfig


class CustomerConfig(AppConfig):
    name = 'customer'
    verbose_name = 'Клиенты'

    def ready(self):
        from oscar.apps.customer import receivers
        from . import receivers
