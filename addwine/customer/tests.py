from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse
import random
import string

from unittest import skip


def create_user():
    USERNAME_LENGTH = 8
    username = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(USERNAME_LENGTH))
    u = User(username=username)
    u.set_password('password123')
    return u.save()


class BirthdateFormTest(TestCase):
    def setUp(self):
        self.user = create_user()
    
    def tearDown(self):
        self.client.logout()

    @skip('Needs to mock amo responses')
    def test_birthdate_amoCRM(self):
        self.client.login(username=self.user.username, password=self.user.password)

        url_name = 'birthday-save'
        data = {
            'day': '11',
            'month': '10',
            'year': '1992'
        }
        resp = self.client.post(reverse(url_name), data)
        print(resp)