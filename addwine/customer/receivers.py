from django.dispatch import receiver
from catalogue.models import Product
from catalogue.signals import product_available
from customer.models import ProductAlert, CommunicationEventType
from feedback.views import send_mail


@receiver(product_available, sender=Product)
def send_alerts(sender, **kwargs):
    product = kwargs['product']
    alerts = ProductAlert.objects.filter(product=product)
    if product.is_discountable and product.on_product_available_discount_info.exists():
        product.on_product_available_discount_info.first().create_range_discount()
    if alerts:
        email_template = CommunicationEventType.objects.get(code=ProductAlert.EMAIL_TEMPLATE_CODE)
        for alert in alerts:
            alert_dict = alert.as_dict()
            alert_dict['is_discountable'] = product.is_discountable
            send_mail(email_template, alert_dict, [alert.email, ])
        alerts.delete()
