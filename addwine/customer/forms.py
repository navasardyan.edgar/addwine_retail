from django import forms
from django.core.exceptions import ValidationError

from customer.models import ProductAlert, Contacts, User


class ContactsForm(forms.ModelForm):
    class Meta:
        model = Contacts
        fields = '__all__'


class ProductAlertForm(forms.ModelForm):

    name = forms.CharField(label='Фамилия Имя Отчество', max_length=512, required=False)

    def __init__(self, *args, **kwargs):
        super(ProductAlertForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['phone_number'].required = False

    def clean(self):
        cleaned_data = super(ProductAlertForm, self).clean()

        count = cleaned_data['count']
        email = cleaned_data['email']
        phone_number = cleaned_data['phone_number']
        product = cleaned_data['product']

        try:
            if phone_number:
                ProductAlert.objects.get(product=product, email=email, phone_number=phone_number)
            else:
                ProductAlert.objects.get(product=product, email=email, auto_filled_phone_number=True)
            raise ValidationError(
                    'Invalid value for fields email and phone: %(email)s %(phone_number)s. Values pair must be unique',
                    params={'email': email, 'phone_number': phone_number},
                )
        except ProductAlert.DoesNotExist:
            pass

        return cleaned_data

    class Meta:
        model = ProductAlert
        exclude = ('amocrm_id', 'date_created', )


class BirthdaySaveForm(forms.Form):
    user = forms.ModelChoiceField(queryset=User.objects.all())
    day = forms.IntegerField(min_value=1, max_value=31, widget=forms.TextInput(attrs={}))
    month = forms.IntegerField(min_value=1, max_value=12, widget=forms.TextInput(attrs={}))
    year = forms.IntegerField(min_value=1945, max_value=2100, widget=forms.TextInput(attrs={}))
