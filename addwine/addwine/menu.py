"""
This file was generated with the custommenu management command, it contains
the classes for the admin menu, you can customize this class as you want.

To activate your custom menu add the following to your settings.py::
    ADMIN_TOOLS_MENU = 'addwine.menu.CustomMenu'
"""
from addwine.dashboard import APPS_MODELS_MAP
from products_editor.utils import is_products_editor
from sommeliers.menu import SommelierMenuItem
from sommeliers.models import is_sommelier

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from admin_tools.menu import items, Menu

PRODUCTS_EDITOR_MENU = []

SOMMELIER_MENU = [
    SommelierMenuItem('Все заявки', reverse('admin:sommeliers_sommeliertask_changelist')),
    SommelierMenuItem('Мои заявки', reverse('admin:sommeliers_sommeliertasksubmited_changelist'), only_my=True),
]

ADMIN_MENU = [
    items.MenuItem(
        'Пользователи',
        children=[
            items.MenuItem('Пользователи', reverse('admin:customer_user_changelist')),
            items.MenuItem('Группы', reverse('admin:auth_group_changelist')),
        ]
    ),
    items.MenuItem('Страницы', reverse('admin:common_pageconfigs_changelist')),
    items.MenuItem('О компании', children=[
        items.MenuItem('Описание', reverse('admin:common_aboutcompany_changelist')),
        items.MenuItem('Видео отзывы', reverse('admin:common_videoreviewaboutcompany_changelist')),
        items.MenuItem('Документы', reverse('admin:common_documentaboutcompany_changelist')),
    ]),
    items.MenuItem('Контакты', reverse('admin:common_contacts_changelist')),
    items.MenuItem('Настройки', reverse('admin:common_settings_changelist')),
    items.MenuItem('Система', children=[
        items.MenuItem('Периодические задачи', reverse('admin:django_celery_beat_periodictask_changelist')),
        items.MenuItem('Информация о задачах', reverse('admin:django_celery_results_taskresult_changelist')),
    ]),
    SommelierMenuItem('Сомелье', children=[
        items.MenuItem('Заявки', reverse('admin:sommeliers_sommeliertask_changelist')),
        SommelierMenuItem('Мои заявки', reverse('admin:sommeliers_sommeliertasksubmited_changelist'), only_my=True),
        items.MenuItem('Шаблоны сообщений', reverse('admin:sommelier_bot_telegrammessagetemplate_changelist')),
    ])
]


class CustomMenu(Menu):
    def init_with_context(self, context):
        self.children = [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
            items.Bookmarks(),
        ]
        if is_sommelier(context.request.user):
            self.children += SOMMELIER_MENU
        elif is_products_editor(context.request.user):
            pass
        else:
            self.children += ADMIN_MENU
        return super(CustomMenu, self).init_with_context(context)
