"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'addwine.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'addwine.dashboard.CustomAppIndexDashboard'
"""
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from feedback.dashboard import FeedbackModelList

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name

APPS_MODELS_MAP = {
    'catalogue': [
        'offer.models.RangeGroup',
        'offer.models.RangeInnerCategory',
        'offer.models.Range',
        'catalogue.models.Partner',
        'catalogue.models.Category',
        'catalogue.models.ProductClass',
        'catalogue.models.Product',
        'catalogue.models.FacetOrder',
        'oscar.apps.catalogue.models.AttributeOptionGroup',
        'catalogue.models.Brand'
    ],
    'news': ['news.models.Category', 'news.models.Article'],
    'faq': ['faq.models.FAQCategory', 'faq.models.FAQQuestion'],
    'customer': ['customer.models.User', 'django.contrib.auth.models.Group'],
    'shipping': [
        'shipping.models.ShippingVideoReview', 'shipping.models.OrderAndItemCharges', 'payment.models.PaymentMethod'
    ],
    'marketing': [
        'marketing.models.SliderCTA', 'marketing.models.SommelierCTA', 'marketing.models.SommelierCard','marketing.models.MailCTA',
        'marketing.models.SEOTextCategory', 'marketing.models.SEOText', 'customer.models.CommunicationEventType',
        'customer.models.ProductAlert', 'search.models.QueryString', 'recommendation_emails.models.RecommendationEmails',
    ],
    'voucher': [
            'offer.models.RangeDiscount', 'voucher.models.VoucherGroup',
            'voucher.models.GiftCard', 'offer.models.SubscriptionForDiscount'
    ],
    'feedback': [
        'order.models.Order', 'feedback.models.Question', 'feedback.models.BackCall', 'feedback.models.SliderCTARequest'
    ],
}

DASHBOARD_MODULES = [
    modules.ModelList('Содержимое', models=APPS_MODELS_MAP['news'] + APPS_MODELS_MAP['faq'], deletable=False,
                      collapsible=False),
    modules.ModelList('Каталог', models=APPS_MODELS_MAP['catalogue'], deletable=False, collapsible=False),
    modules.ModelList('Доставка', models=APPS_MODELS_MAP['shipping'], extra=[{
        'title': 'Таблица доставки',
        'change_url': reverse('admin:shipping_shippingtable_change', args=[1, ])
    }], deletable=False, collapsible=False),
    modules.ModelList('Стимулирование продаж', models=APPS_MODELS_MAP['marketing'], extra=[{
        'title': 'Настройки рекомендательных писем',
        'change_url': reverse('admin:recommendation_emails_recommendationemailssettings_change', args=[1, ])
    }], deletable=False, collapsible=False),
    modules.ModelList('Скидочные программы', models=APPS_MODELS_MAP['voucher'], deletable=False, collapsible=False),
    FeedbackModelList('Заявки', models=APPS_MODELS_MAP['feedback'], counter_models=APPS_MODELS_MAP['feedback'],
                      deletable=False, collapsible=False)
]


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for addwine.
    """

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        self.children = DASHBOARD_MODULES


class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for addwine.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        app_name = context.request.path.split('/')[-2]
        model_list = APPS_MODELS_MAP.get(app_name)
        if model_list:
            self.children = modules.ModelList(title=self.app_title, models=model_list)

        return super(CustomAppIndexDashboard, self).init_with_context(context)
