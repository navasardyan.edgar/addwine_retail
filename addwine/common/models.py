# coding=utf-8
import re

from ckeditor.fields import RichTextField
from django.db import models
from embed_video.fields import EmbedVideoField

from marketing.models import MailCTA, SEOText
from seo.models import AbstractSeoConfigs
from site_configs.models import SingletonModel
from django.conf import settings


class SiteConfiguration(SingletonModel):
    email = models.EmailField(verbose_name="почта", default='', blank=True)
    phone = models.CharField(verbose_name="номер телефона", max_length=30, default='', blank=True)
    whatsapp = models.CharField(verbose_name='номер WhatsApp', max_length=30, blank=True)
    address = models.CharField(verbose_name='адрес', max_length=1024, default='', blank=True)
    short_address = models.CharField(verbose_name='Короткий адрес', max_length=1024, default='', blank=True)
    gps_lat = models.FloatField(verbose_name='Широта', default=0, blank=True)
    gps_lng = models.FloatField(verbose_name='Долгота', default=0, blank=True)
    ogrnip = models.CharField(verbose_name='ОГРНИП', default='', max_length=25, blank=True)
    inn = models.CharField(verbose_name='ИНН', default='', max_length=25,  blank=True)
    legal_information = RichTextField(verbose_name='Юридическая информация', default='', blank=True)
    networks_facebook = models.URLField(verbose_name="ссылка на Facebook", default='', blank=True)
    networks_instagram = models.URLField(verbose_name="ссылка на Instagram", default='', blank=True)
    networks_youtube = models.URLField(verbose_name="ссылка на Youtube", default='', blank=True)
    networks_instagram_token = models.CharField(verbose_name="токен Instagram", max_length=1024, default='', blank=True)

    company_story = RichTextField(verbose_name='история компании', default='', blank=True)
    rules_pay_and_delivery = RichTextField(verbose_name='оплата и доставка', default='', blank=True)
    map_icon = models.FileField(verbose_name='Метка на карте', null=True, blank=True, upload_to='map_icons/')
    probich_image = models.FileField(upload_to='probich/', verbose_name='изображение для модального окна доставки', null=True, blank=True)
    delivery_info = RichTextField(verbose_name='модальное окно доставки', default='', blank=True)
    rules_return = RichTextField(verbose_name='условия возврата', default='', blank=True)
    rules_guarantee = RichTextField(verbose_name='гарантия', default='', blank=True)
    rules_confidential = RichTextField(verbose_name='политика конфиденциальности', default='', blank=True)
    rules_personal = RichTextField(verbose_name='согласие на обработку персональных данных', default='', blank=True)
    rules_offert = RichTextField(verbose_name='договор оферта', default='', blank=True)

    yandex_market_shop_name = models.CharField(verbose_name="краткое название магазина",
                                               default='', blank=True, max_length=20)
    yandex_market_shop_company = models.CharField(verbose_name="полное название магазина/компании",
                                                  default='', blank=True, max_length=512)
    telegram_test_channel_id = models.CharField(verbose_name="ID тестового канала в Telegram", max_length=255,
                                                default=False,
                                                blank=True)
    telegram_channel_id = models.CharField(verbose_name="ID канала в Telegram", max_length=255, default=False,
                                           blank=True)
    send_notify_on_mail = models.BooleanField(verbose_name="высылать уведомления на почту", default=False, blank=True)
    maintenance_mode = models.BooleanField(verbose_name="режим технической поддержки", default=False, blank=True)

    window_inform_active = models.BooleanField(verbose_name='активно', default=False)
    window_inform_image = models.ImageField(verbose_name='изображение')
    window_inform_text = RichTextField(verbose_name='основной текст', blank=True, default='')
    window_inform_text_special = RichTextField(verbose_name='выделенный текст', blank=True, default='')
    window_inform_text_bottom = RichTextField(verbose_name='текст в подвале', blank=True, default='')

    def get_raw_phone(self):
        # Phone in format +7999232131
        if getattr(settings, 'TEST_MODE_FOR_PHONE', False):
            return '+7999232131'
        return self.phone[0] + re.sub("\D", "", self.phone[1:])

    def __str__(self):
        return "Настройки"

    class Meta:
        verbose_name = "настройки"


class Settings(SiteConfiguration):
    class Meta:
        verbose_name = 'настройки'
        verbose_name_plural = 'настройки'
        proxy = True


class AboutCompany(SiteConfiguration):
    class Meta:
        verbose_name = 'о компании'
        verbose_name_plural = 'о компании'
        proxy = True


class Contacts(SiteConfiguration):
    class Meta:
        verbose_name = 'контакты'
        verbose_name_plural = 'контакты'
        proxy = True


class VideoReviewAboutCompany(models.Model):
    video = EmbedVideoField(verbose_name='видео')
    title = models.CharField(verbose_name='заголовок', max_length=255)
    subtitle = models.CharField(verbose_name='подзаголовок', max_length=255)

    class Meta:
        verbose_name = 'видео отзыв'
        verbose_name_plural = 'видео отзывы'


class DocumentAboutCompany(models.Model):
    title = models.CharField(verbose_name='заголовок', max_length=255)
    doc = models.ImageField(verbose_name='файл', max_length=255)

    class Meta:
        verbose_name = 'документ'
        verbose_name_plural = 'документы'


class PageConfigs(AbstractSeoConfigs):
    page_code = models.CharField('код страницы', max_length=255, unique=True)
    mail_cta = models.ForeignKey(MailCTA, verbose_name="призыв к действию: рассылка", null=True, blank=True,
                                 on_delete=models.SET_NULL)

    def __str__(self):
        return 'настройки для страницы с кодом %s' % self.page_code

    class Meta:
        verbose_name = 'статичная страница'
        verbose_name_plural = 'статичные страницы'


class PageConfigsSEOText(models.Model):
    page_configs = models.ForeignKey(PageConfigs, verbose_name='страница',
                                     on_delete=models.CASCADE, related_name='seo_texts')
    seo_text = models.ForeignKey(SEOText, verbose_name='SEO тект', related_name='page_configs')

    def __str__(self):
        return 'SEO текст для страницы %s' % self.page_configs.page_code

    class Meta:
        verbose_name = 'SEO текст'
        verbose_name_plural = 'SEO тексты'


class WorkTimeInterval(models.Model):
    DAY_OF_THE_WEEK = (
        (1, 'пн'),
        (2, 'вт'),
        (3, 'ср'),
        (4, 'чт'),
        (5, 'пт'),
        (6, 'сб'),
        (7, 'вс'),
    )

    DAY_OF_THE_WEEK_FULL_NAMES = [
        'понедельник',
        'вторник',
        'среда',
        'четверг',
        'пятница',
        'суббота',
        'воскресенье'
    ]

    configs = models.ForeignKey(SiteConfiguration, verbose_name='конфигураци', related_name='work_times')
    start_day_of_week = models.SmallIntegerField(verbose_name='начало: день недели', choices=DAY_OF_THE_WEEK, null=True,
                                                 blank=True, default=0)
    end_day_of_week = models.SmallIntegerField(verbose_name='конец: день недели', choices=DAY_OF_THE_WEEK, null=True,
                                               blank=True, default=0)
    start_work_time = models.TimeField(verbose_name='время начала работы', null=True, blank=True)
    end_work_time = models.TimeField(verbose_name='время конца работы', null=True, blank=True)

    @property
    def start_day_of_week_full_name(self):
        return self.DAY_OF_THE_WEEK_FULL_NAMES[self.start_day_of_week-1] if self.start_work_time is not None else None

    @property
    def end_day_of_week_full_name(self):
        return self.DAY_OF_THE_WEEK_FULL_NAMES[self.end_day_of_week-1] if self.end_day_of_week is not None else None

    @property
    def weekend(self):
        return self.start_work_time is None and self.end_work_time is None

    @property
    def start_work_time_hours(self):
        return self.start_work_time.strftime('%H') if self.start_work_time else None

    @property
    def start_work_time_minutes(self):
        return self.start_work_time.strftime('%M') if self.start_work_time else None

    @property
    def end_work_time_hours(self):
        return self.end_work_time.strftime('%H') if self.end_work_time else None

    @property
    def end_work_time_minutes(self):
        return self.end_work_time.strftime('%M') if self.end_work_time else None

    def __str__(self):
        return 'врем работы %s - %s %s:%s' % (self.get_start_day_of_week_display(), self.get_end_day_of_week_display(),
                                              self.start_work_time, self.end_work_time)

    class Meta:
        verbose_name = 'Время работы'
        verbose_name_plural = 'Время работы'
