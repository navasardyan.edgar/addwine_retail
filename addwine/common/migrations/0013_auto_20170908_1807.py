# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-08 15:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0012_auto_20170908_1617'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pageconfigs',
            name='seo_description',
            field=models.CharField(blank=True, max_length=100, verbose_name='описание (description)'),
        ),
    ]
