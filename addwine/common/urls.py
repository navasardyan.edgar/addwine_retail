from django.conf import settings
from django.conf.urls import url

from common.views import robots
from django.contrib.sitemaps.views import sitemap
sitemaps = {}

for app in settings.INSTALLED_APPS:
    try:
        module = __import__(app+".sitemaps", fromlist=[''])
    except:
        continue
    module_sitemap = getattr(module, 'sitemaps', None)
    if module_sitemap:
        sitemaps.update(module_sitemap)

urlpatterns = [
    url(r'robots.txt$', robots),
    url(r'sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
]
