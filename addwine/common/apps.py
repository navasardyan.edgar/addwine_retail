# coding=utf-8
from django.apps import AppConfig


class CommonConfig(AppConfig):
    name = 'common'
    verbose_name = u"Общие"
    verbose_name_plural = u"Общие"
