from decimal import Decimal as D

from cacheops import cached_as
from oscar.apps.partner import strategy
from oscar.apps.partner.availability import Unavailable, Available
from oscar.apps.partner.strategy import UnavailablePrice

from offer.models import RangeDiscount, Range
from partner.prices import PriceWithRangeDiscount


class Selector(object):
    def strategy(self, request=None, user=None, **kwargs):
        return Default()


class NoTaxWithRangeDiscount(object):

    def pricing_policy(self, product, stockrecord):
        # Check stockrecord has the appropriate data
        if not stockrecord or stockrecord.price_excl_tax is None:
            return UnavailablePrice()
        return PriceWithRangeDiscount(
            currency=stockrecord.price_currency,
            excl_tax=stockrecord.price_excl_tax,
            retail=stockrecord.price_retail,
            product=product,
            tax=D('0.00')
        )

    def parent_pricing_policy(self, product, children_stock):
        stockrecords = [x[1] for x in children_stock if x[1] is not None]
        if not stockrecords:
            return UnavailablePrice()
        # We take price from first record
        stockrecord = stockrecords[0]
        return PriceWithRangeDiscount(
            currency=stockrecord.price_currency,
            excl_tax=stockrecord.price_excl_tax,
            retail = stockrecord.price_retail,
            product=product,
            tax=D('0.00')
        )


class FieldBasedRequired(object):
    def availability_policy(self, product, stockrecord):
        if product.is_available:
            return Available()
        else:
            return Unavailable()

    def parent_availability_policy(self, product, children_stock):
        # A parent product is available if one of its children is
        for child, stockrecord in children_stock:
            policy = self.availability_policy(product, stockrecord)
            if policy.is_available_to_buy:
                return Available()
        return Unavailable()


class Default(strategy.UseFirstStockRecord, NoTaxWithRangeDiscount, FieldBasedRequired, strategy.Structured):
    pass
