from django.db import models
from oscar.apps.partner import abstract_models as partner_models
from django.utils.translation import ugettext_lazy as _
from oscar.core.utils import get_default_currency


class StockRecord(partner_models.AbstractStockRecord):
    partner = None
    partner_sku = None
    cost_price = None
    num_in_stock = None
    num_allocated = None
    low_stock_threshold = None

    price_retail = models.DecimalField(
        _("Price (retail)"), decimal_places=2, max_digits=12, default=0)

    def __str__(self):
        return "Товар: %s" % (self.product,)

    class Meta:
        app_label = 'partner'
        indexes = [
            models.Index(fields=['price_excl_tax']),
        ]
        verbose_name = _("Stock record")
        verbose_name_plural = _("Stock records")



from oscar.apps.partner.models import *
