from django.conf import settings
from ratelimit import limits

from yandex_market.tasks import update_catalog, update_google_merchant


TEN_MINUTES = 600


@limits(calls=1, period=TEN_MINUTES)
def yandex_market_update_catalog():
    if settings.YANDEX_MARKET_ENABLED:
        update_catalog.apply_async(countdown=1)


@limits(calls=1, period=TEN_MINUTES)
def google_merchant_update_feed():
    if settings.GOOGLE_MERCHANT_ENABLED:
        update_google_merchant.apply_async(countdown=1)
