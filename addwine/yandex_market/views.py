import os
from pathlib import Path

from django.http import HttpResponse
from django.views import View
import shutil

from yandex_market import settings


class YandexMarketExporter(View):
    template_name = 'yandex_market/market_exporter.html'

    def update_from_tmp_if_exists(self):
        tmp_filepath = Path(settings.CATALOG_FILE_PATH_TMP)
        if tmp_filepath.is_file():
            shutil.copyfile(settings.CATALOG_FILE_PATH_TMP, settings.CATALOG_FILE_PATH)
            os.remove(settings.CATALOG_FILE_PATH_TMP)

    def get(self, request, *args, **kwargs):
        self.update_from_tmp_if_exists()
        with open(settings.CATALOG_FILE_PATH, "r", encoding="utf-8") as fsock:
            return HttpResponse(fsock, content_type='application/xml')


class GoogleMerchantExporter(View):
    def update_from_tmp_if_exists(self):
        tmp_filepath = Path(settings.GOOGLE_MERCHANT_FILE_PATH_TMP)
        if tmp_filepath.is_file():
            shutil.copyfile(settings.GOOGLE_MERCHANT_FILE_PATH_TMP, settings.GOOGLE_MERCHANT_FILE_PATH)
            os.remove(settings.GOOGLE_MERCHANT_FILE_PATH_TMP)

    def get(self, request, *args, **kwargs):
        self.update_from_tmp_if_exists()
        with open(settings.GOOGLE_MERCHANT_FILE_PATH, "r", encoding="utf-8") as fsock:
            return HttpResponse(fsock, content_type='application/xml')