from django.conf.urls import url

from yandex_market.views import YandexMarketExporter, GoogleMerchantExporter

urlpatterns = [
    url(r'yandex_api_market$',
        YandexMarketExporter.as_view(),
        name='yandex_market_export'
    ),
    url(r'google_merchant_feed$',
        GoogleMerchantExporter.as_view(),
        name='google_merchant_export'
    ),
]



