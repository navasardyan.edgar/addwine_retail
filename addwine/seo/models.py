from django.db import models


class AbstractSeoConfigs(models.Model):
    seo_title = models.CharField('заголовок страницы (title)', max_length=60, blank=True)
    seo_description = models.TextField('описание (description)', max_length=100, blank=True)
    seo_keywords = models.CharField('ключевые слова (keywords)', max_length=1024, blank=True)

    seo_og_title = models.CharField('заголовок в соц. сетях', max_length=60, blank=True)
    seo_og_description = models.TextField('описание в соц. сетях ', max_length=100, blank=True)
    seo_og_image = models.ImageField('превью для соц. сетей', blank=True)

    class Meta:
        abstract = True
