import json

from celery import shared_task
from celery.exceptions import MaxRetriesExceededError
from celery.utils.log import get_task_logger
from django.conf import settings
from django.apps import apps
from phonenumber_field.phonenumber import PhoneNumber

from unisender_api.api.pyunisend import PyUniSend

logger = get_task_logger(__name__)

TASKS_CONFIGS_API_REQUEST = {
    'ignore_result': True,
    'default_retry_delay': 1,
    'retry_kwargs': {'max_retries': 5},
}

TASKS_CONFIGS_SYNC_CONTACT = {
    'name': "unisender.sync_contact",
}
TASKS_CONFIGS_SYNC_CONTACT.update(TASKS_CONFIGS_API_REQUEST)
TASKS_CONFIGS_SYNC_CONTACT.update(settings.UNISENDER_TASK_CONFIG)


def get_not_sync_data(api, emails, phones, birthday):
    resp = api.exportContacts(list_id=settings.UNISENDER_EMAIL_LIST_ID, field_names=['email', 'phone', 'Name', 'family', 'dr'])

    if api.errorMessage or api.errorCode:
        return None, None

    pack_size = settings.UNISENDER_CONTACTS_EXPORT_LIMIT
    max_steps_count = settings.UNISENDER_CONTACTS_EXPORT_SYNC_LIMIT / pack_size

    step = 1
    try_more = len(resp['data']) > 0

    while try_more and step < max_steps_count:
        temp_resp = api.exportContacts(list_id=settings.UNISENDER_EMAIL_LIST_ID, field_names=['email', 'phone', 'Name', 'family', 'dr'],
                                       offset=pack_size * step)
        if api.errorCode or api.errorMessage:
            break
        resp['data'].extend(temp_resp['data'])
        try_more = len(temp_resp['data']) > 0
        step += 1

    field_names = resp['field_names']

    email_index = field_names.index('email')
    phone_index = field_names.index('phone')
    name_index = field_names.index('Name')
    surname_index = field_names.index('family')
    birthday_index = field_names.index('dr')

    updatable_emails = {}
    unisender_emails = []
    new_phones = phones.copy()

    for data in resp['data']:
        contact_email = data[email_index]
        contact_phone = data[phone_index]
        contact_name = data[name_index]
        contact_surname = data[surname_index]
        contact_birthday = data[birthday_index]

        unisender_emails.append(contact_email)

        try:
            phone = str(PhoneNumber.from_string(contact_phone))
            new_phones.remove(phone)
        except:
            pass

        if contact_email not in emails:
            continue

        updatable_fields = {}
        if contact_phone:
            updatable_fields[field_names[phone_index]] = contact_phone

        if contact_name:
            updatable_fields[field_names[name_index]] = contact_name

        if contact_surname:
            updatable_fields[field_names[surname_index]] = contact_surname

        if contact_birthday == birthday:
            updatable_fields[field_names[birthday_index]] = contact_birthday

        if len(updatable_fields.keys()) != len(field_names) - 1:
            updatable_emails[contact_email] = updatable_fields

    for email in emails:
        if email not in unisender_emails:
            updatable_emails[email] = {}

    return updatable_emails, new_phones


@shared_task(**TASKS_CONFIGS_SYNC_CONTACT)
def sync_contact_task(user_id):
    user_model = apps.get_model('customer', 'User')

    try:
        instance = user_model.objects.get(id=user_id)
    except user_model.DoesNotExist:
        logger.error('Trying to sync contact of not existing User.')
        return

    if not instance.email:
        return

    api = PyUniSend(settings.UNISENDER_API_KEY)

    phones = instance.get_all_phone_numbers()
    emails = instance.get_all_emails()
    birthday = str(instance.birthday) if instance.birthday else ''
    not_sync_emails, new_phones = get_not_sync_data(api, emails, phones, birthday)

    if not_sync_emails is None:
        logger.error(
            'Unable to sync contact for User with ID %s. Cause: "%s" Code: "%s" '
            % (instance.id, api.errorMessage, api.errorCode)
        )

    if not not_sync_emails:
        return

    def get_free_phone():
        try:
            return new_phones.pop(0)
        except:
            return ''

    import_field_names = ['email', 'phone', 'Name', 'family', 'dr', 'email_list_ids']
    import_data = []
    for email, fields in not_sync_emails.items():
        import_data.append(
            [
                email,
                fields.get('phone', get_free_phone()),
                fields.get('Name', instance.first_name),
                fields.get('family', instance.last_name),
                birthday,
                [settings.UNISENDER_EMAIL_LIST_ID],
            ]
        )

    try:
        resp = api.importContacts(field_names=import_field_names, data=import_data)
        if api.errorMessage or api.errorCode:
            logger.error(
                'Unable to sync contact for User with ID %s. Cause: "%s" Code: "%s" '
                % (instance.id, api.errorMessage, api.errorCode)
            )
    except:
        # If something get wrong just catch it and then unlock model
        logger.exception("Unexpected error")
        resp = None

    if resp:
        return instance

    try:
        retry_kwargs = TASKS_CONFIGS_API_REQUEST['retry_kwargs'].copy()
        retry_kwargs['countdown'] = TASKS_CONFIGS_API_REQUEST['default_retry_delay'] * sync_contact_task.request.retries
        sync_contact_task.retry(**retry_kwargs)
    except MaxRetriesExceededError as e:
        logger.exception("Unable to sync User with id %s" % (instance.id,))
        return None
