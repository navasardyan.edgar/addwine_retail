from django.apps import AppConfig


class UnisenderApiConfig(AppConfig):
    name = 'unisender_api'
