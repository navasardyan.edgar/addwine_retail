from django.dispatch import Signal


product_available = Signal(providing_args=['product'])
product_saved_related = Signal(providing_args=['product'])
