from decimal import Decimal

import xlrd
from django.template.defaultfilters import slugify

from catalogue.models import ProductClass, ProductCategory, Partner, Brand, Product, Category, \
    OnProductAvailableDiscountInfo
from partner.models import StockRecord
import logging

logger = logging.getLogger(__name__)

NAME_MAPPING = {
    'тип товара': {
        'key': 'product_class',
        'type': 'processor'
    },
    'бренд': {
        'key': 'brand',
        'type': 'processor'
    },
    'категория': {
        'key': 'category',
        'type': 'processor'
    },
    'контрагент': {
        'key': 'partner',
        'type': 'processor'
    },
    'артикул': {
        'key': 'upc',
        'type': str
    },
    'наименование': {
        'key': 'title',
        'type': str
    },
    'имя в url': {
        'key': 'slug',
        'type': str
    },
    'описание': {
        'key': 'description',
        'type': str
    },
    'запрещено к дп': {
        'key': 'forbidden_for_delivery',
        'type': bool
    },
    'показывать на сайте': {
        'key': 'is_active',
        'type': bool
    },
    'имеется в наличии': {
        'key': 'is_available',
        'type': bool
    },
    'скидки действуют': {
        'key': 'is_discountable',
        'type': bool
    },
    'количество': {
        'key': 'count',
        'type': 'processor'
    },
    'цена': {
        'key': 'price',
        'type': 'processor'
    },
    'себестоимость': {
        'key': 'price_retail',
        'type': 'processor'
    },
    'cкидка: тип': {
        'key': 'on_product_available_discount_type',
        'type': 'processor'
    },
    'скидка: величина': {
        'key': 'on_product_available_discount_value',
        'type': 'processor'
    },
    'скидка: время действия': {
        'key': 'on_product_available_discount_time',
        'type': 'processor'
    },

    'seo: заголовок': {
        'key': 'seo_title',
        'type': str
    },
    'seo: описание': {
        'key': 'seo_description',
        'type': str
    },
    'seo: keywords': {
        'key': 'seo_keywords',
        'type': str
    }
}

DEFAULT_VALUES = {
    'is_available': True,
    'forbidden_for_delivery': False,
    'is_active': False,
    'on_product_available_discount_type': '%'
}

REQUIRED_FIELDS_ADD = [
    'product_class',
    'brand',
    'category',
    'upc',
    'title',
    'description',
    'price'
]

REQUIRED_FIELDS_UPDATE = [
    'upc',
    'count',
    'price'
]


def product_class_processor(obj, field_map, process_context):
    product_class_name = field_map['product_class']['value']
    product_class, created = ProductClass.objects.get_or_create(name__iexact=product_class_name, defaults={
        'name': product_class_name,
        'slug': slugify(product_class_name)
    })
    obj.product_class = product_class


def brand_processor(obj, field_map, process_context):
    brand_name = field_map['brand']['value']
    brand, created = Brand.objects.get_or_create(title__iexact=brand_name, defaults={
        'title': brand_name,
        'slug': slugify(brand_name)
    })
    obj.brand = brand


def category_processor(obj, field_map, process_context):
    category_full_name = field_map['category']['value'].strip()
    categories_names = category_full_name.split('/')

    current_context = process_context.get('categories')
    if not current_context:
        process_context['categories'] = current_context = {}

    category = None
    categories_manager = Category.objects
    for category_name in categories_names:
        key = category_name.strip().lower()
        category_data = current_context.get(key)
        if not category_data:
            try:
                category = categories_manager.get(name__iexact=category_name)
            except Category.DoesNotExist:
                if category:
                    category = category.add_child(name=category_name)
                else:
                    category = Category.add_root(name=category_name)

            categories_manager = category.get_children()

            current_context[key] = category_data = {
                'category': category,
                'child': {}
            }

        category = category_data['category']
        current_context = category_data['child']

    if not category:
        return

    ProductCategory.objects.get_or_create(
        product=obj,
        category=category
    )


def partner_processor(obj, field_map, process_context):
    partner_name = field_map.get('partner')
    if not partner_name:
        return
    partner_name = partner_name['value']

    partner, created = Partner.objects.get_or_create(name__iexact=partner_name, defaults={
        'name': partner_name
    })
    obj.partner = partner


def price_processor(obj, field_map, process_context):
    data = {
        'price_excl_tax': Decimal(field_map['price']['value'])
    }
    if field_map.get('price_retail'):
        data['price_retail'] = field_map['price_retail']['value']

    stock_record, created = StockRecord.objects.get_or_create(
        product=obj,
        defaults=data
    )

    if not created:
        stock_record.price_excl_tax = Decimal(data['price_excl_tax'])
        if data.get('price_retail'):
            stock_record.price_retail = Decimal(data['price_retail'])
        stock_record.save()


def discount_processor(obj, field_map, process_context):
    discount_type = field_map.get('on_product_available_discount_type',
                                  {'value': DEFAULT_VALUES['on_product_available_discount_type']})
    discount_value = field_map.get('on_product_available_discount_value')
    discount_time = field_map.get('on_product_available_discount_time')

    discount_type = discount_type['value']
    discount_type = OnProductAvailableDiscountInfo.PERCENTAGE if discount_type == '%' else OnProductAvailableDiscountInfo.FIXED
    if not discount_value or not discount_time:
        return

    discount_value = discount_value['value']
    discount_time = discount_time['value']

    discount, created = OnProductAvailableDiscountInfo.objects.get_or_create(
        product=obj,
        defaults={
            'type': discount_type,
            'value': discount_value,
            'hours_count': discount_time
        }
    )

    if not created:
        discount.type = discount_type
        discount.value = discount_value
        discount.hours_count = discount_time
        discount.save()


def count_processor(obj, field_map, process_context):
    if 'count' not in field_map:
        return
    count = field_map['count']['value']
    obj.is_available = count > 0


PREPROCESSORS_ADD = [
    product_class_processor,
    brand_processor,
    partner_processor,
    count_processor
]

PROCESSORS_ADD = [
    category_processor,
    price_processor,
    discount_processor
]


def __process_row_add_or_update(data, process_context):
    created = False
    upc = data['upc']

    # Trying to convert decimal value to int to remove 0 on the end of string
    try:
        upc['value'] = int(upc['value'])
    except:
        pass

    try:
        obj = Product.objects.get(upc=upc['type'](upc['value']))
    except Product.DoesNotExist:
        created = True
        obj = Product()

    for field_name, field_data in data.items():
        if field_data['type'] == 'processor':
            continue

        if field_data['type'] == bool:
            field_data['value'] = str(field_data['value']).strip().lower() == 'да'

        setattr(obj, field_name, field_data['type'](field_data['value']))

    for default_field, default_value in DEFAULT_VALUES.items():
        if not data.get(default_field) and created:
            setattr(obj, default_field, default_value)

    if not data.get('slug'):
        obj.slug = slugify(obj.title)

    for processor in PREPROCESSORS_ADD:
        processor(obj, data, process_context)

    obj.save()

    for processor in PROCESSORS_ADD:
        processor(obj, data, process_context)

    return created


def __process_row_update(data, process_context):
    upc = data['upc']

    # Trying to convert decimal value to int to remove 0 on the end of string
    try:
        upc['value'] = int(upc['value'])
    except:
        pass

    obj = Product.objects.get(upc=upc['type'](upc['value']))
    count_processor(obj, data, process_context)
    price_processor(obj, data, process_context)
    obj.save()

    return False


def __make_dict_from_row(row, field_names):
    data = {}
    for index, field_data in enumerate(field_names):
        value = row[index]
        value = value if value is not None else DEFAULT_VALUES.get(field_data['key'])
        if value is not None:
            data[field_data['key']] = {
                'value': value,
                'type': field_data['type']
            }
    return data


def __process_sheet(row_processor, sheet, field_names, process_context, start_index=0):
    stats = {
        'added': 0,
        'updated': 0,
        'errors': 0,
    }

    row_count = sheet.nrows
    for row_index in range(start_index, row_count):
        row = sheet.row_values(row_index)
        try:
            created = row_processor(__make_dict_from_row(row, field_names), process_context)
            if created:
                stats['added'] += 1
            else:
                stats['updated'] += 1
        except Product.DoesNotExist:
            logger.warning('Product does not exists ' + str(row))
        except Exception:
            logger.exception('Error during importing data from excel')
            stats['errors'] += 1

    return stats


def field_name_to_literal(field_name):
    for literal, field_data in NAME_MAPPING.items():
        if field_data['key'] == field_name:
            return literal
    return None


def add_or_update_products_from_excel(file):
    doc = xlrd.open_workbook(file_contents=file.read())

    names = doc.sheet_by_index(0).row(0)
    field_names = []
    key_fields_names = set()
    missed_fields = []

    for name in names:
        name = name.value.lower().strip()
        name = ' '.join(name.split())
        field_data = NAME_MAPPING.get(name)
        if field_data:
            field_names.append(field_data)
            key_fields_names.add(field_data['key'])

    for required_field in REQUIRED_FIELDS_ADD:
        if required_field not in key_fields_names:
            missed_fields.append(required_field)

    full_stats = {
        'counter': {
            'added': 0,
            'updated': 0,
            'errors': 0,
        }
    }

    if missed_fields:
        full_stats['missed_fields'] = [field_name_to_literal(missed_field) for missed_field in missed_fields]
        return full_stats

    stats = __process_sheet(__process_row_add_or_update, doc.sheet_by_index(0), field_names, {}, 1)
    full_stats['counter']['added'] += stats['added']
    full_stats['counter']['updated'] += stats['updated']
    full_stats['counter']['errors'] += stats['errors']

    for sheet in doc.sheets()[1:]:
        stats = __process_sheet(__process_row_add_or_update, sheet, field_names, {})
        full_stats['counter']['added'] += stats['added']
        full_stats['counter']['updated'] += stats['updated']
        full_stats['counter']['errors'] += stats['errors']

    return full_stats


def update_products_from_excel(file):
    doc = xlrd.open_workbook(file_contents=file.read())

    names = doc.sheet_by_index(0).row(0)
    field_names = []
    key_fields_names = set()
    missed_fields = []

    for name in names:
        name = name.value.lower().strip()
        name = ' '.join(name.split())
        field_data = NAME_MAPPING.get(name)
        if field_data:
            field_names.append(field_data)
            key_fields_names.add(field_data['key'])

    for required_field in REQUIRED_FIELDS_UPDATE:
        if required_field not in key_fields_names:
            missed_fields.append(required_field)

    full_stats = {
        'counter': {
            'added': 0,
            'updated': 0,
            'errors': 0,
        }
    }

    if missed_fields:
        full_stats['missed_fields'] = [field_name_to_literal(missed_field) for missed_field in missed_fields]
        return full_stats

    stats = __process_sheet(__process_row_update, doc.sheet_by_index(0), field_names, {}, 1)
    full_stats['counter']['added'] += stats['added']
    full_stats['counter']['updated'] += stats['updated']
    full_stats['counter']['errors'] += stats['errors']

    for sheet in doc.sheets()[1:]:
        stats = __process_sheet(__process_row_update, sheet, field_names, {})
        full_stats['counter']['added'] += stats['added']
        full_stats['counter']['updated'] += stats['updated']
        full_stats['counter']['errors'] += stats['errors']

    return full_stats
