from django.conf import settings
from django.conf.urls import url

from catalogue.models import Product, Category, ProductClass, Brand, Partner
from common.views import get_admin_autocomplete_view
from .webhook import MoyskladWebhookView

urlpatterns = [
    url(r'autocomplete/catalogue/products$', get_admin_autocomplete_view(Product, 'title').as_view()
        , name='autocomplete_product'),
    url(r'autocomplete/catalogue/categories$', get_admin_autocomplete_view(Category, 'name').as_view()
        , name='autocomplete_category'),
    url(r'autocomplete/catalogue/product_classes', get_admin_autocomplete_view(ProductClass, 'name').as_view()
        , name='autocomplete_product_class'),
    url(r'autocomplete/catalogue/brands', get_admin_autocomplete_view(Brand, 'title').as_view()
        , name='autocomplete_brand'),
    url(r'autocomplete/catalogue/partners', get_admin_autocomplete_view(Partner, 'name').as_view()
        , name='autocomplete_partner'),
    # full url will be utils/catalogue/webhook/3hdf90t4jg
    url(r'webhook/3hdf90t4jg', MoyskladWebhookView.as_view(), name='moysklad_webhook'),
]
