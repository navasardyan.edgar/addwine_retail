import requests
from django.conf import settings
from requests.auth import HTTPBasicAuth
import logging
import simplejson as json

logger = logging.getLogger(__name__)


class BaseApi:
    ENTITY_URL_TEMPLATE = ''
    URL = 'https://' + settings.MOYSKLAD_DOMAIN + '/'+'api/remap/1.1/entity/'

    username = settings.MOYSKLAD_LOGIN
    password = settings.MOYSKLAD_PASSWORD

    def auth(self):
        session = requests.session()
        session.auth = (self.username, self.password)
        return session

    def get(self, url):
        session = self.auth()
        try:
            resp = session.get(url)

            if resp.status_code != 200:
                print('Request failed with status %s. Response %s' % (resp.status_code, resp.text))
                logger.error('Request failed with status %s. Response %s' % (resp.status_code, resp.text))
                return None
            return resp

        except requests.exceptions.RequestException as e:
            logger.exception('Request for MoySklad failed: \n %s' % e)
            return None

    def post(self, url, data):
        session = self.auth()
        try:
            resp = session.post(url, json=data)

            if resp.status_code != 200:
                logger.error('Request failed with status %s. Response %s' % (resp.status_code, resp.text))
                print('Request failed with status %s. Response %s' % (resp.status_code, resp.text))
                return None
            return resp

        except requests.exceptions.RequestException as e:
            logger.exception('Request for MoySklad failed: \n %s' % e)
            return None

    def put(self, url, data):
        session = self.auth()
        try:
            resp = session.put(url, json=data)

            if resp.status_code != 200:
                logger.error('Request failed with status %s. Response %s' % (resp.status_code, resp.text))
                print('Request failed with status %s. Response %s' % (resp.status_code, resp.text))
                return None
            return resp

        except requests.exceptions.RequestException as e:
            logger.exception('Request for MoySklad failed: \n %s' % e)
            return None

