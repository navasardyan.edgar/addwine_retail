from ratelimit import RateLimitException

from .base import BaseApi, logger
from django.core.cache import cache
from catalogue.models import Product
from yandex_market.utils import yandex_market_update_catalog, google_merchant_update_feed


class MoysklAssortmentAPI(BaseApi):
    ENTITY_URL_TEMPLATE = 'assortment?scope=product&offset={offset}&limit={limit}'
    CACHES_TIMEOUT = 60*60*4  # 4 hours

    def build_url(self, offset, limit=100):
        return self.URL + self.ENTITY_URL_TEMPLATE.format(offset=offset, limit=limit)

    def get_page_assortiment(self, offset, limit=100):
        resp = self.get(self.build_url(offset, limit))
        if not resp:
            return None
        resp = resp.json()
        size, offset, limit = (resp['meta']['size'],
                               resp['meta']['offset'],
                               resp['meta']['limit'])
        rows = resp['rows']
        offset += limit
        return rows, size, offset, limit

    def build_key(self, article):
        return 'product_article_{}'.format(article)

    def get_or_change(self, article, new_value, force=False):
        old_value = cache.get(self.build_key(article))
        cache.set(self.build_key(article), new_value, self.CACHES_TIMEOUT)
        if old_value != new_value or force:
            value = int(new_value) if new_value >= 0 else 0
            Product.objects.filter(upc=article).update(quantity=value)

    def scan_assortiment(self, force=False):
        # start values/ size=300 means what quantity of products in moysklad less then 300
        size, offset, limit = 300, 0, 100
        while offset < size:
            rows, size, offset, limit = self.get_page_assortiment(offset, limit)
            for row in rows:
                article = row.get('article')
                if not article:
                    continue

                try:
                    stock = row['stock']
                    self.get_or_change(article, stock, force)
                except KeyError:
                    logger.warning('Get row from moysklad without stock. Article: {article}'.format(article=article))
        try:
            yandex_market_update_catalog()
            google_merchant_update_feed()
        except RateLimitException:
            pass









