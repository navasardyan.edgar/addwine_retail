from .group import MoyskladGroupAPI
import os
import base64


def build_base64_dict(img) -> dict:
    image = getattr(img, 'original', None)
    if image:
        try:
            result = {'image': {
                'filename': os.path.basename(image.name),
                'content': base64.b64encode(image.read())
            }}
            return result
        except IOError:
            return {}
    else:
        return {}


def get_group_for_product(p) -> dict:
    c = p.categories.first()
    if c:
        if c.ms_id:
            productfolder_id = c.ms_id
        else:
            productfolder_id = MoyskladGroupAPI().get_productfolder_id(c)
            if not productfolder_id:
                print('No category: {}'.format(c))
                return {}

        result = {'productFolder': {
            "meta": {
                "href": "https://online.moysklad.ru/api/remap/1.1/entity/productfolder/{}".format(productfolder_id),
                "metadataHref": "https://online.moysklad.ru/api/remap/1.1/entity/productfolder/metadata",
                "type": "productfolder",
                "mediaType": "application/json"
            }
        }}
        return result
    else:
        return {}


def get_buy_price(p) -> dict:
    price = p.get_price_retail()
    if price:
        result = {'buyPrice': {
            "value": price * 100,
            "currency": {
                "meta": {
                    "href": "https://online.moysklad.ru/api/remap/1.1/entity/currency/addbd85f-fc1a-11e7-7a31-d0fd000238b5",
                    "metadataHref": "https://online.moysklad.ru/api/remap/1.1/entity/currency/metadata",
                    "type": "currency",
                    "mediaType": "application/json"
                }
            }
        }}
        return result
    else:
        return {}


def get_sale_prices(p) -> dict:
    price = p.get_price()
    if price:
        result = {'salePrices': [
            {
                "value": p.get_price() * 100,
                "currency": {
                    "meta": {
                        "href": "https://online.moysklad.ru/api/remap/1.1/entity/currency/addbd85f-fc1a-11e7-7a31-d0fd000238b5",
                        "metadataHref": "https://online.moysklad.ru/api/remap/1.1/entity/currency/metadata",
                        "type": "currency",
                        "mediaType": "application/json"
                    }
                },
                "priceType": "Цена продажи"
            },
        ]}
        return result
    else:
        return {}
