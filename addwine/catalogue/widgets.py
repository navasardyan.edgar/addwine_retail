from django.forms import widgets
from catalogue.models import ProductAttribute


class ProductAttributeSelectWidget(widgets.Select):

    option_template_name = 'admin/catalogue/productattribute/inline_select_option.html'

    def optgroups(self, name, value, attrs=None):
        groups = super().optgroups(name, value, attrs)

        target_ids = []
        group_map = {}
        for group in groups:
            for option in group[1]:
                value = option['value']
                if value:
                    target_ids.append(value)
                    group_map[value] = option

        if len(target_ids):
            types = ProductAttribute.objects.filter(pk__in=target_ids).values('pk', 'type')
            for field_type in types:
                group_map[field_type['pk']]['field_type'] = field_type['type']

        return groups

