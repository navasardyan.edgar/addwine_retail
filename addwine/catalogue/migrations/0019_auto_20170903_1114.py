# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-03 08:14
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0018_auto_20170902_1458'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='brandimage',
            options={'ordering': ['order_field'], 'verbose_name': 'изображение', 'verbose_name_plural': 'изображения'},
        ),
    ]
