# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-07 18:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0032_pack'),
    ]

    operations = [
        migrations.CreateModel(
            name='PackImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('original', models.ImageField(max_length=255, upload_to='images/products/%Y/%m/', verbose_name='Original')),
                ('caption', models.CharField(blank=True, max_length=200, verbose_name='Caption')),
                ('display_order', models.PositiveIntegerField(default=0, help_text='An image with a display order of zero will be the primary image for a product', verbose_name='Display order')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('pack', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='catalogue.Pack', verbose_name='комплект')),
            ],
            options={
                'verbose_name_plural': 'изображения',
                'verbose_name': 'изображение',
                'ordering': ['display_order'],
            },
        ),
    ]
