from model_mommy import mommy
from model_mommy.recipe import Recipe, foreign_key, seq
from .models import Product, ProductClass, Brand, ProductAlsoBuying, \
    ProductImage, Category
import random

mommy.generators.add('oscar.models.fields.slugfield.SlugField', lambda: 'slug')
mommy.generators.add('ckeditor_uploader.fields.RichTextUploadingField', lambda: '<html></html>')


rcp_product_class = Recipe(
    ProductClass,
)

rcp_brand = Recipe(
    Brand,
)

rcp_product_category = Recipe(
    Category,
)

rcp_product = Recipe(
    Product,
    product_class=foreign_key(rcp_product_class),
    brand=foreign_key(rcp_brand),
    title=seq('Product'),
    rating=lambda: random.randint(0, 10),
)

rcp_product_recommendation = Recipe(
    ProductAlsoBuying,
    primary=foreign_key(rcp_product),
    recommendation=foreign_key(rcp_product),
    ranking=lambda: random.randint(0, 10)
)

rcp_product_image = Recipe(
    ProductImage,
    product=foreign_key(rcp_product),
)


