from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
import json
from catalogue.moysklad_api.product import MoysklProductAPI
from catalogue.models import Product
import pprint


@method_decorator(csrf_exempt, name='dispatch')
class MoyskladWebhookView(View):
    def post(self, request):
        data = json.loads(request.body.decode('utf-8'))
        product_url = data['events'][0]['meta']['href']
        resp = MoysklProductAPI().get_product_by_url(product_url)
        resp = resp.json()
        p = Product.objects.filter(ms_uuid=resp['id']).first()
        if not p:
            p = Product.objects.filter(upc=resp['article']).first()
            if p:
                p.ms_uuid = resp['id']
                p.save()
        if p:
            info = p.stockrecords.first()
            info.price_excl_tax = resp['salePrices'][0]['value']/100
            info.price_retail = resp['buyPrice']['value']/100
            info.from_webhook = True
            info.save()

        return HttpResponse(status=200)
