from oscar.apps.catalogue.product_attributes import ProductAttributesContainer as BaseContainer

class ProductAttributesContainer(BaseContainer):
    def __getstate__(self):
        return self.__dict__