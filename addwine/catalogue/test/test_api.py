import os, json
import requests_mock
from model_mommy import mommy
from django.urls import reverse
from django.test.testcases import TestCase
from decimal import Decimal


def response_from_file(filename):
    with open(os.path.join(os.getcwd(), 'catalogue', 'test', 'mock_responses', filename), 'r', encoding='utf8') as f:
        data = json.load(f)
    return data


class TestWebhookRequest(TestCase):
    def setUp(self):
        self.p = mommy.make_recipe('catalogue.rcp_product')
        self.p.upc = '123-456'
        self.p.save()
        mommy.make_recipe('partner.rcp_stock_record', product=self.p)

    @requests_mock.Mocker()
    def test_success_sync_webhook(self, m):
        mock_get_product_url = 'https://online.moysklad.ru/api/remap/1.1/entity/product/c1557cfb-c2cc-11e6-7a31-d0fd000f0b00'
        m.get(mock_get_product_url,
              status_code=200,
              headers={'content-type': 'application/json'},
              json=response_from_file('product_c1557cfb-c2cc-11e6-7a31-d0fd000f0b00.json'),
              )

        webhok_url = reverse('moysklad_webhook')
        data = response_from_file('ms_webhook_input.json')
        r = self.client.post(webhok_url,
                             data=json.dumps(data),
                             content_type='application/json')

        self.assertEqual(r.status_code, 200)
        self.p.refresh_from_db()
        self.assertEqual(str(self.p.ms_uuid), 'c1557cfb-c2cc-11e6-7a31-d0fd000f0b00')
        self.assertEqual(self.p.get_price(), Decimal('346347.00'))
        self.assertEqual(self.p.get_price_retail(), Decimal('235530.00'))
