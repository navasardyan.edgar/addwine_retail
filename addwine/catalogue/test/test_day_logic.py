from django.test import TestCase
from unittest.mock import patch
from model_mommy import mommy
from django.conf import settings
from datetime import datetime
from catalogue.models import Product

import catalogue.templatetags.product_delivery_day as dd


mommy.generators.add('oscar.models.fields.slugfield.SlugField', lambda: 'slug')
mommy.generators.add('ckeditor_uploader.fields.RichTextUploadingField', lambda: '<html></html>')

TIME_PATTERN = '%d %m %Y %H:%M'


class TestDeliveryDayLogic(TestCase):
    def setUp(self):
        self.product = mommy.make_recipe('catalogue.rcp_product')
        self.product.is_available = True
        self.product.save()

    def execute_testcases(self, test_cases, mock_timezone):
        for (time_case, delivery_status, pickup_status) in test_cases:
            with self.subTest('Test product logic with moisklad', time_case=time_case):
                mock_timezone.localtime.return_value = datetime.strptime(time_case, TIME_PATTERN)
                expected_result = {'delivery': delivery_status, 'pickup': pickup_status, 'default': None}
                self.assertDictEqual(dd.get_delivery_day(self.product), expected_result)

    @patch('catalogue.templatetags.product_delivery_day.timezone')
    def test_products_logic_with_moysklad(self, mock_timezone):
        self.product.quantity = 10
        self.product.save()

        test_cases = (
            #     time case        delivery status      pickup status
            ('02 07 2018 09:12', dd.DELIVERY_AFTER_12, dd.PICKUP_TODAY_12_20),
            ('03 07 2018 16:59', dd.DELIVERY_AFTER_TWO_HOURS, dd.PICKUP_NOW),
            ('04 07 2018 20:01', dd.DELIVERY_TOMORROW, dd.PICKUP_TOMORROW),
            ('06 07 2018 17:01', dd.DELIVERY_AFTER_TWO_DAYS, dd.PICKUP_NOW),
            ('06 07 2018 20:01', dd.DELIVERY_AFTER_TWO_DAYS, dd.PICKUP_AFTER_TWO_DAYS),
            ('07 07 2018 12:00', dd.DELIVERY_AFTER_TOMORROW, dd.PICKUP_AFTER_TOMORROW),
            ('08 07 2018 00:00', dd.DELIVERY_TOMORROW, dd.PICKUP_TOMORROW),
        )
        self.execute_testcases(test_cases, mock_timezone)

    @patch('catalogue.templatetags.product_delivery_day.timezone')
    def test_products_logic_with_admin_today(self, mock_timezone):
        self.product.delivery_day = Product.DDATE_TODAY
        self.product.save()

        test_cases = (
            #     time case        delivery status      pickup status
            ('05 07 2018 13:59', dd.DELIVERY_TODAY, dd.PICKUP_TODAY_16_20),
            ('04 07 2018 19:59', dd.DELIVERY_TOMORROW, dd.PICKUP_TOMORROW),
            ('06 07 2018 14:01', dd.DELIVERY_AFTER_TWO_DAYS, dd.PICKUP_AFTER_TWO_DAYS),
            ('07 07 2018 12:00', dd.DELIVERY_AFTER_TOMORROW, dd.PICKUP_AFTER_TOMORROW),
            ('08 07 2018 00:00', dd.DELIVERY_TOMORROW, dd.PICKUP_TOMORROW),
        )
        self.execute_testcases(test_cases, mock_timezone)

    @patch('catalogue.templatetags.product_delivery_day.timezone')
    def test_products_logic_with_admin_nextday(self, mock_timezone):
        self.product.delivery_day = Product.DDATE_TOMORROW
        self.product.save()

        test_cases = (
            #     time case        delivery status      pickup status
            ('04 07 2018 13:59', dd.DELIVERY_TOMORROW, dd.PICKUP_TOMORROW_16_20),
            ('05 07 2018 13:59', dd.DELIVERY_TOMORROW, dd.PICKUP_TOMORROW_16_20),
            ('04 07 2018 19:59', dd.DELIVERY_AFTER_TOMORROW, dd.PICKUP_AFTER_TOMORROW),
            ('05 07 2018 19:59', dd.DELIVERY_AFTER_THREE_DAYS, dd.PICKUP_AFTER_THREE_DAYS),
            ('06 07 2018 11:00', dd.DELIVERY_AFTER_TWO_DAYS, dd.PICKUP_AFTER_TWO_DAYS),
            ('07 07 2018 12:00', dd.DELIVERY_AFTER_TOMORROW, dd.PICKUP_AFTER_TOMORROW),
            ('08 07 2018 00:00', dd.DELIVERY_TOMORROW, dd.PICKUP_TOMORROW),
        )
        self.execute_testcases(test_cases, mock_timezone)

    @patch('catalogue.templatetags.product_delivery_day.timezone')
    def test_products_logic_with_admin_twodays(self, mock_timezone):
        self.product.delivery_day = Product.DDATE_AFTER_TOMORROW
        self.product.save()

        test_cases = (
            #     time case        delivery status      pickup status
            ('03 07 2018 13:59', dd.DELIVERY_AFTER_TOMORROW, dd.PICKUP_AFTER_TOMORROW),
            ('04 07 2018 13:59',  dd.DELIVERY_AFTER_TOMORROW, dd.PICKUP_AFTER_TOMORROW),
            ('04 07 2018 19:59', dd.DELIVERY_AFTER_FOUR_DAYS, dd.PICKUP_AFTER_FOUR_DAYS),
            ('05 07 2018 19:59', dd.DELIVERY_AFTER_THREE_DAYS, dd.PICKUP_AFTER_THREE_DAYS),
            ('06 07 2018 11:00', dd.DELIVERY_AFTER_THREE_DAYS, dd.PICKUP_AFTER_THREE_DAYS),
            ('07 07 2018 12:00', dd.DELIVERY_AFTER_TWO_DAYS, dd.PICKUP_AFTER_TWO_DAYS),
            ('08 07 2018 00:00', dd.DELIVERY_AFTER_TOMORROW, dd.PICKUP_AFTER_TOMORROW),
        )
        self.execute_testcases(test_cases, mock_timezone)

    @patch('catalogue.templatetags.product_delivery_day.timezone')
    def test_products_logic_with_available_status(self, mock_timezone):

        test_cases = (
            #     time case        delivery status      pickup status
            ('04 07 2018 13:59', dd.DELIVERY_TOMORROW, dd.PICKUP_TOMORROW_16_20),
            ('05 07 2018 13:59', dd.DELIVERY_TOMORROW, dd.PICKUP_TOMORROW_16_20),
            ('04 07 2018 19:59', dd.DELIVERY_AFTER_TOMORROW, dd.PICKUP_AFTER_TOMORROW),
            ('05 07 2018 19:59', dd.DELIVERY_AFTER_THREE_DAYS, dd.PICKUP_AFTER_THREE_DAYS),
            ('06 07 2018 11:00', dd.DELIVERY_AFTER_TWO_DAYS, dd.PICKUP_AFTER_TWO_DAYS),
            ('07 07 2018 12:00', dd.DELIVERY_AFTER_TOMORROW, dd.PICKUP_AFTER_TOMORROW),
            ('08 07 2018 00:00', dd.DELIVERY_TOMORROW, dd.PICKUP_TOMORROW),
        )
        self.execute_testcases(test_cases, mock_timezone)

    def test_products_logic_with_available_status_false(self):
        self.product.is_available = False
        self.product.save()

        expected_result = {'delivery': None, 'pickup': None, 'default': 'по предзаказу'}
        self.assertDictEqual(dd.get_delivery_day(self.product), expected_result)



