import logging

import requests
from django.conf import settings
from django.contrib import messages

from catalogue.api.serializers import ProductDetailSerializer
from catalogue.models import Product

logger = logging.getLogger(__name__)


def export_product_to_wl(request, product_id):
    """Экспорт в WineLevel
    """
    product = Product.objects.get(pk=product_id)
    if not settings.WINELEVEL_EXPORT_IS_ENABLED or product.exported_to_wl:
        return

    try:
        exported_data = ProductDetailSerializer(product).data
        response = requests.post(
            url=settings.WINELEVEL_EXPORT_URL,
            json=exported_data,
        )
        response.raise_for_status()
        product.exported_to_wl = True
        product.save()
        messages.success(request, '{} экспортирован в winelevel'.format(str(product)))
    except Exception as e:
        msg = 'Ошибка при экспорте {} в winelevel: {}'.format(str(product), str(e))
        logger.error(msg, exc_info=True)
        messages.error(request, msg)
