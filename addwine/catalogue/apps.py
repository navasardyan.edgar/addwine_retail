from django.apps import AppConfig


class CatalogueConfig(AppConfig):
    name = 'catalogue'
    verbose_name = 'Каталог'

    def ready(self):
        from oscar.apps.catalogue import receivers
        import catalogue.signals_handlers