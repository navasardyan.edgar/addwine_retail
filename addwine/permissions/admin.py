from django.contrib import admin

from django.conf import settings

RULES_MAP = {
    'a': 'add',
    'c': 'change',
    'd': 'delete'
}

PERMISSION_NAMES = []

for model_name, rules in settings.PERMISSIONS.items():
    for rule in rules:
        PERMISSION_NAMES.append(RULES_MAP[rule]+'_'+model_name)

class PermissionAdminMixin(object):

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name in ('permissions', 'user_permissions'):
            qs = kwargs.get('queryset', db_field.rel.to.objects)
            qs = self.__filter_permissions(qs)
            kwargs['queryset'] = qs

        return super(PermissionAdminMixin, self).formfield_for_manytomany(db_field, request, **kwargs)

    def __filter_permissions(self, qs):
        qs = qs.filter(codename__in=PERMISSION_NAMES)
        return qs
