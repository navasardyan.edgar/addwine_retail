# docker build -t ubuntu1604py36
FROM ubuntu:16.04

RUN apt-get -y update
RUN apt-get upgrade -y
RUN apt-get install -y --fix-missing \
    gcc \
    git \
    wget \
    curl \
    pkg-config \
    python3.5 \
    python3.5-dev \
    software-properties-common \
    zip \
    build-essential \
    libaio1 \
    libxml2-dev \
    libxslt-dev \
    && apt-get clean && rm -rf /tmp/* /var/tmp/*

RUN wget https://bootstrap.pypa.io/pip/3.5/get-pip.py
RUN python3.5 get-pip.py

RUN pip install --no-cache-dir wheel

COPY ./requirements.txt ./
RUN pip3 install --no-cache-dir -r requirements.txt

ADD ./addwine ./addwine
ADD ./celery ./celery
#ADD ./public_html ./public_html
ADD ./locale ./locale


WORKDIR /addwine
